<?php

namespace Core\System;

use Core\EntityId;
use Core\Events\IEventInitiator;
use Core\User\Role;

class System implements IEventInitiator {
  public const ID = '0';

  /**
   * @inheritDoc
   */
  public function getId(): EntityId {
    return new EntityId(self::ID);
  }

  /**
   * @inheritDoc
   */
  public function getRole(): Role {
    return Role::SYSTEM;
  }
}
