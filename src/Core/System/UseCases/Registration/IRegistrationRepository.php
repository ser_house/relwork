<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.01.2019
 * Time: 13:41
 */

namespace Core\System\UseCases\Registration;


use Core\System\UseCases\Registration\Exceptions\NotFoundException;
use Core\User\Email\Email;

/**
 * Interface IRegistrationRepository
 */
interface IRegistrationRepository {

	/**
	 * @param Registration $registration
	 *
	 * @return void
	 */
	public function save(Registration $registration): void;

	/**
	 * @param Email $email
	 *
	 * @return Registration|null
	 */
	public function findByEmail(Email $email): ?Registration;

  /**
   * @param Email $email
   *
   * @return Registration
   * @throws NotFoundException
   */
	public function getByEmail(Email $email): Registration;

	/**
	 * @param Email $email
	 */
	public function removeByEmail(Email $email): void;
}
