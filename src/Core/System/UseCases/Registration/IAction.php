<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:03
 */

namespace Core\System\UseCases\Registration;

interface IAction {
	/**
	 *
	 */
	public function execute(): void;
}
