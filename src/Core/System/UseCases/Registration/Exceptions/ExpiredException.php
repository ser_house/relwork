<?php

namespace Core\System\UseCases\Registration\Exceptions;

use DomainException;

final class ExpiredException extends DomainException {

}
