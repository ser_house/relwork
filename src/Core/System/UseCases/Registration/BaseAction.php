<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:31
 */

namespace Core\System\UseCases\Registration;

class BaseAction {

	public function __construct(protected Registration $registration) {

	}
}
