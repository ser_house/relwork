<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2019
 * Time: 0:10
 */

namespace Core\System\UseCases\Registration;

use Core\User\Email\Email;
use Core\User\IUserRepository;
use DateInterval;
use DateTimeImmutable;
use Exception;

/**
 * Class Registration
 */
class Registration {

	/** @var int пауза в минутах, запрещающая повторную отправку письма */
	public const PAUSE_AFTER_SENT = 5;
	/** @var int срок годности ссылки в письме в минутах */
	public const LINK_LIFETIME = 24 * 60;

	private DateTimeImmutable $time;

	/**
	 * Registration constructor.
	 *
	 * @param Email $email
	 * @param DateTimeImmutable|null $time
	 *
	 * @throws Exception
	 */
	public function __construct(public readonly Email $email, DateTimeImmutable $time = null) {
		$this->time = $time ?? new DateTimeImmutable();
	}

	/**
	 * @param IRegistrationRepository $registrationRepository
	 *
	 * @return bool
	 */
	public function isSent(IRegistrationRepository $registrationRepository): bool {
		$item = $registrationRepository->findByEmail($this->email);
		return null !== $item;
	}

	/**
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function isPauseTimeExpired(): bool {
		$minutes = self::PAUSE_AFTER_SENT;
		$interval = new DateInterval("PT{$minutes}M");

		$intervalTime = $this->time->add($interval);

		$now = new DateTimeImmutable();
		return $now->getTimestamp() >= $intervalTime->getTimestamp();
	}

	/**
	 * @param IUserRepository $repository
	 *
	 * @return bool
	 */
	public function emailIsExists(IUserRepository $repository): bool {
		return $repository->existsUserWithEmail($this->email);
	}

	/**
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function isExpired(): bool {
		$minutes = self::LINK_LIFETIME;
		$interval = new DateInterval("PT{$minutes}M");

		$intervalTime = $this->time->add($interval);

		$now = new DateTimeImmutable();
		return $now->getTimestamp() > $intervalTime->getTimestamp();
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function getTime(): DateTimeImmutable {
		return $this->time;
	}
}
