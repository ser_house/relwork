<?php

namespace Core\System\UseCases\Registration;

use Core\User\Email\Email;

interface IUrlGenerator {

  public function generateCreateProfileUrl(Email $email): string;
}
