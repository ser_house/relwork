<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 20:37
 */

namespace Core\System\UseCases\Registration;

use Exception;

/**
 * Interface IRegistrationActionFactory
 */
interface IRegistrationActionFactory {
	/**
	 * @param Registration $registration
	 *
	 * @return IAction
	 * @throws Exception
	 */
	public function getAction(Registration $registration): IAction;
}
