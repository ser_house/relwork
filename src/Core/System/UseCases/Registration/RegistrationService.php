<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2019
 * Time: 2:17
 */

namespace Core\System\UseCases\Registration;


use Core\Generic\Errors\Exceptions\MissingValueException;
use Core\Generic\Infrastructure\EncodeStringService;
use Core\System\UseCases\Registration\Exceptions\ExpiredException;
use Core\System\UseCases\Registration\Exceptions\NotFoundException;
use Core\User\Email\Email;
use Core\User\Exceptions\MissingMailCodeException;


/**
 * Class RegistrationService
 */
class RegistrationService {


  public function __construct(
    private IRegistrationRepository $registrationRepository,
    private EncodeStringService $encodeStringService,
  ) {
  }

  /**
   * @param string|null $code
   *
   * @return Email
   * @throws MissingMailCodeException|NotFoundException|ExpiredException
   */
  public function getEmailFromCode(?string $code): Email {
    if (empty($code)) {
      throw new MissingMailCodeException('Missing code from e-mail.');
    }

    $decoded_email = $this->encodeStringService->decode($code);
    $email = new Email($decoded_email);

    $registration = $this->registrationRepository->getByEmail($email);

    if ($registration->isExpired()) {
      $this->registrationRepository->removeByEmail($email);
      throw new ExpiredException("Registration time for '$email' expired.");
    }

    return $email;
  }
}
