<?php

namespace Core\Events;


use DateTimeImmutable;

abstract class Event {

  public readonly DateTimeImmutable $dateTime;

  protected function __construct(public readonly IEventInitiator $eventInitiator) {
    $this->dateTime = new DateTimeImmutable();
  }
}
