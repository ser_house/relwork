<?php

namespace Core\Events;


trait Events {
  /** @var Event[] */
  private array $events = [];

  /**
   * @param Event $event
   */
  private function addEvent(Event $event): void {
    $this->events[] = $event;
  }

  /**
   * @return Event[]
   */
  public function releaseEvents(): array {
    $events = $this->events;
    $this->events = [];
    return $events;
  }
}
