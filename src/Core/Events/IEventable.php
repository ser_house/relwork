<?php

namespace Core\Events;

interface IEventable {
  /**
   * @return Event[]
   */
  public function releaseEvents(): array;
}
