<?php

namespace Core\Events;

interface IEventDispatcher {

  /**
   * @param Event $event
   *
   * @return void
   */
  public function dispatch(Event $event): void;

  /**
   * @param iterable $events
   *
   * @return void
   */
  public function dispatchEvents(iterable $events): void;
}
