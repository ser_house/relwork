<?php

namespace Core\Events;


use Core\EntityId;
use Core\User\Role;


interface IEventInitiator {

  /**
   *
   * @return EntityId
   */
  public function getId(): EntityId;

  /**
   *
   * @return Role
   */
  public function getRole(): Role;
}
