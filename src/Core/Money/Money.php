<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.02.2019
 * Time: 18:26
 */

namespace Core\Money;


use InvalidArgumentException;

/**
 * Class Money
 */
class Money {

	/**
	 * Money constructor.
	 *
	 * @param float $amount
	 * @param Currency $currency
	 */
	public function __construct(
    private float $amount = 0.0,
    public readonly Currency $currency = new Currency('RUB')) {
	}

	/**
	 * @return float
	 */
	public function getAmount(): float {
		return $this->amount;
	}

	/**
	 * @return Currency
	 */
	public function getCurrency(): Currency {
		return $this->currency;
	}

	/**
	 * @param Money $amount
	 *
	 * @return Money
	 */
	public function add(Money $amount): Money {
		$new_value = $this->amount + $amount->getAmount();

		return new self($new_value, $this->currency);
	}

	/**
	 * @param Money $amount
	 *
	 * @return Money
	 */
	public function subtract(Money $amount): self {
    $new_value = $this->amount - $amount->getAmount();

    return new self($new_value, $this->currency);
	}


	/**
	 * Checks whether a Money has the same Currency as this.
	 *
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function isSameCurrency(Money $other): bool {
		return (string)$this->currency === (string)$other->getCurrency();
	}

	/**
	 * Asserts that a Money has the same currency as this.
	 *
	 * @param Money $other
	 *
	 * @throws InvalidArgumentException If $other has a different currency
	 */
	private function assertSameCurrency(Money $other): void {
		if (!$this->isSameCurrency($other)) {
			throw new InvalidArgumentException('Currencies must be identical');
		}
	}

	/**
	 * Checks whether the value represented by this object equals to the other.
	 *
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function equals(Money $other): bool {
		return $this->isSameCurrency($other) && $this->amount === $other->amount;
	}

	/**
	 * Returns an integer less than, equal to, or greater than zero
	 * if the value of this object is considered to be respectively
	 * less than, equal to, or greater than the other.
	 *
	 * @param Money $other
	 *
	 * @return int
	 */
	public function compare(Money $other): int {
    $this->assertSameCurrency($other);
    return $this->amount <=> $other->getAmount();
	}

	/**
	 * Checks whether the value represented by this object is greater than the other.
	 *
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function greaterThan(Money $other): bool {
		return $this->compare($other) > 0;
	}

	/**
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function greaterThanOrEqual(Money $other): bool {
		return $this->compare($other) >= 0;
	}

	/**
	 * Checks whether the value represented by this object is less than the other.
	 *
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function lessThan(Money $other): bool {
		return $this->compare($other) < 0;
	}

	/**
	 * @param Money $other
	 *
	 * @return bool
	 */
	public function lessThanOrEqual(Money $other): bool {
		return $this->compare($other) <= 0;
	}
}
