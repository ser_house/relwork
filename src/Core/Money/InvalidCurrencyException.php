<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.08.2021
 * Time: 11:37
 */

namespace Core\Money;

use DomainException;

final class InvalidCurrencyException extends DomainException {

}
