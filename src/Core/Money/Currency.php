<?php

namespace Core\Money;

class Currency {
  private readonly string $value;

  /**
   * Currency constructor.
   *
   * @param string $value
   *
   */
  public function __construct(string $value) {
    if (strlen($value) < 3 ) {
      throw new InvalidCurrencyException("Значение валюты не может быть короче 3-х символов: \{'$value'\}");
    }
    // Код советского рубля ещё используется в финансовых инструментах.
    $this->value = 'SUR' === $value ? 'RUB' : $value;
  }

  public function __toString() {
    return $this->value;
  }
}
