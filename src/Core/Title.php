<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 9:19
 */

namespace Core;


class Title {

	public function __construct(public readonly string $value) {

	}

	/**
	 * @inheritDoc
	 */
	public function __toString() {
		return $this->value;
	}
}
