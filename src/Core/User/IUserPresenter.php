<?php

namespace Core\User;

use Core\Client\View\ClientMenuView;
use Core\Client\View\ProfileView;

interface IUserPresenter {

  /**
   * @return ClientMenuView
   */
  public function presentMenu(): ClientMenuView;

  /**
   * @return ProfileView
   */
  public function presentProfile(): ProfileView;
}
