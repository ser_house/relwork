<?php

namespace Core\User;

use Core\Generic\Infrastructure\Hydrator as GenericHydrator;
use Core\User\Email\Email;
use Core\User\Password\HashedPassword;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use ReflectionClass;
use ReflectionException;

final class Hydrator extends GenericHydrator {

  /**
   * @param array $data
   * @param string $class
   *
   * @return object
   * @throws JsonException
   * @throws ReflectionException
   */
  public function hydrate(array $data, string $class = User::class): object {
    $refl = new ReflectionClass($class);

    $object = $refl->newInstanceWithoutConstructor();

    $props = $refl->getProperties();
    foreach ($props as $prop) {
      $prop_name = $prop->name;

      switch ($prop_name) {
        case 'events':
          continue 2;

        case 'avatar':
          $value = $this->hydrateAvatar($data);
          break;

        default:
          $db_field_name = $this->propToDbFieldName($prop_name);
          if (isset($data[$db_field_name])) {
            $value = $this->hydratePropValue($prop_name, $data[$db_field_name], $class);
          }
          else {
            $value = null;
          }
          break;
      }
      $prop->setValue($object, $value);
    }

    return $object;
  }

  protected function hydrateAvatar(array $data): object {
    return new Avatar(
      $data['avatar_name'],
      $data['avatar_rel_dir'],
      $data['avatar_uri'],
    );
  }

  /**
   * @param string $prop
   *
   * @return string
   */
  protected function propToDbFieldName(string $prop): string {
    return match ($prop) {
      'createdAt' => 'created_at',
      'timeZone' => 'timezone',
      default => $prop,
    };
  }

  /**
   * @param string $prop_name
   * @param mixed $val
   *
   * @return mixed
   * @throws JsonException
   */
  protected function getExplicitValue(string $prop_name, mixed $val): mixed {
    switch ($prop_name) {
      case 'createdAt':
        return new DateTimeImmutable($val);

      case 'timeZone':
        return new DateTimeZone($val);

      case 'role':
        return Role::from($val);

      default:
        return null;
    }
  }

  /**
   * @param string $prop_name
   * @param string $class
   *
   * @return ReflectionClass
   * @throws Exception
   */
  protected function getPropReflection(string $prop_name, string $class): ReflectionClass {
    return match ($prop_name) {
      'id' => new ReflectionClass(UserId::class),
      'name' => new ReflectionClass(NickName::class),
      'email' => new ReflectionClass(Email::class),
      'password' => new ReflectionClass(HashedPassword::class),
      default => throw new Exception("Unknown prop '$prop_name'"),
    };
  }
}
