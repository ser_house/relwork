<?php

namespace Core\User;

class Avatar {

  public function __construct(
    public readonly string $name,
    public readonly string $rel_dir,
    public readonly string $uri,
  ) {
  }
}
