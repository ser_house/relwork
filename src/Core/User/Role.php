<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2019
 * Time: 6:02
 */

namespace Core\User;

use Core\Title;


enum Role : int {
  case SYSTEM = 0;
  case SELLER = 1;
  case BUYER = 2;
  case SUPPORT = 3;
  case MODERATOR = 4;
  case ARBITER = 5;
  case ADMIN = 6;


  /**
   * @return Title
   */
  public function title(): Title {
    return match($this) {
      self::SYSTEM => new Title('Система'),
      self::SELLER => new Title('Продавец'),
      self::BUYER => new Title('Покупатель'),
      self::SUPPORT => new Title('Менеджер поддержки'),
      self::MODERATOR => new Title('Модератор'),
      self::ARBITER => new Title('Арбитр'),
      self::ADMIN => new Title('Администратор'),
    };
  }
}
