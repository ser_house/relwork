<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2019
 * Time: 5:51
 */

namespace Core\User\Context;


use Core\User\User;

interface IUserContext {

  /**
   * @return User
   */
  public function user(): User;
}
