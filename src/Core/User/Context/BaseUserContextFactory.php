<?php

namespace Core\User\Context;

use Core\File\IFileRepository;
use Core\Generic\Infrastructure\StaticCache;
use Core\User\IUserPresenter;
use Core\User\User;

abstract class BaseUserContextFactory implements IUserContextFactory {
  protected StaticCache $cache;

  public function __construct(protected IFileRepository $fileRepository) {
    $this->cache = new StaticCache();
  }

  /**
   * @inheritDoc
   */
  public function createContext(User $user): IUserContext {
    return $this->buildContext($user);
  }

  /**
   * @inheritDoc
   */
  public function createPresenter(User $user): IUserPresenter {
    return $this->buildPresenter($user);
  }

  /**
   * @param User $user
   *
   * @return IUserContext
   */
  abstract protected function buildContext(User $user): IUserContext;

  /**
   * @param User $user
   *
   * @return IUserPresenter
   */
  abstract protected function buildPresenter(User $user): IUserPresenter;
}
