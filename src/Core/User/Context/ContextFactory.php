<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2019
 * Time: 5:30
 */

namespace Core\User\Context;

use Core\Client\Buyer\BuyerContextFactory;
use Core\Client\Seller\SellerContextFactory;
use Core\User\Role;
use Core\User\User;


class ContextFactory {

  public function __construct(
    private SellerContextFactory $sellerContextFactory,
    private BuyerContextFactory $buyerContextFactory,
  ) {

  }

  /**
   * @param User $user
   *
   * @return IUserContextFactory
   * @throws ContextException
   */
  public function getUserContextFactory(User $user): IUserContextFactory {
    return match ($user->role) {
      Role::SELLER => $this->sellerContextFactory,
      Role::BUYER => $this->buyerContextFactory,
      default => throw new ContextException('Не определен контекст для роли ' . $user->role->title()),
    };
  }
}
