<?php

namespace Core\User\Context;

use Core\User\IUserPresenter;
use Core\User\User;

interface IUserContextFactory {

  /**
   * @param User $user
   *
   * @return IUserContext
   */
  public function createContext(User $user): IUserContext;

  /**
   * @param User $user
   *
   * @return IUserPresenter
   */
  public function createPresenter(User $user): IUserPresenter;
}
