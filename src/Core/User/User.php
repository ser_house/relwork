<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.01.2019
 * Time: 6:49
 */

namespace Core\User;

use Core\File\IFileService;
use Core\User\Email\Email;
use Core\User\Password\HashedPassword;
use DateTimeImmutable;
use DateTimeZone;


/**
 * Class User
 */
class User {
  public readonly UserId $id;
  public readonly DateTimeImmutable $createdAt;
  public readonly Avatar $avatar;

  /**
   * Пользователь должен быть с аватаркой. Но для создания конечного файла
   * аватарки нужен пользователь (его id), чтобы сформировать директории.
   * Получается, что для создания пользователя нужен созданный файл аватарки,
   * а для создания файла аватарки нужен пользователь.
   * Вполне логично, что файл будет создан в процессе создания пользователя.
   */
  public function __construct(
    public readonly NickName $name,
    public readonly Email $email,
    public readonly HashedPassword $password,
    public readonly DateTimeZone $timeZone,
    public readonly Role $role,
    AvatarInput $avatarInput,
    IFileService $fileService,
  ) {
    $this->id = new UserId();
    $this->createdAt = new DateTimeImmutable();

    $uploadedFile = $fileService->moveToPublic($this->id, $avatarInput->file);
    $this->avatar = new Avatar($uploadedFile->name, $uploadedFile->relative_directory, $uploadedFile->uri);
  }
}
