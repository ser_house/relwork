<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:14
 */

namespace Core\User\Events;

use Core\Events\Event;
use Core\System\System;
use Core\User\User;

/**
 * Class UserRegisteredEvent
 */
class UserRegisteredEvent extends Event {

  public function __construct(public readonly User $user) {
    parent::__construct(new System());
  }
}
