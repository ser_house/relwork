<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2019
 * Time: 19:38
 */

namespace Core\User\Email;


use InvalidArgumentException;

class Email {

  /**
   * Email constructor.
   *
   * @param string $value
   */
  public function __construct(private string $value) {
    $value = trim($value);
    if (!str_contains($value, '@')) {
      throw new InvalidArgumentException("Invalid email: $value");
    }
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }
}
