<?php

namespace Core\User\Email;

interface IEmailSender {

  /**
   * @param Email $email
   * @param string $subject
   * @param array $content
   *
   * @return void
   */
  public function sendRegistrationEmail(Email $email, string $subject, array $content): void;

  /**
   * @param Email $email
   * @param string $subject
   * @param string $template
   * @param array $content
   *
   * @return void
   */
  public function send(Email $email, string $subject, string $template, array $content): void;
}
