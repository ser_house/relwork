<?php

namespace Core\User\UseCases\ViewProfile;

use Core\Client\View\ProfileView;
use Core\User\Context\ContextFactory;
use Core\User\User;

class Action {
  public function __construct(
    private ContextFactory $contextFactory,
  ) {
  }

  public function getView(User $user): ProfileView {
    $userContextFactory = $this->contextFactory->getUserContextFactory($user);
    return $userContextFactory->createPresenter($user)->presentProfile();
  }
}
