<?php

namespace Core\User\UseCases\CreateProfile;

use Core\Client\Buyer\Buyer;
use Core\Client\Buyer\IBuyerRepository;
use Core\Client\Events\ClientRegisteredEvent;
use Core\Client\Seller\ISellerRepository;
use Core\Client\Seller\Seller;
use Core\Events\IEventDispatcher;
use Core\File\IFileService;
use Core\File\Image\UploadProcessor\CompoundProcessor;
use Core\File\Image\UploadProcessor\CropProcessor;
use Core\File\Image\UploadProcessor\SizeProcessor;
use Core\Generic\Infrastructure\TimeZoneService;
use Core\ITransaction;
use Core\System\UseCases\Registration\IRegistrationRepository;
use Core\User\Events\UserRegisteredEvent;
use Core\User\Role;
use Core\User\User;
use Throwable;
use Core\File\Image\Rules\Avatar as AvatarRules;

class Action {
  public function __construct(
    private IRegistrationRepository $registrationRepository,
    private ISellerRepository $sellerRepository,
    private IBuyerRepository $buyerRepository,
    private IEventDispatcher $eventDispatcher,
    private ITransaction $transaction,
    private IFileService $fileService,
    private TimeZoneService $timeZoneService,
    private AvatarRules $avatarRules,
  ) {
  }

  public function defaultValues(): array {
    $roles_map = [
      Role::SELLER->value => (string)Role::SELLER->title(),
      Role::BUYER->value => (string)Role::BUYER->title(),
    ];
    return [
      'role' => Role::SELLER->value,
      'roles' => $roles_map,
      'timezones' => $this->timeZoneService->listKeyedByName(),
      'rules' => $this->avatarRules->rules(),
    ];
  }

  public function handle(Request $request): User {
    $avatarInput = $request->avatarInput;
    $processor = new CompoundProcessor([
      new CropProcessor($avatarInput->x, $avatarInput->y, $avatarInput->size, $avatarInput->size),
      new SizeProcessor(),
    ]);

    $processor->process($avatarInput->file->getPath());

    $user = new User(
      $request->name,
      $request->email,
      $request->hashedPassword,
      $request->tz,
      $request->role,
      $avatarInput,
      $this->fileService,
    );

    $this->transaction->begin();
    try {
      switch ($request->role) {
        case Role::SELLER:
          $seller = new Seller($user);
          $this->sellerRepository->add($seller);
          break;

        case Role::BUYER:
          $buyer = new Buyer($user);
          $this->buyerRepository->add($buyer);
          break;

        default:
          break;
      }

      $this->registrationRepository->removeByEmail($request->email);

      $this->transaction->commit();

      $this->eventDispatcher->dispatch(new UserRegisteredEvent($user));
      $this->eventDispatcher->dispatch(new ClientRegisteredEvent($user->id, $request->role));

      return $user;
    }
    catch (Throwable $e) {
      $this->transaction->rollBack();
      $this->fileService->remove($avatarInput->file->getPath());
      // @todo: удаляем загруженный системой файл и созданный файл аватарки.
      $this->fileService->removeByRelativePath("{$user->avatar->rel_dir}/{$user->avatar->name}");

      throw $e;
    }
  }
}
