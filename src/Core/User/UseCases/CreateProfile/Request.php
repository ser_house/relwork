<?php

namespace Core\User\UseCases\CreateProfile;

use Core\Generic\Errors\Exceptions\FieldsExceptionsCollection;
use Core\Generic\Errors\Exceptions\InvalidInputDataException;
use Core\Generic\Errors\Exceptions\MissingValueException;
use Core\User\AvatarInput;
use Core\User\Email\Email;
use Core\User\Exceptions\InvalidRoleValueException;
use Core\User\NickName;
use Core\User\Password\HashedPassword;
use Core\User\Password\Password;
use Core\User\Role;
use DateTimeZone;
use Throwable;

class Request {
  public readonly NickName $name;
  public readonly Password $password;
  public readonly HashedPassword $hashedPassword;
  public readonly DateTimeZone $tz;
  public readonly Role $role;
  public readonly AvatarInput $avatarInput;

  public function __construct(public readonly Email $email, array $data) {

    $fieldExceptions = new FieldsExceptionsCollection('profile_labels');

    $required = [
      'nickname',
      'timezone',
      'role',
      'avatar',
      'x',
      'y',
      'size',
    ];

    foreach ($required as $field) {
      switch($field) {
        case 'role':
        case 'x':
        case 'y':
          $is_missing = !isset($data[$field]);
          break;

        default:
          $is_missing = empty($data[$field]);
          break;
      }
      if ($is_missing) {
        $fieldExceptions->addException(new MissingValueException($field), $field);
      }
    }

    $role = Role::tryFrom($data['role']);
    if (null === $role) {
      $fieldExceptions->addException(new InvalidRoleValueException($data['role']), $field);
    }

    $this->role = $role;

    if (!$fieldExceptions->hasExceptions('avatar')) {
      try {
        $this->avatarInput = new AvatarInput(
          $data['avatar'],
          $data['x'],
          $data['y'],
          $data['size']
        );
      }
      catch (InvalidInputDataException $e) {
        $fieldExceptions->addExceptionsCollection($e->exceptionsCollection, 'avatar');
      }
      catch (Throwable $e) {
        $fieldExceptions->addException($e, 'avatar');
      }
    }

    try {
      $this->name = new NickName($data['nickname']);
    }
    catch (Throwable $e) {
      $fieldExceptions->addException($e, 'nickname');
    }

    $this->password = new Password();
    $this->hashedPassword = new HashedPassword($this->password);

    $this->tz = new DateTimeZone($data['timezone']);

    if ($fieldExceptions->hasExceptions()) {
      throw new InvalidInputDataException($fieldExceptions);
    }
  }
}
