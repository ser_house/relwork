<?php

namespace Core\User;

use Core\File\IMovableFile;
use Core\Generic\Errors\Exceptions\FieldsExceptionsCollection;
use Core\Generic\Errors\Exceptions\InvalidInputDataException;
use Core\Generic\Errors\Exceptions\IsLessThanException;

class AvatarInput {

  public function __construct(
    public readonly IMovableFile $file,
    public readonly float $x,
    public readonly float $y,
    public readonly float $size,
  ) {

    $fieldExceptions = new FieldsExceptionsCollection();

    if ($this->x < 0.0) {
      $fieldExceptions->addException(new IsLessThanException($this->x, 0.0), 'x');
    }
    if ($this->y < 0.0) {
      $fieldExceptions->addException(new IsLessThanException($this->y, 0.0), 'y');
    }
    if ($this->size < 100.0) {
      $fieldExceptions->addException(new IsLessThanException($this->size, 100.0), 'size');
    }

    if ($fieldExceptions->hasExceptions()) {
      throw new InvalidInputDataException($fieldExceptions);
    }
  }
}
