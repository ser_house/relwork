<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2019
 * Time: 19:40
 */

namespace Core\User;


use Core\Generic\Errors\Exceptions\LengthException;

class NickName {

  private const MIN_LEN = 3;
  private const MAX_LEN = 10;


  public function __construct(private readonly string $value) {

    $length = mb_strlen($this->value);
    if ($length < self::MIN_LEN || $length > self::MAX_LEN) {
      throw new LengthException($this->value, self::MIN_LEN, self::MAX_LEN);
    }
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }
}
