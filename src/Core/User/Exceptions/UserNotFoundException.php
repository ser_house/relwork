<?php

namespace Core\User\Exceptions;

use DomainException;

final class UserNotFoundException extends DomainException {

}
