<?php

namespace Core\User\Exceptions;

use DomainException;

class InvalidValueException extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(
    string $message,
    public readonly mixed $value,
    public readonly mixed $restrict,

  ) {
    parent::__construct($message);
  }
}
