<?php

namespace Core\User\Exceptions;

use DomainException;

class InvalidRoleValueException extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(public readonly mixed $value) {
    parent::__construct();
  }

}
