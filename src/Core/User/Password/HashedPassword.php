<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2019
 * Time: 19:25
 */

namespace Core\User\Password;


class HashedPassword {

  private readonly string $value;

	public function __construct(string $value) {
    $this->value = password_hash($value, PASSWORD_ARGON2I);
	}

	/**
	 * @inheritDoc
	 */
	public function __toString() {
		return $this->value;
	}
}
