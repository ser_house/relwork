<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.02.2019
 * Time: 19:25
 */

namespace Core\User\Password;


class Password {

  private readonly string $value;

  public function __construct() {
    $symbols = "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ-0123456789+";
    $shuffled = str_shuffle($symbols);
    $this->value = substr($shuffled, 5, random_int(15, 20));
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }
}
