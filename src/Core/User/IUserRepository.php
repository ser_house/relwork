<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2019
 * Time: 12:57
 */

namespace Core\User;

use Core\User\Email\Email;
use Core\User\Exceptions\UserNotFoundException;


interface IUserRepository {
	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	public function existsUserWithEmail(string $email): bool;

	/**
	 * @param UserId $userId
	 *
	 * @return User|null
	 */
	public function findByUserId(UserId $userId): ?User;

  /**
   * @param UserId $userId
   *
   * @return User
   * @throws UserNotFoundException
   */
	public function getByUserId(UserId $userId): User;

	/**
	 * @param NickName $name
	 *
	 * @return User|null
	 */
	public function findByNickname(NickName $name): ?User;

  /**
   * @param Email $email
   *
   * @return User|null
   */
	public function findByEmail(Email $email): ?User;

	/**
	 * @param UserId $userId
	 * @param Role $newRole
	 */
	public function updateRole(UserId $userId, Role $newRole): void;
}
