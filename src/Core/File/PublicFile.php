<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.03.2019
 * Time: 18:08
 */

namespace Core\File;

class PublicFile {

	public function __construct(
    public readonly string $name,
    public readonly string $relative_directory,
    public readonly string $uri,
  ) {
	}
}
