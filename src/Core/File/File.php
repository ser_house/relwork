<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 0:51
 */

namespace Core\File;

use Core\User\UserId;
use DateTimeImmutable;


/**
 * Class File
 */
class File {

  public readonly FileId $id;
  public readonly DateTimeImmutable $createdAt;
  public readonly ?DateTimeImmutable $updatedAt;

	/**
	 * File constructor.
	 *
	 * @param UserId $userId
	 * @param string $name
   *   название файла, под которым он хранится в системе
   *   все публичные адреса файла будут содержать именно его
   *   исключение - формы создания и редактирования содержащей файл сущности
	 * @param string $relative_directory директория в файловой системе, в которой находится файл
	 * @param string $uri относительный от публичной директории сайта адрес файла
	 */
	public function __construct(
    public readonly UserId $userId,
    public readonly string $name,
    public readonly string $relative_directory,
    public readonly string $uri,
  ) {
    $this->id = new FileId();
    $this->createdAt = new DateTimeImmutable();
	}

	/**
	 * Относительный путь к файлу.
	 *
	 * @return string
	 */
	public function getRelativeFilePath(): string {
		return "$this->relative_directory/$this->name";
	}
}
