<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.03.2019
 * Time: 21:37
 */

namespace Core\File\Events;


use Core\Events\Event;
use Core\Events\IEventInitiator;
use Core\File\FileId;

class NewFileSavedEvent extends Event {

  public function __construct(
    IEventInitiator $eventInitiator,
    public readonly FileId $fileId,
  ) {
    parent::__construct($eventInitiator);
  }
}
