<?php

namespace Core\File\Events;

use Core\Events\Event;
use Core\Events\IEventInitiator;
use Core\File\FileId;

class FileUpdatedEvent extends Event {

  public function __construct(
    IEventInitiator $eventInitiator,
    public readonly FileId $fileId,
  ) {
    parent::__construct($eventInitiator);
  }
}
