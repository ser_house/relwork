<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2019
 * Time: 13:02
 */

namespace Core\File\Image\UploadProcessor;

use Imagick;

class CropProcessor implements IProcessor {
  public function __construct(
    private readonly int $x,
    private readonly int $y,
    private readonly int $w,
    private readonly int $h,
  ) {
  }


  /**
   * @inheritDoc
   */
  public function process(string $file_path): void {
    $img = new Imagick($file_path);
    $img->cropImage($this->w, $this->h, $this->x, $this->y);
    $img->writeImage($file_path);
  }
}
