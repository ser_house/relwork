<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2019
 * Time: 13:00
 */

namespace Core\File\Image\UploadProcessor;

interface IProcessor {

  /**
   * @param string $file_path
   *
   * @return void
   */
	public function process(string $file_path): void;
}
