<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2019
 * Time: 13:02
 */

namespace Core\File\Image\UploadProcessor;


class CompoundProcessor implements IProcessor {

  /**
   * CompoundProcessor constructor.
   *
   * @param IProcessor[] $processors
   */
  public function __construct(private iterable $processors) {

  }


  /**
   * @inheritDoc
   */
  public function process(string $file_path): void {
    foreach ($this->processors as $processor) {
      $processor->process($file_path);
    }
  }

}
