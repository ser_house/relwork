<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2019
 * Time: 13:02
 */

namespace Core\File\Image\UploadProcessor;

use Imagick;

class SizeProcessor implements IProcessor {
  public function __construct(
    private readonly int $size_max = 300,
  ) {
  }


  /**
   * @inheritDoc
   */
  public function process(string $file_path): void {
    $img = new Imagick($file_path);

    $width = $img->getImageWidth();
    $height = $img->getImageHeight();

    if ($width === $height) {
      $newHeight = $this->size_max;
      $newWidth = $this->size_max;
    }
    elseif ($width > $height) {
      $newHeight = $this->size_max;
      $newWidth = ($this->size_max / $height) * $width;
    }
    else {
      $newWidth = $this->size_max;
      $newHeight = ($this->size_max / $width) * $height;
    }

    $img->resizeImage($newWidth, $newHeight, Imagick::FILTER_CATROM, 0.9);
    $img->writeImage($file_path);
  }

}
