<?php

namespace Core\File\Image\Rules;

use Core\File\Rules as FileRules;

class Avatar extends FileRules {

  protected const TYPES = [
    'image/jpeg' => 'jpg,jpeg',
    'image/png' => 'png',
    'image/webp' => 'webp',
  ];

  protected const MAX_FILE_SIZE_BYTES = 1024 * 2;
}
