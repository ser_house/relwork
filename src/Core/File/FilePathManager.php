<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.07.2019
 * Time: 10:02
 */


namespace Core\File;

use Core\User\UserId;
use RuntimeException;
use SplFileInfo;

/**
 * Class FilePathManager
 */
class FilePathManager {

	public function __construct(
    public readonly string $public_dir,
    public readonly string $temp_public_files_dir,
    public readonly string $temp_public_files_uri,
    public readonly string $public_files_dir,
    public readonly string $public_files_uri
  ) {

	}

	/**
	 * @param string $uri
	 *
	 * @return string
	 */
	public function getPublicFilePathFromUri(string $uri): string {
		return "$this->public_dir$uri";
	}

	/**
	 * @param string $file_name
	 *
	 * @return string
	 */
	public function getTempPublicFilePath(string $file_name): string {
		return "$this->temp_public_files_dir/$file_name";
	}

	/**
	 * @param string $file_name
	 *
	 * @return string
	 */
	public function getTempPublicFileUri(string $file_name): string {
		return "$this->temp_public_files_uri/$file_name";
	}

	/**
	 * @param string $relative_path
	 *
	 * @return string
	 */
	public function getAbsoluteFilePathFromRelative(string $relative_path): string {
		return "$this->public_files_dir$relative_path";
	}

	/**
	 * @param string $absolute_path
	 *
	 * @return string
	 */
	public function getRelativeFilePathFromAbsolutePath(string $absolute_path): string {
		$file = new SplFileInfo($absolute_path);
		$path_to_dir = $file->getPath();
		return str_replace($this->public_files_dir, '', $path_to_dir);
	}

  /**
   * @param string $file_path
   *
   * @return string
   */
	public function getPublicFileUri(string $file_path): string {
		return $this->getRelativeFilePathFromAbsolutePath($file_path);
	}

	public function getUserTempFilesPublicDir(UserId $userId): string {
		return "$this->temp_public_files_dir/$userId";
	}

	/**
	 * @param UserId $userId
	 *
	 * @return string
	 */
	public function getUserFilesCurrentSubDir(UserId $userId): string {
		return $userId . '/' . $this->getFilesCurrentSubDir();
	}

	public function getDefaultFileUri(string $file_name): string {
		return "$this->public_dir$file_name";
	}

  /**
   * Проверяет директорию на наличие и создаёт, если той еще нет.
   *
   * @param string $directory
   */
  public function testDirectory(string $directory): void {
    if (!file_exists($directory) && !is_dir($directory)) {
      if (!mkdir($directory, 0775, true) && !is_dir($directory)) {
        throw new RuntimeException(sprintf('Directory "%s" was not created', $directory));
      }
      chmod($directory, 0775);
    }
  }

	/**
	 *
	 * @return string
	 */
	private function getFilesCurrentSubDir(): string {
//		return md5(date('Y')) . '/' . md5(date('m')) . '/' . md5(date('d'));
		return date('Y') . '/' . date('m') . '/' . date('d');
	}
}
