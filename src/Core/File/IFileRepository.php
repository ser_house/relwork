<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 0:33
 */

namespace Core\File;


interface IFileRepository {

  /**
   * @param File $file
   *
   * @return void
   */
  public function add(File $file): void;

  /**
   * @param File $file
   *
   * @return void
   */
  public function update(File $file): void;

  /**
   * @param FileId $id
   *
   * @return File|null
   */
  public function findById(FileId $id): ?File;

  /**
   * @param FileId $id
   *
   * @return File
   * @throws FileNotFoundException
   */
  public function getById(FileId $id): File;

  /**
   * @param FileId $id
   *
   * @return void
   */
  public function removeById(FileId $id): void;

  /**
   * @param FileId $id
   *
   * @return string
   */
  public function getFileUri(FileId $id): string;
}
