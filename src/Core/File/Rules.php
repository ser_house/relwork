<?php

namespace Core\File;

use Core\Generic\Infrastructure\Formatter\FileSize as FileSizeFormatter;

abstract class Rules {
  // mime type => extension,extension
  protected const TYPES = [];

  protected const MAX_FILE_SIZE_BYTES = 1024 * 5;
  protected const MAX_FILESIZE = self::MAX_FILE_SIZE_BYTES * 1024;

  public function __construct(protected FileSizeFormatter $fileSizeFormatter) {

  }

  public function rules(): array {
    return [
      'types' => $this->types(),
      'max_filesize' => $this->maxFileSize(),
      'help' => $this->help(),
    ];
  }
  /**
   *
   * @return string[]
   */
  public function help(): array {
    $valid_extensions_str = $this->extensionsAsString();
    return [
      "Допускаются файлы одного из следующих типов: $valid_extensions_str",
      'Максимальный размер файла: ' . $this->fileSizeFormatter->format(static::MAX_FILESIZE),
    ];
  }

  /**
   *
   * @return array
   */
  public function types(): array {
    return static::TYPES;
  }

  /**
   * @param string $delimiter
   *
   * @return string
   */
  public function mimeTypesAsString(string $delimiter = ','): string {
    return implode($delimiter, array_keys($this->types()));
  }

  /**
   *
   * @return array
   */
  public function extensions(): array {
    return array_values($this->types());
  }

  /**
   * @param string $delimiter
   *
   * @return string
   */
  public function extensionsAsString(string $delimiter = ','): string {
    $valid_extensions = $this->extensions();
    return implode($delimiter, $valid_extensions);
  }

  /**
   *
   * @return int
   */
  public function maxFileSize(): int {
    return static::MAX_FILESIZE;
  }

  /**
   *
   * @return int
   */
  public function maxFileSizeBytes(): int {
    return static::MAX_FILE_SIZE_BYTES;
  }
}
