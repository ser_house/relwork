<?php

namespace Core\File;

interface IMovableFile {

  /**
   * @return string
   */
  public function getFilename(): string;

  /**
   * @return string
   */
  public function getPath(): string;

  /**
   * @param string $dir
   *
   * @return string real path of moved file
   */
  public function moveTo(string $dir): string;
}
