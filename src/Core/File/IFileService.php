<?php

namespace Core\File;

use Core\User\UserId;

interface IFileService {

  /**
   * @param UserId $userId
   * @param IMovableFile $file
   *
   * @return PublicFile
   */
  public function moveToPublic(UserId $userId, IMovableFile $file): PublicFile;

  /**
   * @param string $path
   *
   * @return void
   */
  public function remove(string $path): void;

  /**
   * @param string $relative_path
   *
   * @return void
   */
  public function removeByRelativePath(string $relative_path): void;
}
