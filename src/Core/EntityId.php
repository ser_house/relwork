<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.02.2019
 * Time: 20:43
 */

namespace Core;


use Ramsey\Uuid\Uuid;

class EntityId {

  public function __construct(protected string $value = '') {
    $this->value = empty($value) ? (string)Uuid::uuid4() : $value;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }
}
