<?php


namespace Core;


interface ITransaction {

  public function begin(): void;

  public function commit(): void;

  public function rollback(): void;
}
