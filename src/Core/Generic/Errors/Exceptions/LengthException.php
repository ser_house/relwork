<?php

namespace Core\Generic\Errors\Exceptions;

use DomainException;

class LengthException extends DomainException {
  public readonly int $len;
  /**
   * @inheritDoc
   */
  public function __construct(
    public readonly string $value,
    public readonly int $min,
    public readonly int $max,
  ) {
    $this->len = mb_strlen($this->value);
    parent::__construct();
  }
}
