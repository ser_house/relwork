<?php

namespace Core\Generic\Errors\Exceptions;


use DomainException;

class InvalidInputDataException extends DomainException {

  public function __construct(public readonly FieldsExceptionsCollection $exceptionsCollection) {
    parent::__construct('Invalid input data.');
  }

  /**
   * @return FieldsExceptionsCollection
   */
  public function getExceptions(): FieldsExceptionsCollection {
    return $this->exceptionsCollection;
  }
}
