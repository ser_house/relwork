<?php

namespace Core\Generic\Errors\Exceptions;

use DomainException;

class MissingValueException extends DomainException {
  /**
   * MissingValueException constructor.
   *
   * @param string $key
   */
  public function __construct(private string $key) {
    parent::__construct();
  }

  /**
   * @return string
   */
  public function getKey(): string {
    return $this->key;
  }
}
