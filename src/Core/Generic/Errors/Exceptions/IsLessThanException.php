<?php

namespace Core\Generic\Errors\Exceptions;

use DomainException;

class IsLessThanException extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(
    public readonly mixed $value,
    public readonly mixed $restrict,
  ) {
    parent::__construct();
  }
}
