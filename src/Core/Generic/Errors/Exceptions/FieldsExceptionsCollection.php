<?php

namespace Core\Generic\Errors\Exceptions;


use ArrayIterator;
use IteratorAggregate;
use Throwable;
use Traversable;

class FieldsExceptionsCollection implements IteratorAggregate {
  private const MULTI_KEYS_DELIMITER = '|';

  private array $exceptions = [];


  public function __construct(public readonly ?string $translate_domain_name = null) {}


  /**
   * @param Throwable $exception
   * @param string ...$keys
   *
   * @return $this
   */
  public function addException(Throwable $exception, string ...$keys): self {
    if (count($keys) > 1) {
      $key = implode(self::MULTI_KEYS_DELIMITER, $keys);
    }
    else {
      $key = reset($keys);
    }

    if (!isset($this->exceptions[$key])) {
      $this->exceptions[$key] = [];
    }
    $this->exceptions[$key][] = $exception;

    return $this;
  }

  /**
   * @param FieldsExceptionsCollection $exceptionsCollection
   * @param string ...$keys
   *
   * @return $this
   */
  public function addExceptionsCollection(FieldsExceptionsCollection $exceptionsCollection, string ...$keys): self {
    if (count($keys) > 1) {
      $base_key = implode(self::MULTI_KEYS_DELIMITER, $keys);
    }
    else {
      $base_key = reset($keys);
    }

    foreach($exceptionsCollection->getExceptions() as $exceptions) {
      if (!isset($this->exceptions[$base_key])) {
        $this->exceptions[$base_key] = [];
      }

      $this->exceptions[$base_key] = array_merge($this->exceptions[$base_key], $exceptions);
    }

    return $this;
  }

  /**
   * @param string $keys
   *
   * @return array
   */
  public function keysToArray(string $keys): array {
    return explode(self::MULTI_KEYS_DELIMITER, $keys);
  }

  /**
   * @return array
   */
  public function getExceptions(): array {
    return $this->exceptions;
  }

  /**
   * @return bool
   */
  public function hasExceptions(string ...$keys): bool {
    if (empty($keys)) {
      return !empty($this->exceptions);
    }
    if (count($keys) > 1) {
      $base_key = implode(self::MULTI_KEYS_DELIMITER, $keys);
    }
    else {
      $base_key = reset($keys);
    }

    return !empty($this->exceptions[$base_key]);
  }

  /**
   * @inheritDoc
   */
  public function getIterator(): Traversable {
    return new ArrayIterator($this->exceptions);
  }
}
