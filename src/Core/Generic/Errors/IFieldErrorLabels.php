<?php

namespace Core\Generic\Errors;

interface IFieldErrorLabels {

  /**
   * @param string $field
   * @param string|null $translate_domain_name
   *
   * @return string
   */
  public function getFromOneField(string $field, string $translate_domain_name = null): string;

  /**
   * @param array $fields
   * @param string|null $translate_domain_name
   *
   * @return array
   */
  public function getFromManyFields(array $fields, string $translate_domain_name = null): array;
}
