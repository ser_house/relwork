<?php

namespace Core\Generic\Errors;


use ArrayIterator;
use IteratorAggregate;
use Traversable;

class FieldErrors implements IteratorAggregate, \JsonSerializable {
  private array $errors = [];

  /**
   * @inheritDoc
   */
  public function addError(string $field, string $error): self {
    if (!isset($this->errors[$field])) {
      $this->errors[$field] = [];
    }

    $this->errors[$field][] = $error;

    return $this;
  }

  /**
   * @inheritDoc
   */
  public function hasErrors(): bool {
    return !empty($this->errors);
  }

  /**
   * @inheritDoc
   */
  public function getErrors(): array {
    return $this->errors;
  }

  public function listErrors(): array {
    $messages = [];
    foreach($this->errors as $field_errors) {
      $messages[] = $field_errors;
    }
    return array_merge(...$messages);
  }

  /**
   * @inheritDoc
   */
  public function getIterator(): Traversable {
    return new ArrayIterator($this->errors);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize(): mixed {
    return $this->errors;
  }
}
