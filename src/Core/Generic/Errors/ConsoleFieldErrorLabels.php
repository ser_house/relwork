<?php

namespace Core\Generic\Errors;

class ConsoleFieldErrorLabels implements IFieldErrorLabels {
  /**
   * @inheritDoc
   */
  public function getFromOneField(string $field, string $translate_domain_name = null): string {
    return $field;
  }

  /**
   * @inheritDoc
   */
  public function getFromManyFields(array $fields, string $translate_domain_name = null): array {
    return $fields;
  }
}
