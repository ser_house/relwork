<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2019
 * Time: 6:28
 */

namespace Core\Generic\Infrastructure;


use InvalidArgumentException;

/**
 * Class EncodeStringService
 */
class EncodeStringService {
	/**
	 * @param string $value
	 *
	 * @return string
	 */
	public function encode(string $value): string {
		return base64_encode($value);
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	public function decode(string $value): string {
		$decoded = base64_decode($value, true);
		if (false === $decoded) {
			throw new InvalidArgumentException("Invalid value: $value");
		}
		return utf8_encode($decoded);
	}
}
