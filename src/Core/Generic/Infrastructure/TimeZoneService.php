<?php

namespace Core\Generic\Infrastructure;

use DateTimeImmutable;
use DateTimeZone;

class TimeZoneService {

  public function listKeyedByName(): array {
    $timezones = [];

    $timezone_identifiers = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, 'RU');
    foreach ($timezone_identifiers as $tz_name) {
      $dateTimeZone = new DateTimeZone($tz_name);
      $dateTime = new DateTimeImmutable('now', $dateTimeZone);

      $offset = $dateTime->format('P');
      $current = $dateTime->format('H:i');

      $timezones[$tz_name] = "$current ($tz_name, $offset)";
    }
    asort($timezones);
    return $timezones;
  }
}
