<?php

namespace Core\Generic\Infrastructure;

use Core\Client\Seller\Level;
use Core\Client\Seller\Rating;
use Core\Client\Settings;
use Core\Description;
use Core\File\File;
use Core\File\FileId;
use Core\Hours;
use Core\Job\Category\Category;
use Core\Job\Category\CategoryId;
use Core\Job\Job;
use Core\Job\JobId;
use Core\Job\State;
use Core\Money\Money;
use Core\Title;
use Core\User\Email\Email;
use Core\User\NickName;
use Core\User\Password\HashedPassword;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

class Hydrator {

  /**
   * @param array $data
   * @param string $class
   *
   * @return object
   * @throws JsonException
   * @throws ReflectionException
   */
  public function hydrate(array $data, string $class): object {
    $refl = new ReflectionClass($class);

    $object = $refl->newInstanceWithoutConstructor();

    $props = $refl->getProperties();
    foreach ($props as $prop) {
      $prop_name = $prop->name;
      if ('events' === $prop_name) {
        continue;
      }

      $db_field_name = $this->propToDbFieldName($prop_name);
      if (isset($data[$db_field_name])) {
        $value = $this->hydratePropValue($prop_name, $data[$db_field_name], $class);
      }
      else {
        $value = null;
      }
      $prop->setValue($object, $value);
    }

    return $object;
  }

  /**
   * @param string $prop
   *
   * @return string
   */
  protected function propToDbFieldName(string $prop): string {
    return match ($prop) {
      'createdAt' => 'created_at',
      'updatedAt' => 'updated_at',
      'timeZone' => 'timezone',
      'userId' => 'user_id',
      'clientId' => 'client_id',
      'categoryId' => 'category_id',
      'parentId' => 'parent_id',
      'imageFileId' => 'image_file_id',
      default => $prop,
    };
  }

  /**
   * @param string $prop_name
   * @param mixed $val
   * @param string $class
   *
   * @return mixed
   * @throws JsonException
   * @throws ReflectionException
   */
  protected function hydratePropValue(string $prop_name, mixed $val, string $class): mixed {
    $explicitValue = $this->getExplicitValue($prop_name, $val);
    if (null !== $explicitValue) {
      return $explicitValue;
    }

    $propRefl = $this->getPropReflection($prop_name, $class);

    $prop = $propRefl->newInstanceWithoutConstructor();
    $propVal = new ReflectionProperty($prop, 'value');
    $propVal->setValue($prop, $val);

    return $prop;
  }

  /**
   * @param string $prop_name
   * @param mixed $val
   *
   * @return mixed
   * @throws JsonException
   */
  protected function getExplicitValue(string $prop_name, mixed $val): mixed {
    switch ($prop_name) {
      case 'relative_directory':
      case 'uri':
        return $val;

      case 'settings':
        $dbSettings = json_decode($val, true, 512, JSON_THROW_ON_ERROR);
        return new Settings($dbSettings['universal']);

      case 'balance':
      case 'reserved':
      case 'cost':
        return new Money($val);

      case 'role':
        return Role::from($val);

      case 'level':
        return Level::from($val);

      case 'createdAt':
      case 'updatedAt':
        return new DateTimeImmutable($val);

      case 'timeZone':
        return new DateTimeZone($val);

      case 'state':
        return State::from($val);

      default:
        return null;
    }
  }

  /**
   * @param string $prop_name
   * @param string $class
   *
   * @return ReflectionClass
   * @throws Exception
   */
  protected function getPropReflection(string $prop_name, string $class): ReflectionClass {
    return match ($prop_name) {
      'id' => $this->getIdReflection($class),
      'userId' => new ReflectionClass(UserId::class),
      'name' => new ReflectionClass(NickName::class),
      'email' => new ReflectionClass(Email::class),
      'password' => new ReflectionClass(HashedPassword::class),
      'avatarId' => new ReflectionClass(FileId::class),
      'title' => new ReflectionClass(Title::class),
      'description' => new ReflectionClass(Description::class),
      'parentId' => new ReflectionClass(CategoryId::class),
      'rating' => new ReflectionClass(Rating::class),
      'imageFileId' => new ReflectionClass(FileId::class),
      'hours' => new ReflectionClass(Hours::class),
      'clientId' => new ReflectionClass(UserId::class),
      'categoryId' => new ReflectionClass(CategoryId::class),
      default => throw new Exception("Unknown prop '$prop_name'"),
    };
  }

  /**
   * @param string $class
   *
   * @return ReflectionClass
   * @throws Exception
   */
  protected function getIdReflection(string $class): ReflectionClass {
    return match($class) {
      User::class => new ReflectionClass(UserId::class),
      Job::class => new ReflectionClass(JobId::class),
      Category::class => new ReflectionClass(CategoryId::class),
      File::class => new ReflectionClass(FileId::class),
      default => throw new Exception("Class $class has not property with id class."),
    };
  }
}
