<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.09.2019
 * Time: 6:42
 */


namespace Core\Generic\Infrastructure;


/**
 * Class StaticCache
 */
class StaticCache {

  private array $cache = [];

  /**
   * @param string $key
   * @param $data
   */
  public function set(string $key, $data): void {
    $this->cache[$key] = $data;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function get(string $key) {
    if ($this->has($key)) {
      return $this->cache[$key];
    }

    return null;
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  public function has(string $key): bool {
    return array_key_exists($key, $this->cache);
  }

  /**
   * @param string $key
   */
  public function flush(string $key): void {
    unset($this->cache[$key]);
  }

  /**
   * @return void
   */
  public function clear(): void {
    $this->cache = [];
  }
}
