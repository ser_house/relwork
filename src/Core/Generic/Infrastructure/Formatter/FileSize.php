<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.05.2020
 * Time: 3:54
 */


namespace Core\Generic\Infrastructure\Formatter;


/**
 * Class FileSize
 */
class FileSize {

  /**
   * @param int $bytes
   *
   * @return string
   */
  public function format(int $bytes): string {
    if ($bytes >= 1000000000) {
      return round($bytes / 1000000000) . ' Gb';
    }
    if ($bytes >= 1000000) {
      return round($bytes / 1000000) . ' Mb';
    }

    return round($bytes / 1000) . ' Kb';
  }
}
