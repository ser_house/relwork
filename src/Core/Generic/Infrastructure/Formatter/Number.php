<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.05.2020
 * Time: 9:34
 */


namespace Core\Generic\Infrastructure\Formatter;


class Number {

  public function floatFromInput(string $input): float {
    $value = str_replace([' ', ','], ['', '.'], $input);
    return number_format($value, 2, '.', '');
  }

  public function formatAmount(float $amount): string {
    return number_format($amount, 2, ',', ' ');
  }
}
