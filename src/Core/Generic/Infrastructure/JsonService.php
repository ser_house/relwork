<?php

namespace Core\Generic\Infrastructure;

use JsonException;
use stdClass;

/**
 * Обертка для более удобной работы с json.
 */
class JsonService {

  /**
   * @param mixed $data
   *
   * @return string
   * @throws JsonException
   */
  public function encode(mixed $data): string {
    return json_encode($data, JSON_THROW_ON_ERROR|JSON_UNESCAPED_UNICODE);
  }
  /**
   * @param string $json
   *
   * @return stdClass
   * @throws JsonException
   */
  public function decodeAsObj(string $json): stdClass {
    return json_decode($json, false, 512, JSON_THROW_ON_ERROR);
  }

  /**
   * @param string $json
   *
   * @return array
   * @throws JsonException
   */
  public function decodeAsArray(string $json): array {
    return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
  }
}
