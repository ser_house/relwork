<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 9:22
 */

namespace Core;


/**
 * Class Hours
 */
class Hours {

	public function __construct(public readonly int $value) {

	}

	/**
	 * @inheritDoc
	 */
	public function __toString() {
		return (string)$this->value;
	}
}
