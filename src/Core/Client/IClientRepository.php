<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:05
 */

namespace Core\Client;


use Core\User\Role;
use Core\User\UserId;

interface IClientRepository {
	/**
	 * @param UserId $userId
	 *
	 * @return Client|null
	 */
	public function findByUserId(UserId $userId): ?Client;

  /**
   * @param UserId $userId
   * @param Role $newRole
   *
   * @return void
   */
	public function updateRole(UserId $userId, Role $newRole): void;
}
