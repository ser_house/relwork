<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 11:03
 */

namespace Core\Client\Buyer;

use Core\Client\Exception\NotEnoughAmount;
use Core\Client\Settings;
use Core\Events\Events;
use Core\Events\IEventable;
use Core\Events\IEventInitiator;
use Core\Money\Money;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;

class Buyer implements IEventInitiator, IEventable {
  use Events;

  public function __construct(
    public readonly User $user,
    public readonly Settings $settings = new Settings(),
    public readonly Money $balance = new Money(),
    protected Money $reserved = new Money(),
  ) {
  }

  /**
   * @inheritDoc
   */
  public function getId(): UserId {
    return $this->user->id;
  }

  /**
   * @inheritDoc
   */
  public function getRole(): Role {
    return Role::BUYER;
  }

  /**
   * @return Money
   */
  public function getReserved(): Money {
    return $this->reserved;
  }

  /**
   *
   * @return Money
   */
  public function getAvailableMoney(): Money {
    return $this->balance->subtract($this->reserved);
  }

  /**
   * @param Money $money
   *
   * @return Buyer
   * @throws NotEnoughAmount
   */
  public function reserveMoney(Money $money): self {
    $available = $this->getAvailableMoney();
    if ($available->lessThan($money)) {
      throw new NotEnoughAmount('Not enough amount on balance.');
    }

    $this->reserved = $this->reserved->add($money);
    return $this;
  }
}
