<?php

namespace Core\Client\Buyer;

use Core\Client\Buyer\Presenter\BuyerPresenter;
use Core\Client\Presenters\ClientPresenter;
use Core\File\IFileRepository;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\User\Context\BaseUserContextFactory;
use Core\User\User;

class BuyerContextFactory extends BaseUserContextFactory {
  public function __construct(
    IFileRepository $fileRepository,
    private IBuyerRepository $buyerRepository,
    private ClientPresenter $clientPresenter,
    private Number $numberFormatter,
  ) {
    parent::__construct($fileRepository);
  }

  /**
   * @param User $user
   *
   * @return BuyerContext
   */
  protected function buildContext(User $user): BuyerContext {
    $key = "context_$user->id";
    if (!$this->cache->has($key)) {
      $buyer = $this->buyerRepository->getByUserId($user->id);
      $this->cache->set($key, new BuyerContext($buyer));
    }
    return $this->cache->get($key);
  }

  /**
   * @param User $user
   *
   * @return BuyerPresenter
   */
  protected function buildPresenter(User $user): BuyerPresenter {
    $key = "presenter_$user->id";
    if (!$this->cache->has($key)) {
      $this->cache->set($key, new BuyerPresenter(
        $this->clientPresenter,
        $this->numberFormatter,
        $this->buildContext($user),
      ));
    }
    return $this->cache->get($key);
  }
}
