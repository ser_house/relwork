<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2019
 * Time: 5:35
 */

namespace Core\Client\Buyer\Presenter;


use Core\Client\Buyer\BuyerContext;
use Core\Client\Buyer\View\BuyerMenuView;
use Core\Client\Buyer\View\ProfileView;
use Core\Client\Presenters\ClientPresenter;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\User\IUserPresenter;

class BuyerPresenter implements IUserPresenter {

  public function __construct(
    private ClientPresenter $clientPresenter,
    private Number $numberFormatter,
    private BuyerContext $context,
  ) {
  }

  /**
   *
   * @return BuyerMenuView
   */
  public function presentMenu(): BuyerMenuView {
    $view = new BuyerMenuView();
    $this->clientPresenter->presentMenu($this->context, $view);

    $balance = $this->context->buyer->balance;
    $view->balance = $this->numberFormatter->formatAmount($balance->getAmount());

    $reserved = $this->context->buyer->getReserved();
    $view->reserved = $this->numberFormatter->formatAmount($reserved->getAmount());

    return $view;
  }

  /**
   * @inheritDoc
   */
  public function presentProfile(): ProfileView {
    $view = new ProfileView();
    $this->clientPresenter->presentProfile($this->context, $view);

    $balance = $this->context->buyer->balance;
    $view->balance = $this->numberFormatter->formatAmount($balance->getAmount());

    $reserved = $this->context->buyer->getReserved();
    $view->reserved = $this->numberFormatter->formatAmount($reserved->getAmount());
    return $view;
  }
}
