<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2019
 * Time: 12:57
 */

namespace Core\Client\Buyer;

use Core\Client\Exception\BuyerNotFoundException;
use Core\User\UserId;

interface IBuyerRepository {
	/**
	 * @param UserId $userId
	 *
	 * @return Buyer|null
	 */
	public function findByUserId(UserId $userId): ?Buyer;

  /**
   * @param UserId $userId
   *
   * @return Buyer
   * @throws BuyerNotFoundException
   */
	public function getByUserId(UserId $userId): Buyer;

	/**
	 * @param Buyer $buyer
	 */
	public function add(Buyer $buyer): void;
}
