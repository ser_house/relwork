<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2019
 * Time: 5:42
 */

namespace Core\Client\Buyer\View;

use Core\Client\View\ClientMenuView;


class BuyerMenuView extends ClientMenuView {

  public string $balance;
  public string $reserved;
}
