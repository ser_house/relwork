<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 13:32
 */

namespace Core\Client\Buyer;

use Core\User\Context\IUserContext;
use Core\User\User;

class BuyerContext implements IUserContext {

  public function __construct(
    public readonly Buyer $buyer,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function user(): User {
    return $this->buyer->user;
  }
}
