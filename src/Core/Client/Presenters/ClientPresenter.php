<?php

namespace Core\Client\Presenters;

use Core\Client\View\ClientMenuView;
use Core\Client\View\ProfileView;
use Core\User\Context\IUserContext;

class ClientPresenter {

  public function presentMenu(IUserContext $context, ClientMenuView $view): void {
    $user = $context->user();
    $view->name = (string)$user->name;
    $view->avatar_uri = "/files/{$user->avatar->uri}";
    $view->role = $user->role->title();
  }

  public function presentProfile(IUserContext $context, ProfileView $view): void {
    $user = $context->user();
    $view->name = (string)$user->name;
    $view->avatar_uri = "/files/{$user->avatar->uri}";
    $view->role = $user->role->title();
  }
}
