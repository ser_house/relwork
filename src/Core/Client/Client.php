<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.01.2019
 * Time: 6:14
 */

namespace Core\Client;

use Core\Client\Seller\Level;
use Core\Client\Seller\Rating;
use Core\Money\Money;
use Core\User\Role;
use Core\User\UserId;


/**
 * Class Client
 */
class Client {

	public function __construct(
    public readonly UserId $userId,
    public readonly Settings $settings,
    public readonly Role $role,
    private Money $balance,
    public readonly Money $reserved,
    public readonly Rating $rating,
    public readonly Level $level,
  ) {

	}

	/**
	 * @return Money
	 */
	public function getBalance(): Money {
		return $this->balance;
	}

	/**
	 * @return Money
	 */
	public function getReserved(): Money {
		return $this->reserved;
	}

	/**
	 * @return Rating
	 */
	public function getRating(): Rating {
		return $this->rating;
	}

	/**
	 * @return Level
	 */
	public function getLevel(): Level {
		return $this->level;
	}

	/**
	 *
	 * @return Money
	 */
	public function getAvailableMoney(): Money {
		return $this->balance;
	}

	/**
	 * @param Money $money
	 *
	 * @return Client
	 */
	public function addMoney(Money $money): self {
		$this->balance = $this->balance->add($money);
		return $this;
	}
}
