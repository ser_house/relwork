<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2019
 * Time: 12:57
 */

namespace Core\Client\Seller;

use Core\Client\Exception\SellerNotFoundException;
use Core\User\UserId;

interface ISellerRepository {
	/**
	 * @param UserId $userId
	 *
	 * @return Seller|null
	 */
	public function findByUserId(UserId $userId): ?Seller;

  /**
   * @param UserId $userId
   *
   * @return Seller
   * @throws SellerNotFoundException
   */
	public function getByUserId(UserId $userId): Seller;

	/**
	 * @param Seller $seller
	 */
	public function add(Seller $seller): void;
}
