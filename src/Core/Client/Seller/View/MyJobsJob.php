<?php


namespace Core\Client\Seller\View;

class MyJobsJob {

	public string $job_id;
	public string $img_url;
	public string $title;
	public string $created_at;
	public string $category;
	public string $state;
	public string $cost;
}
