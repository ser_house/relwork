<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:38
 */

namespace Core\Client\Seller\View;

class JobPageView {

	public string $job_id;
	public string $title;
	public string $img_url;
	public string $created_at;
	public string $category;
	public string $description;
	public string $state;
	public string $cost;
	public string $hours;
}
