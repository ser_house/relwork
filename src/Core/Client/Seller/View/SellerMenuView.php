<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 13:38
 */

namespace Core\Client\Seller\View;

use Core\Client\View\ClientMenuView;

class SellerMenuView extends ClientMenuView {

  public string $balance;
  public string $level;

}
