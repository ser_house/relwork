<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 13:38
 */

namespace Core\Client\Seller\View;

use Core\Client\View\ProfileView as ClientProfileView;


class ProfileView extends ClientProfileView {

  public string $balance;
  public string $level;
}
