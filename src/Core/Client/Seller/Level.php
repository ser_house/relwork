<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.01.2019
 * Time: 6:19
 */

namespace Core\Client\Seller;


use Core\Title;

enum Level : int {
  case NEWBIE = 0;
  case VETERAN = 1;
  case MASTER = 2;
  case EXPERT = 3;

	/**
	 * @return Title
	 */
	public function title(): Title {
		return match($this) {
      self::NEWBIE => new Title('Новичок'),
      self::VETERAN => new Title('Опытный'),
      self::MASTER => new Title('Мастер'),
      self::EXPERT => new Title('Эксперт'),
    };
	}


	/**
	 *
	 * @return Level|null
	 */
	public function nextLevel(): ?Level {
		$next_value = $this->value + 1;
    if ($next_value > self::EXPERT->value) {
      // @todo: throw exception
      return null;
    }
    return self::from($next_value);
	}

	/**
	 *
	 * @return Level|null
	 */
	public function prevLevel(): ?Level {
		$prev_value = $this->value - 1;
    if ($prev_value < self::NEWBIE->value) {
      // @todo: throw exception
      return null;
    }
    return self::from($prev_value);
	}
}
