<?php

namespace Core\Client\Seller\UseCases\AddJob;

use Core\Client\Seller\ISellerRepository;
use Core\Events\IEventDispatcher;
use Core\File\IFileRepository;
use Core\File\IFileService;
use Core\ITransaction;
use Core\Job\IJobRepository;
use Core\Job\Job;
use Core\Job\Request\JobAddRequest;
use Core\User\User;
use Core\User\UserId;
use Exception;

class AddJobService {
  public function __construct(
    private ISellerRepository $sellerRepository,
    private IJobRepository $jobRepository,
    private IFileRepository $fileRepository,
    private IFileService $fileService,
    private IEventDispatcher $eventDispatcher,
    private ITransaction $transaction
  ) {
  }


  /**
   * @param User $user
   * @param JobAddRequest $request
   *
   * @return Job
   */
  public function add(User $user, JobAddRequest $request): Job {
    $seller = $this->sellerRepository->getByUserId($user->id);

    $this->transaction->begin();

    try {
      $job = $seller->addJob($request, $this->jobRepository, $this->fileRepository);
      $this->transaction->commit();

      $events = $seller->releaseEvents();
      $this->eventDispatcher->dispatchEvents($events);

      return $job;
    }
    catch (Exception $e) {
      $this->transaction->rollBack();
      $this->fileService->remove($request->image->getRelativeFilePath());

      throw $e;
    }
  }
}
