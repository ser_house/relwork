<?php

namespace Core\Client\Seller\UseCases\ChangeJob;

use Core\Client\Seller\ISellerRepository;
use Core\Events\IEventDispatcher;
use Core\File\IFileRepository;
use Core\File\IFileService;
use Core\ITransaction;
use Core\Job\IJobRepository;
use Core\Job\Job;
use Core\Job\JobData;
use Core\Job\JobId;
use Core\Job\Request\JobUpdateRequest;
use Core\User\User;
use Core\User\UserId;
use Exception;

class ChangeJobService {
  private Job $job;

  public function __construct(
    private ISellerRepository $sellerRepository,
    private IJobRepository $jobRepository,
    private IFileRepository $fileRepository,
    private IFileService $fileService,
    private IEventDispatcher $eventDispatcher,
    private ITransaction $transaction,
  ) {

  }

  /**
   * @param JobId $jobId
   *
   * @return Job
   */
  public function getJob(JobId $jobId): Job {
    if (!$this->job) {
      $this->job = $this->jobRepository->getByJobId($jobId);
    }
    return $this->job;
  }

  /**
   * @param JobId $jobId
   *
   * @return JobData
   */
  public function getJobDataByJobId(JobId $jobId): JobData {
    $origJob = $this->getJob($jobId);
    return new JobData($origJob, $this->fileRepository);
  }

  /**
   * @param User $user
   * @param JobUpdateRequest $request
   *
   * @return Job
   */
  public function change(User $user, JobData $updatedJobData): Job {
    $this->fixJobDataImageFile($updatedJobData, $this->job, $user->id);
    $updateRequest = new JobUpdateRequest($this->job, $updatedJobData);

    $seller = $this->sellerRepository->getByUserId($user->id);

    $this->transaction->begin();

    try {
      $updatedJob = $seller->updateJob($updateRequest, $this->jobRepository, $this->fileRepository);
      $this->transaction->commit();

      $events = $seller->releaseEvents();
      $this->eventDispatcher->dispatchEvents($events);

      return $updatedJob;
    }
    catch (Exception $e) {
      $this->transaction->rollBack();
      $this->fileService->removeByRelativePath($updateRequest->image->getRelativeFilePath());

      throw $e;
    }
  }

  /**
   * @param JobData $jobData
   * @param Job $origJob
   * @param UserId $userId
   *
   * @return void
   */
  private function fixJobDataImageFile(JobData $jobData, Job $origJob, UserId $userId): void {
    if ($jobData->image->is_temp) {
      $fileUploaded = $this->fileService->moveToPublic($userId, $jobData->image->file);

      $jobData->image->uri = $fileUploaded->uri;
      $jobData->image->relative_directory = $fileUploaded->relative_directory;
    }
    else {
      $image = $this->fileRepository->getById($origJob->imageFileId);
      $jobData->image->uri = $image->uri;
      $jobData->image->relative_directory = $image->relative_directory;
    }
  }
}
