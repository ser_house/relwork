<?php

namespace Core\Client\Seller;

use Core\Client\Presenters\ClientPresenter;
use Core\Client\Seller\Presenter\SellerPresenter;
use Core\File\IFileRepository;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\User\Context\BaseUserContextFactory;
use Core\User\User;

class SellerContextFactory extends BaseUserContextFactory {
  public function __construct(
    IFileRepository $fileRepository,
    private ISellerRepository $sellerRepository,
    private ClientPresenter $clientPresenter,
    private Number $numberFormatter,
  ) {
    parent::__construct($fileRepository);
  }

  /**
   * @param User $user
   *
   * @return SellerContext
   */
  protected function buildContext(User $user): SellerContext {
    $key = "context_$user->id";
    if (!$this->cache->has($key)) {
      $seller = $this->sellerRepository->getByUserId($user->id);
      $this->cache->set($key, new SellerContext($seller));
    }
    return $this->cache->get($key);
  }

  /**
   * @param User $user
   *
   * @return SellerPresenter
   */
  protected function buildPresenter(User $user): SellerPresenter {
    $key = "presenter_$user->id";
    if (!$this->cache->has($key)) {
      $this->cache->set($key, new SellerPresenter(
        $this->clientPresenter,
        $this->numberFormatter,
        $this->buildContext($user),
      ));
    }
    return $this->cache->get($key);
  }
}
