<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.02.2019
 * Time: 13:09
 */

namespace Core\Client\Seller;


class Rating {

	public function __construct(private float $value) {

	}

	/**
	 * @return float
	 */
	public function value(): float {
		return $this->value;
	}

	/**
	 * @param Rating $rating
	 *
	 * @return bool
	 */
	public function equals(Rating $rating): bool {
		return (string)$rating->value() === (string)$this->value();
	}

	/**
	 * @inheritDoc
	 */
	public function __toString() {
		return (string)$this->value();
	}
}
