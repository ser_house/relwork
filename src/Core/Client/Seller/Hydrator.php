<?php

namespace Core\Client\Seller;

use Core\Client\Settings;
use Core\Generic\Infrastructure\Hydrator as GenericHydrator;
use Core\User\Hydrator as UserHydrator;
use Core\Money\Money;
use Core\User\Avatar;
use Core\User\Email\Email;
use Core\User\NickName;
use Core\User\Password\HashedPassword;
use Core\User\Role;
use Core\User\User;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use ReflectionClass;
use ReflectionException;

final class Hydrator extends GenericHydrator {
  public function __construct(private UserHydrator $userHydrator) {
  }


  /**
   * @param array $data
   * @param string $class
   *
   * @return object
   * @throws JsonException
   * @throws ReflectionException
   */
  public function hydrate(array $data, string $class = Seller::class): object {
    $refl = new ReflectionClass($class);

    $object = $refl->newInstanceWithoutConstructor();

    $props = $refl->getProperties();
    foreach ($props as $prop) {
      $prop_name = $prop->name;

      switch ($prop_name) {
        case 'events':
          continue 2;

        case 'user':
          $value = $this->userHydrator->hydrate($data, User::class);
          break;

        default:
          $db_field_name = $this->propToDbFieldName($prop_name);
          if (isset($data[$db_field_name])) {
            $value = $this->hydratePropValue($prop_name, $data[$db_field_name], $class);
          }
          else {
            $value = null;
          }
          break;
      }
      $prop->setValue($object, $value);
    }

    return $object;
  }

  /**
   * @param string $prop
   *
   * @return string
   */
  protected function propToDbFieldName(string $prop): string {
    return match ($prop) {
      'createdAt' => 'created_at',
      'timeZone' => 'timezone',
      default => $prop,
    };
  }

  /**
   * @param string $prop_name
   * @param mixed $val
   *
   * @return mixed
   * @throws JsonException
   */
  protected function getExplicitValue(string $prop_name, mixed $val): mixed {
    switch ($prop_name) {
      case 'settings':
        $dbSettings = json_decode($val, true, 512, JSON_THROW_ON_ERROR);
        return new Settings($dbSettings['universal']);

      case 'balance':
        return new Money($val);

      case 'rating':
        return new Rating($val);

      case 'role':
        return Role::from($val);

      case 'level':
        return Level::from($val);

      case 'createdAt':
        return new DateTimeImmutable($val);

      case 'timeZone':
        return new DateTimeZone($val);

      default:
        return null;
    }
  }

  /**
   * @param string $prop_name
   * @param string $class
   *
   * @return ReflectionClass
   * @throws Exception
   */
  protected function getPropReflection(string $prop_name, string $class): ReflectionClass {
    return match ($prop_name) {
      'name' => new ReflectionClass(NickName::class),
      'email' => new ReflectionClass(Email::class),
      'password' => new ReflectionClass(HashedPassword::class),
      'rating' => new ReflectionClass(Rating::class),
      default => throw new Exception("Unknown prop '$prop_name'"),
    };
  }
}
