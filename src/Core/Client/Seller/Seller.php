<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 11:03
 */

namespace Core\Client\Seller;

use Core\Client\Exception\ImpossibleToDownLevel;
use Core\Client\Exception\ImpossibleToUpLevel;
use Core\Client\Settings;
use Core\Events\Events;
use Core\Events\IEventable;
use Core\Events\IEventInitiator;
use Core\File\Events\FileAddedEvent;
use Core\File\Events\FileUpdatedEvent;
use Core\File\File;
use Core\File\IFileRepository;
use Core\Job\Events\JobAddedEvent;
use Core\Job\Events\JobUpdatedEvent;
use Core\Job\IJobRepository;
use Core\Job\Job;
use Core\Job\Request\JobAddRequest;
use Core\Job\Request\JobUpdateRequest;
use Core\Money\Money;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;

/**
 * Class Seller
 */
class Seller implements IEventInitiator, IEventable {
  use Events;

  public function __construct(
    public readonly User $user,
    public readonly Settings $settings = new Settings(),
    private Money $balance = new Money(),
    private Rating $rating = new Rating(1.0),
    private Level $level = Level::NEWBIE,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function getId(): UserId {
    return $this->user->id;
  }

  /**
   * @inheritDoc
   */
  public function getRole(): Role {
    return Role::SELLER;
  }

  /**
   * @return Rating
   */
  public function getRating(): Rating {
    return $this->rating;
  }

  /**
   * @return Money
   */
  public function getBalance(): Money {
    return $this->balance;
  }

  /**
   *
   * @return Money
   */
  public function getAvailableMoney(): Money {
    return $this->balance;
  }

  /**
   * @param Money $money
   *
   * @return Seller
   */
  public function addMoney(Money $money): self {
    $this->balance = $this->getBalance()->add($money);
    return $this;
  }

  public function addJob(
    JobAddRequest $request,
    IJobRepository $jobRepository,
    IFileRepository $fileRepository
  ): Job {

    $uploadedImageFile = $request->image;
    $imageFile = new File(
      $this->getId(),
      $uploadedImageFile->name,
      $uploadedImageFile->relative_directory,
      $uploadedImageFile->uri,
    );

    $job = new Job(
      $this->getId(),
      $request->categoryId,
      $request->title,
      $request->description,
      $imageFile->id,
      $request->state,
      $request->cost,
      $request->hours
    );

    $fileRepository->add($imageFile);
    $this->addEvent(new FileAddedEvent($this, $imageFile->id));

    $jobRepository->add($job);

    $this->addEvent(new JobAddedEvent($this, $job->id));
    return $job;
  }

  public function updateJob(
    JobUpdateRequest $request,
    IJobRepository $jobRepository,
    IFileRepository $fileRepository,
  ): Job {

    $userId = $this->getId();

    $oldJob = $request->origJob;
    $jobId = $oldJob->id;

    $imageFileId = $oldJob->imageFileId;

    $origImageFile = $fileRepository->findById($imageFileId);
    $fileRepository->update($request->image);

    $this->addEvent(new FileUpdatedEvent($this, $imageFileId));

    // @todo: это для события, но надо сделать не так:
    // id, объект с измененными полями.
    // А то и вовсе разные события на изменения в разных полях.
    $updatedJob = new Job(
      $userId,
      $request->categoryId,
      $request->title,
      $request->description,
      $imageFileId,
      $request->state,
      $request->cost,
      $request->hours
    );

    $jobRepository->update(
      $jobId,
      $request->categoryId,
      $request->title,
      $request->description,
      $imageFileId,
      $request->state,
      $request->cost,
      $request->hours
    );
    $this->addEvent(new JobUpdatedEvent($this, $oldJob, $origImageFile, [], $updatedJob, $request->image, []));
    return $updatedJob;
  }

  /**
   * @param IJobRepository $jobRepository
   *
   * @return Job[]
   */
  public function getJobs(IJobRepository $jobRepository): array {
    return $jobRepository->findBySellerId($this->getId());
  }

  /**
   * @return Level
   */
  public function getLevel(): Level {
    return $this->level;
  }

  /**
   *
   * @return Level
   * @throws ImpossibleToUpLevel
   */
  public function levelUp(): Level {
    $currentLevel = $this->getLevel();
    $nextLevel = $currentLevel->nextLevel();
    if (null === $nextLevel) {
      throw new ImpossibleToUpLevel('The level can not be higher than the current.');
    }

    $this->level = $nextLevel;
    return $this->getLevel();
  }

  /**
   *
   * @return Level
   * @throws ImpossibleToDownLevel
   */
  public function levelDown(): Level {
    $currentLevel = $this->getLevel();
    $prevLevel = $currentLevel->prevLevel();
    if (null === $prevLevel) {
      throw new ImpossibleToDownLevel('The level can not be lower than the current.');
    }

    $this->level = $prevLevel;
    return $this->getLevel();
  }
}
