<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2019
 * Time: 5:35
 */

namespace Core\Client\Seller\Presenter;

use Core\Client\Presenters\ClientPresenter;
use Core\Client\Seller\SellerContext;
use Core\Client\Seller\View\ProfileView;
use Core\Client\Seller\View\SellerMenuView;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\User\IUserPresenter;

class SellerPresenter implements IUserPresenter {

  public function __construct(
    private ClientPresenter $clientPresenter,
    private Number $numberFormatter,
    private SellerContext $context,
  ) {

  }

  /**
   *
   * @return SellerMenuView
   */
  public function presentMenu(): SellerMenuView {
    $view = new SellerMenuView();
    $this->clientPresenter->presentMenu($this->context, $view);

    $balance = $this->context->seller->getBalance();
    $view->balance = $this->numberFormatter->formatAmount($balance->getAmount());

    $view->level = $this->context->seller->getLevel()->title();

    return $view;
  }

  /**
   * @inheritDoc
   */
  public function presentProfile(): ProfileView {
    $view = new ProfileView();
    $this->clientPresenter->presentProfile($this->context, $view);

    $balance = $this->context->seller->getBalance();
    $view->balance = $this->numberFormatter->formatAmount($balance->getAmount());

    $view->level = $this->context->seller->getLevel()->title();

    return $view;
  }
}
