<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:38
 */

namespace Core\Client\Seller\Presenter;


use Core\Client\Seller\View\JobPageView;
use Core\File\IFileRepository;
use Core\Generic\Infrastructure\Formatter\DateTime;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\Job\Category\Category;
use Core\Job\Job;

class JobPagePresenter {

  public function __construct(
    private DateTime $dateTimeFormatter,
    private Number $numberFormatter,
  ) {
  }

  public function present(IFileRepository $fileRepository, Job $job, Category $category): JobPageView {
    $view = new JobPageView();

    $view->job_id = (string)$job->id;
    $view->title = (string)$job->title;

    $imageFile = $fileRepository->getById($job->imageFileId);

    $view->img_url = "/files/$imageFile->uri";

    $view->created_at = $this->dateTimeFormatter->dateTimeShort($job->createdAt);
    $view->category = (string)$category->title;
    $view->description = (string)$job->description;
    $view->state = (string)$job->state->title();
    $view->cost = $this->numberFormatter->formatAmount($job->cost->getAmount());

    $view->hours = $this->dateTimeFormatter->hours($job->hours->value);

    return $view;
  }
}
