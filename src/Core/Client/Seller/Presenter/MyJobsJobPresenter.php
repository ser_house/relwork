<?php


namespace Core\Client\Seller\Presenter;


use Core\Client\Seller\View\MyJobsJob as JobView;
use Core\File\File;
use Core\Generic\Infrastructure\Formatter\DateTime;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\Job\Category\Category;
use Core\Job\Job;


class MyJobsJobPresenter {
  public function __construct(
    private DateTime $dateTimeFormatter,
    private Number $numberFormatter,
  ) {
  }

	/**
	 * @return JobView
	 */
	public function present(Job $job,Category $category,File $image,): JobView {
		$view = new JobView();

		$view->job_id = (string)$job->id;
		$view->img_url = "/files/$image->uri";

		$view->title = (string)$job->title;

		$view->created_at = $this->dateTimeFormatter->dateTimeShort($job->createdAt);
		$view->category = (string)$category->title;
		$view->state = (string)$job->state->title();

		$view->cost = $this->numberFormatter->formatAmount($job->cost->getAmount());

		return $view;
	}
}
