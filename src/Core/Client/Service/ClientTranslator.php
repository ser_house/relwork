<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 23:36
 */

namespace Core\Client\Service;

use Core\Client\Buyer\Buyer;
use Core\Client\Client;
use Core\Client\IClientRepository;
use Core\Client\Seller\Seller;
use Core\User\Role;

/**
 * Class ClientTranslator
 */
class ClientTranslator {

	public function __construct(private IClientRepository $clientRepository) {

	}


	/**
	 * @param Client $client
	 *
	 * @return Buyer
	 */
	public function toBuyer(Client $client): Buyer {
		$this->clientRepository->updateRole($client->userId, Role::BUYER);
		return new Buyer($client->userId, $client->settings, $client->getBalance(), $client->getReserved());
	}

	/**
	 * @param Client $client
	 *
	 * @return Seller
	 */
	public function toSeller(Client $client): Seller {
		$this->clientRepository->updateRole($client->userId, Role::SELLER);
		return new Seller($client->userId, $client->settings, $client->getBalance(), $client->getRating(), $client->getLevel());
	}
}
