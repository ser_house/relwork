<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.02.2019
 * Time: 8:19
 */

namespace Core\Client\Service;

use Core\Client\Buyer\IBuyerRepository;
use Core\Client\Events\RoleChangedEvent;
use Core\Client\Exception\InvalidRole;
use Core\Client\Seller\ISellerRepository;
use Core\Events\IEventDispatcher;
use Core\User\Exceptions\UserNotFoundException;
use Core\User\IUserRepository;
use Core\User\Role;
use Core\User\UserId;

/**
 * Class ClientToggleRole
 */
class ClientToggleRole {

	public function __construct(
    private IUserRepository $userRepository,
    private ISellerRepository $sellerRepository,
    private IBuyerRepository $buyerRepository,
    private IEventDispatcher $eventDispatcher,
  ) {
	}

	/**
	 * @param UserId $userId
	 *
	 * @return Role
	 * @throws InvalidRole
	 */
	public function toggle(UserId $userId): Role {
    // @todo: здесь переключается только текущий пользователь.
    // Надо, чтобы не только текущий
    // (модератор, администратор, по идее, тоже должны иметь возможность переключать роли).
		$user = $this->userRepository->findByUserId($userId);
		if (null === $user) {
			throw new UserNotFoundException("User not found: $userId");
		}

		if (Role::SELLER !== $user->role && Role::BUYER !== $user->role) {
			throw new InvalidRole("Invalid client's role.");
		}

    if (Role::SELLER === $user->role) {
      $newRole = Role::BUYER;
      $initiator = $this->sellerRepository->getByUserId($userId);
    }
    else {
      $newRole = Role::SELLER;
      $initiator = $this->buyerRepository->getByUserId($userId);
    }

		$this->userRepository->updateRole($user->id, $newRole);
    $this->eventDispatcher->dispatch(new RoleChangedEvent($initiator, $user->id, $user->role, $newRole));

		return $newRole;
	}
}
