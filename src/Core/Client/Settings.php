<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.02.2019
 * Time: 2:20
 */

namespace Core\Client;


use JsonSerializable;

class Settings implements JsonSerializable {

	public function __construct(public bool $universal = false) {

	}


	/**
	 * @inheritDoc
	 */
	public function jsonSerialize(): array {
		return [
			'universal' => $this->universal,
		];
	}
}
