<?php

namespace Core\Client\View;

class ProfileView {
  public string $name;
  public string $avatar_uri;
  public string $role;
}
