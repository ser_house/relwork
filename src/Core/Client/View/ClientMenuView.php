<?php

namespace Core\Client\View;

class ClientMenuView {
  public string $name;
  public string $avatar_uri;
  public string $role;
}
