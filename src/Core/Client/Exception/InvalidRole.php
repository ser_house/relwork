<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:12
 */

namespace Core\Client\Exception;


use DomainException;

class InvalidRole extends DomainException {

}
