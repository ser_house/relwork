<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.02.2019
 * Time: 17:48
 */

namespace Core\Client\Exception;


use DomainException;

class ImpossibleToDownLevel extends DomainException {

}
