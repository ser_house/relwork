<?php

namespace Core\Client\Exception;

use DomainException;

final class SellerNotFoundException extends DomainException {

}
