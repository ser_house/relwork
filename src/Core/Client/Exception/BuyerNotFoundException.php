<?php

namespace Core\Client\Exception;

use DomainException;

final class BuyerNotFoundException extends DomainException {

}
