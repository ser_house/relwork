<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2019
 * Time: 2:25
 */

namespace Core\Client\Events;

/**
 * Class ClientRegisteredHandler
 */
class ClientRegisteredHandler {

  /**
   * @param ClientRegisteredEvent $event
   */
  public function __invoke(ClientRegisteredEvent $event) {

  }
}
