<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:02
 */

namespace Core\Client\Events;


use Core\Events\Event;
use Core\System\System;
use Core\User\Role;
use Core\User\UserId;

class ClientRegisteredEvent extends Event {

  public function __construct(public readonly UserId $userId, public readonly Role $role) {
    parent::__construct(new System());
  }
}
