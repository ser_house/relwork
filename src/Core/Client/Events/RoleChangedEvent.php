<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:17
 */

namespace Core\Client\Events;

use Core\Events\Event;
use Core\Events\IEventInitiator;
use Core\User\Role;
use Core\User\UserId;

class RoleChangedEvent extends Event {

  public function __construct(
    IEventInitiator $eventInitiator,
    public readonly UserId $userId,
    public readonly Role $prevRole,
    public readonly Role $newRole
  ) {
    parent::__construct($eventInitiator);
  }
}
