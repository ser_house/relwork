<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:17
 */

namespace Core\Client\Events;


class RoleChangedHandler {

  /**
   * @inheritDoc
   */
  public function __invoke(RoleChangedEvent $event) {
    $userId = $event->userId;
    $newRoleId = $event->newRole;
  }
}
