<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.07.2019
 * Time: 11:16
 */


namespace Core\Job;


use Core\File\IFileRepository;


/**
 * Class JobData
 *
 * Существующий JobData как DTO примитивных значений.
 */
class JobData {

  public readonly string $id;
  public readonly string $title;
  public readonly ImageData $image;
  public readonly string $image_file_relative_path;
  public readonly string $category_id;
  public readonly string $description;
  public readonly string $state;
  /** @var string[] */
  public readonly array $files;
  public readonly string $cost;
  public readonly string $currency;
  public readonly string $hours;

  public function __construct(Job $domainJob, IFileRepository $fileRepository) {
    $this->id = (string)$domainJob->id;
    $this->title = (string)$domainJob->title;
    $imageId = $domainJob->imageFileId;
    $origImage = $fileRepository->getById($imageId);
//    $this->image = new ImageData("/files/$origImage->uri", $origImage->name);
    $this->image_file_relative_path = $origImage->getRelativeFilePath();
    $this->category_id = (string)$domainJob->categoryId;
    $this->description = (string)$domainJob->description;
    $this->state = (string)$domainJob->state->value;
    $this->cost = $domainJob->cost->getAmount();
    $this->hours = (string)$domainJob->hours;
  }
}
