<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:38
 */

namespace Core\Job\View;


class JobPageView {

	public string $title;
	public string $image_url;
	public string $created_at;
	public string $creator;
	public string $category;
	public string $description;
	public string $state;
	public string $cost;
	public string $hours;
}
