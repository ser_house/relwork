<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:38
 */

namespace Core\Job\View;


use Core\File\IFileRepository;
use Core\Generic\Infrastructure\Formatter\DateTime;
use Core\Generic\Infrastructure\Formatter\Number;
use Core\Job\Category\Category;
use Core\Job\Job;
use Core\User\User;

class JobPagePresenter {

  public function __construct(
    private IFileRepository $fileRepository,
    private DateTime $dateTimeFormatter,
    private Number $numberFormatter,
  ) {

  }


  public function present(Job $job,Category $category,User $creator): JobPageView {
    $view = new JobPageView();

    $view->title = (string)$job->title;

    $imageFile = $this->fileRepository->getById($job->imageFileId);
    $view->image_url = "/files/$imageFile->uri";
    $view->created_at = $this->dateTimeFormatter->dateTimeShort($job->createdAt);
    $view->creator = (string)$creator->name;
    $view->category = (string)$category->title;
    $view->description = (string)$job->description;
    $view->state = (string)$job->state->title();
    $view->cost = $this->numberFormatter->formatAmount($job->cost->getAmount());

    $view->hours = $this->dateTimeFormatter->hours($job->hours->value);

    return $view;
  }
}
