<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 1:55
 */

namespace Core\Job;


use Core\Description;
use Core\File\FileId;
use Core\Hours;
use Core\Job\Category\CategoryId;
use Core\Money\Money;
use Core\Title;
use Core\User\UserId;
use DateTimeImmutable;

class Job {

  public readonly JobId $id;
  public readonly DateTimeImmutable $createdAt;

	public function __construct(
    public readonly UserId $clientId,
    public readonly CategoryId $categoryId,
    public readonly Title $title,
    public readonly Description $description,
    public readonly FileId $imageFileId,
    public readonly State $state,
    public readonly Money $cost,
    public readonly Hours $hours,
  ) {
    $this->id = new JobId();
    $this->createdAt = new DateTimeImmutable();
	}
	public function changeStateTo(State $newState): void {
		// @todo
	}
}
