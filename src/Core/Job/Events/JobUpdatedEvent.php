<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 19:41
 */

namespace Core\Job\Events;


use Core\Events\Event;
use Core\Events\IEventInitiator;
use Core\File\File;
use Core\Job\Job;


/**
 * Class JobUpdatedEvent
 */
class JobUpdatedEvent extends Event {

  /**
   * JobUpdatedEvent constructor.
   *
   * @param IEventInitiator $eventInitiator
   * @param Job $oldJob
   * @param File $oldJobImage
   * @param File[] $oldJobFiles
   * @param Job $newJob
   * @param File $newJobImage
   * @param File[] $newJobFiles
   */
  public function __construct(
    IEventInitiator $eventInitiator,
    public readonly Job $oldJob,
    public readonly File $oldJobImage,
    public readonly array $oldJobFiles,
    public readonly Job $newJob,
    public readonly File $newJobImage,
    public readonly array $newJobFiles,
  ) {
    parent::__construct($eventInitiator);
  }
}
