<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 19:41
 */

namespace Core\Job\Events;


use Core\Events\Event;
use Core\Events\IEventInitiator;
use Core\Job\JobId;

/**
 * Class JobAddedEvent
 */
class JobAddedEvent extends Event {

  public function __construct(
    IEventInitiator $eventInitiator,
    public readonly JobId $jobId,
  ) {
    parent::__construct($eventInitiator);
  }
}
