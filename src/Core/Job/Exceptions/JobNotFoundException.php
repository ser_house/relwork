<?php

namespace Core\Job\Exceptions;

use DomainException;

final class JobNotFoundException extends DomainException {

}
