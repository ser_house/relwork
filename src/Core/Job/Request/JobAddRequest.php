<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.03.2019
 * Time: 3:25
 */

namespace Core\Job\Request;

use Core\Description;
use Core\File\File;
use Core\Hours;
use Core\Job\Category\CategoryId;
use Core\Job\State;
use Core\Money\Currency;
use Core\Money\Money;
use Core\Title;
use InvalidArgumentException;


/**
 * Class JobAddRequest
 */
class JobAddRequest {

  public readonly CategoryId $categoryId;
  public readonly Title $title;
  public readonly Description $description;
  public readonly File $image;
  public readonly Money $cost;
  public readonly Hours $hours;
  public readonly State $state;

  public function __construct(array $input) {
    $required = [
      'category',
      'title',
      'description',
      'image',
      'cost',
      'hours',
    ];
    $missing = [];
    foreach ($required as $item) {
      if (!isset($input[$item])) {
        $missing[] = $item;
      }
    }

    if (!empty($missing)) {
      $values_str = implode(',', $missing);
      throw new InvalidArgumentException("Missing required values: $values_str");
    }

    $currency = $input['currency'] ? new Currency($input['currency']) : new Currency('RUB');

    $this->categoryId = new CategoryId($input['category']);
    $this->title = new Title($input['title']);
    $this->description = new Description($input['description']);
    $this->image = $input['image'];
    $this->cost = new Money((float)$input['cost'], $currency);
    $this->hours = new Hours($input['hours']);

    // @todo: статус зависит от факторов (добавить может не продавец, а, скажем, администратор)
    $this->state = State::NEW;
  }
}
