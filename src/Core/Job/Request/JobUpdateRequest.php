<?php


namespace Core\Job\Request;

use Core\Description;
use Core\File\File;
use Core\Hours;
use Core\Job\Category\CategoryId;
use Core\Job\Job;
use Core\Job\JobData;
use Core\Job\State;
use Core\Money\Money;
use Core\Title;
use Exception;
use InvalidArgumentException;


/**
 * Class JobUpdateRequest
 */
class JobUpdateRequest {

	public readonly Job $origJob;
  public readonly CategoryId $categoryId;
  public readonly Title $title;
  public readonly Description $description;
  public readonly File $image;
  public readonly State $state;
  public readonly Money $cost;
  public readonly Hours $hours;


	/**
	 * JobUpdateRequest constructor.
	 *
	 * @param Job $origJob
	 * @param JobData $newJobData
	 *
	 * @throws Exception
	 */
	public function __construct(Job $origJob, JobData $newJobData) {
		$required = [
			'category_id',
			'title',
			'image',
			'description',
			'cost',
			'hours',
		];
		$missing = [];
		foreach($required as $item) {
			if (empty($newJobData->{$item})) {
				$missing[] = $item;
			}
		}

		if (!empty($missing)) {
			$values_str = implode(',', $missing);
			throw new InvalidArgumentException("Missing required values: $values_str");
		}

		$this->categoryId = new CategoryId($newJobData->category_id);
		$this->title = new Title($newJobData->title);
		$this->description = new Description($newJobData->description);

		$this->image = new File(
//			$origJob->imageFileId,
			$origJob->clientId,
			$newJobData->image->file_name,
			$newJobData->image->relative_directory,
			$newJobData->image->uri
		);
		$this->state = State::from($newJobData->state);
		$this->cost = new Money($newJobData->cost);
		$this->hours = new Hours($newJobData->hours);

		$this->origJob = $origJob;
	}
}
