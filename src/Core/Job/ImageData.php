<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.08.2019
 * Time: 10:38
 */


namespace Core\Job;


use ArrayAccess;
use Core\File\IMovableFile;

/**
 * Class ImageData
 */
class ImageData implements ArrayAccess {

	public function __construct(
    public IMovableFile $file,
    public string $uri,
    public string $file_name,
    public string $relative_directory = '',
    public bool $is_temp = false
  ) {

	}

	/**
	 * @param array $data
	 *
	 * @return ImageData
	 */
	public static function buildFromData(array $data): self {
		$uri = $data['uri'];
		$file_name = $data['file_name'];
		$relative_directory = !empty($data['relative_directory']) ? $data['relative_directory'] : '';
		$is_temp = !empty($data['is_temp']) && 'false' !== $data['is_temp'];

		return new self($uri, $file_name, $relative_directory, $is_temp);
	}

	/**
	 * Whether a offset exists
	 * @link https://php.net/manual/en/arrayaccess.offsetexists.php
	 *
	 * @param mixed $offset <p>
	 * An offset to check for.
	 * </p>
	 *
	 * @return boolean true on success or false on failure.
	 * </p>
	 * <p>
	 * The return value will be casted to boolean if non-boolean was returned.
	 * @since 5.0.0
	 */
	public function offsetExists(mixed $offset): bool {
		return !empty($this->{$offset});
	}

	/**
	 * Offset to retrieve
	 * @link https://php.net/manual/en/arrayaccess.offsetget.php
	 *
	 * @param mixed $offset <p>
	 * The offset to retrieve.
	 * </p>
	 *
	 * @return mixed Can return all value types.
	 * @since 5.0.0
	 */
	public function offsetGet(mixed $offset): mixed {
		return $this->offsetExists($offset) ? $this->{$offset} : null;
	}

	/**
	 * Offset to set
	 * @link https://php.net/manual/en/arrayaccess.offsetset.php
	 *
	 * @param mixed $offset <p>
	 * The offset to assign the value to.
	 * </p>
	 * @param mixed $value <p>
	 * The value to set.
	 * </p>
	 *
	 * @return void
	 * @since 5.0.0
	 */
	public function offsetSet(mixed $offset, mixed $value): void {
		$this->{$offset} = $value;
	}

	/**
	 * Offset to unset
	 * @link https://php.net/manual/en/arrayaccess.offsetunset.php
	 *
	 * @param mixed $offset <p>
	 * The offset to unset.
	 * </p>
	 *
	 * @return void
	 * @since 5.0.0
	 */
	public function offsetUnset(mixed $offset): void {
		$this->{$offset} = null;
	}
}
