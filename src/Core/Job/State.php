<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 2:00
 */

namespace Core\Job;

use Core\Title;

enum State : int {
  case NEW = 0;
  case MODERATE = 1;
  case REWORK = 3;
  case PUBLISHED = 4;
  case IN_WORK = 5;
  case UNPUBLISHED = 6;


	/**
	 * @return Title
	 */
	public function title(): Title {
    return match($this) {
      self::NEW => new Title('Новая'),
      self::MODERATE => new Title('Модерация'),
      self::REWORK => new Title('Доработка'),
      self::PUBLISHED => new Title('Опубликована'),
      self::IN_WORK => new Title('В работе'),
      self::UNPUBLISHED => new Title('Неопубликована'),
    };
	}
}
