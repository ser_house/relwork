<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 1:54
 */

namespace Core\Job;

use Core\Description;
use Core\File\FileId;
use Core\Hours;
use Core\Job\Category\CategoryId;
use Core\Job\Exceptions\JobNotFoundException;
use Core\Money\Money;
use Core\Title;
use Core\User\UserId;

interface IJobRepository {
	/**
	 * @param JobId $jobId
	 *
	 * @return Job|null
	 */
	public function findByJobId(JobId $jobId): ?Job;

  /**
   * @param JobId $jobId
   *
   * @return Job
   * @throws JobNotFoundException
   */
	public function getByJobId(JobId $jobId): Job;

	/**
	 * @param UserId $userId
	 *
	 * @return array
	 */
	public function findBySellerId(UserId $userId): array;

	/**
	 * @param Job $job
	 */
	public function add(Job $job): void;

  /**
   * @param JobId $jobId
   * @param CategoryId $categoryId
   * @param Title $title
   * @param Description $description
   * @param FileId $imageFileId
   * @param State $state
   * @param Money $cost
   * @param Hours $hours
   *
   * @return void
   */
  public function update(
    JobId $jobId,
    CategoryId $categoryId,
    Title $title,
    Description $description,
    FileId $imageFileId,
    State $state,
    Money $cost,
    Hours $hours
  ): void;
}
