<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 9:07
 */

namespace Core\Job\Category;


use Core\Description;
use Core\Title;

class Category {
  public readonly CategoryId $id;

	public function __construct(
    public readonly Title $title,
    public readonly ?Description $description = null,
    public readonly ?CategoryId $parentId = null,
  ) {
    $this->id = new CategoryId();
	}
}
