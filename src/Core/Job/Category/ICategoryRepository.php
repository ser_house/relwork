<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 9:11
 */

namespace Core\Job\Category;


interface ICategoryRepository {
	/**
	 * @param CategoryId $categoryId
	 *
	 * @return Category|null
	 */
	public function findByCategoryId(CategoryId $categoryId): ?Category;

  /**
   * @param CategoryId $categoryId
   *
   * @return Category
   * @throws CategoryNotFoundException
   */
	public function getByCategoryId(CategoryId $categoryId): Category;

	/**
	 * @param CategoryId|null $parentId
	 *
	 * @return array
	 */
	public function findAll(CategoryId $parentId = null): array;

	/**
	 * @param Category $category
	 */
	public function add(Category $category): void;
}
