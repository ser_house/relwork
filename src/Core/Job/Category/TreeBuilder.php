<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.03.2019
 * Time: 11:43
 */

namespace Core\Job\Category;


use stdClass;

class TreeBuilder {

	/**
	 * @param Category[] $categories
	 *
	 * @return array
	 */
	public function build(array $categories): array{
		$category_items = [];
		foreach($categories as $category) {
			$item = new stdClass();
			$item->id = (string)$category->id;
			$item->parent_id = $category->parentId ? (string)$category->parentId : null;
			$item->title = (string)$category->title;
			$category_items[] = $item;
		}
		return $this->buildCategoriesTree($category_items);
	}

	/**
	 * @param Category[] $categories
	 *
	 * @return array
	 */
	public function buildAsChoices(array $categories): array {
		$tree = $this->build($categories);

		$choices = [];
		foreach($tree as $item) {
			if (empty($item->children)) {
				$choices[$item->title] = $item->id;
			}
			else {
				$choices[$item->title] = [];
				foreach($item->children as $child) {
					$choices[$item->title][$child->title] = $child->id;
				}
			}
		}

		return $choices;
	}

	/**
	 * http://tech.vg.no/2011/10/31/building-tree-structures-in-php-using-references/
	 *
	 * Предусловие: у объектов должны быть свойства id и parent_id,
	 * объекты должны быть отсортированы по возрастанию parent_id.
	 *
	 * @param array $items
	 *   массив объектов.
	 *
	 * @return array
	 */
	private function buildCategoriesTree(array $items): array {
		$tree = [];

		/* Most datasets in the wild are enumerative arrays and we need associative array
			where the same ID used for addressing parents is used. We make associative
			array on the fly */
		$references = [];
		foreach ($items as &$item) {
			// Add empty placeholder for children
			$item->children = [];
			// Add the node to our associative array using it's ID as key
			$references[$item->id] = &$item;

			// It it's a root node, we add it directly to the tree
			if (!$item->parent_id) {
				$tree[$item->id] = &$item;
			}
			else {
				// It was not a root node, add this node as a reference in the parent.
				$references[$item->parent_id]->children[$item->id] = &$item;
			}
		}

		return $tree;
	}
}
