<?php

namespace Core\Job\Category;

use DomainException;

final class CategoryNotFoundException extends DomainException {

}
