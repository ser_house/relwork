<?php

namespace Framework;

use Core\ITransaction;
use Doctrine\DBAL\Connection;

final class Transaction implements ITransaction {

  public function __construct(private Connection $connection) {
  }

  public function begin(): void {
    $this->connection->beginTransaction();
  }

  public function commit(): void {
    $this->connection->commit();
  }

  public function rollback(): void {
    $this->connection->rollBack();
  }
}
