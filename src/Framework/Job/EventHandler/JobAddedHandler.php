<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 22:03
 */

namespace Framework\Job\EventHandler;


use Core\Job\Events\JobAddedEvent;

class JobAddedHandler {
	public function __invoke(JobAddedEvent $jobAddedEvent) {

	}
}
