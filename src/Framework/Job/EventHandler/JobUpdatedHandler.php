<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.07.2019
 * Time: 19:00
 */


namespace Framework\Job\EventHandler;


use Core\File\IFileService;
use Core\Job\Events\JobUpdatedEvent;

/**
 * Class JobUpdatedHandler
 */
class JobUpdatedHandler {


  public function __construct(private IFileService $fileService) {
  }


  /**
   * @param JobUpdatedEvent $jobUpdatedEvent
   */
  public function __invoke(JobUpdatedEvent $jobUpdatedEvent) {
    $oldImage = $jobUpdatedEvent->oldJobImage;
    $newImage = $jobUpdatedEvent->newJobImage;

    if ($newImage->getRelativeFilePath() !== $oldImage->getRelativeFilePath()) {
      $this->fileService->removeByRelativePath($oldImage->getRelativeFilePath());
    }
  }
}
