<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 1:56
 */

namespace Framework\Job;


use Core\Description;
use Core\File\FileId;
use Core\Generic\Infrastructure\Hydrator;
use Core\Hours;
use Core\Job\Category\CategoryId;
use Core\Job\Exceptions\JobNotFoundException;
use Core\Job\IJobRepository;
use Core\Job\Job;
use Core\Job\JobId;
use Core\Job\State;
use Core\Money\Money;
use Core\Title;
use Core\User\UserId;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

class JobRepository implements IJobRepository {

  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {
  }

  /**
   * @inheritDoc
   */
  public function findByJobId(JobId $jobId): ?Job {
    $queryBuilder = $this->connection->createQueryBuilder();
    $id = (string)$jobId;
    $item = $queryBuilder->select('*')
      ->from('job')
      ->where('id = ?')
      ->setParameter(0, $id, ParameterType::STRING)
      ->fetchAssociative();

    if (empty($item)) {
      return null;
    }

    /** @var Job $job */
    $job = $this->hydrator->hydrate($item, Job::class);
    return $job;
  }

  /**
   * @inheritDoc
   */
  public function getByJobId(JobId $jobId): Job {
    $job = $this->findByJobId($jobId);
    if (null === $job) {
      throw new JobNotFoundException("Job by id '$jobId' not found in repository.");
    }
    return $job;
  }

  /**
   * @inheritDoc
   */
  public function findBySellerId(UserId $userId): array {
    // @todo: пагинацию надо еще учесть, обязательно: со временем задач может набраться много, за годы-то.
    // Скажем, не миллионы, конечно, даже и не тысячи и не сотни.
    $queryBuilder = $this->connection->createQueryBuilder();
    $id = (string)$userId;
    $items = $queryBuilder->select('*')
      ->from('job')
      ->where('client_id = ?')
      ->setParameter(0, $id, ParameterType::STRING)
      ->fetchAllAssociative();

    if (empty($items)) {
      return [];
    }

    $jobs = [];
    foreach ($items as $item) {
      $jobs[] = $this->hydrator->hydrate($item, Job::class);
    }

    return $jobs;
  }

  /**
   * @inheritDoc
   */
  public function add(Job $job): void {
    // @todo: при обновлении задачи менять можно явно не всё.
    $data = [
      'id' => (string)$job->id,
      'created_at' => $job->createdAt->format('Y-m-d H:i'),
      'client_id' => (string)$job->clientId,
      'category_id' => (string)$job->categoryId,
      'title' => (string)$job->title,
      'description' => (string)$job->description,
      'image_file_id' => (string)$job->imageFileId,
      'state' => $job->state->value,
      'cost' => $job->cost->getAmount(),
      'hours' => (string)$job->hours,
    ];
    $this->connection->insert('job', $data);
    // @todo: и файлы в job_file
  }

  /**
   * @inheritDoc
   */
  public function update(
    JobId $jobId,
    CategoryId $categoryId,
    Title $title,
    Description $description,
    FileId $imageFileId,
    State $state,
    Money $cost,
    Hours $hours
  ): void {
    $data = [
      'category_id' => (string)$categoryId,
      'title' => (string)$title,
      'description' => (string)$description,
      'image_file_id' => (string)$imageFileId,
      'state' => $state->value,
      'cost' => $cost->getAmount(),
      'hours' => (string)$hours,
    ];
    $this->connection->update('job', $data, ['id' => (string)$jobId]);
    // @todo: и файлы в job_file
  }
}
