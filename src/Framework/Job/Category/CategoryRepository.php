<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 9:13
 */

namespace Framework\Job\Category;


use Core\Generic\Infrastructure\Hydrator;
use Core\Job\Category\Category;
use Core\Job\Category\CategoryId;
use Core\Job\Category\CategoryNotFoundException;
use Core\Job\Category\ICategoryRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

class CategoryRepository implements ICategoryRepository {

  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {
  }

  /**
   * @inheritDoc
   */
  public function findByCategoryId(CategoryId $categoryId): ?Category {
    $queryBuilder = $this->connection->createQueryBuilder();
    $id = (string)$categoryId;
    $item = $queryBuilder->select('*')
      ->from('category')
      ->where('id = ?')
      ->setParameter(0, $id, ParameterType::STRING)
      ->fetchAssociative();

    if (empty($item)) {
      return null;
    }

    /** @var Category $category */
    $category = $this->hydrator->hydrate($item, Category::class);
    return $category;
  }

  /**
   * @inheritDoc
   */
  public function getByCategoryId(CategoryId $categoryId): Category {
    $category = $this->findByCategoryId($categoryId);
    if (null === $category) {
      throw new CategoryNotFoundException("Category by id '$categoryId' not found in repository.");
    }
    return $category;
  }

  /**
   * @inheritDoc
   */
  public function findAll(CategoryId $parentId = null): array {
    $parent_condition = 'IS NULL';
    if (null !== $parentId) {
      $parent_condition = ' = :parent_id';
    }
    $sql = "WITH RECURSIVE nodes(id, title, description, parent_id) AS (
			SELECT s1.id, s1.title, s1.description, s1.parent_id
			FROM category s1 WHERE parent_id $parent_condition
			UNION
			SELECT s2.id, s2.title, s2.description, s2.parent_id
			FROM category s2, nodes s1 WHERE s2.parent_id = s1.id
		)
		SELECT * FROM nodes;
		";

    $stmt = $this->connection->prepare($sql);

    if (null !== $parentId) {
      $id = (string)$parentId;
      $stmt->bindParam(':parent_id', $id);
    }

    $items = $stmt->executeQuery()->fetchAllAssociative();

    $categories = [];
    foreach ($items as $item) {
      $categories[] = $this->hydrator->hydrate($item, Category::class);
    }

    return $categories;
  }

  /**
   * @inheritDoc
   */
  public function add(Category $category): void {
    $data = [
      'id' => (string)$category->id,
      'title' => (string)$category->title,
      'description' => $category->description ? (string)$category->description : null,
      'parent_id' => $category->parentId ? (string)$category->parentId : null,
    ];
    $this->connection->insert('category', $data);
  }
}
