<?php

namespace Framework\File;

use Core\File\IMovableFile;
use Symfony\Component\HttpFoundation\File\File as SymfonyFile;

final class MovableFile implements IMovableFile {
  public function __construct(private SymfonyFile $file) {
  }

  /**
   * @inheritDoc
   */
  public function getFilename(): string {
    return $this->file->getFilename();
  }

  /**
   * @inheritDoc
   */
  public function getPath(): string {
    return $this->file->getRealPath();
  }

  /**
   * @inheritDoc
   */
  public function moveTo(string $dir): string {
    return $this->file->move($dir, $this->getFilename())->getRealPath();
  }
}
