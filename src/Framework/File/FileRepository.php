<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 0:58
 */

namespace Framework\File;


use Core\File\File;
use Core\File\FileId;
use Core\File\FileNotFoundException;
use Core\File\IFileRepository;
use Core\Generic\Infrastructure\Hydrator;
use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

/**
 * Class FileRepository
 */
class FileRepository implements IFileRepository {


  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function add(File $file): void {
    $data = [
      'id' => (string)$file->id,
      'created_at' => $file->createdAt->format('Y-m-d H:i'),
      'user_id' => (string)$file->userId,
      'name' => $file->name,
      'relative_directory' => $file->relative_directory,
      'uri' => $file->uri,
    ];
    $this->connection->insert('file', $data);
  }

  /**
   * @inheritDoc
   */
  public function update(File $file): void {
    $updatedAt = new DateTimeImmutable();

    $data = [
      'updated' => $updatedAt->format('Y-m-d H:i'),
      'name' => $file->name,
      'relative_directory' => $file->relative_directory,
      'uri' => $file->uri,
    ];
    $this->connection->update('file', $data, ['id' => (string)$file->id]);
  }

  /**
   * @inheritDoc
   */
  public function findById(FileId $id): ?File {
    $queryBuilder = $this->connection->createQueryBuilder();
    $id_str = (string)$id;
    $item = $queryBuilder->select('*')
      ->from('file')
      ->where('id = ?')
      ->setParameter(0, $id_str, ParameterType::STRING)
      ->fetchAssociative();

    if (empty($item)) {
      return null;
    }

    /** @var File $file */
    $file = $this->hydrator->hydrate($item, File::class);
    return $file;
  }

  /**
   * @inheritDoc
   */
  public function getById(FileId $id): File {
    $file = $this->findById($id);
    if (null === $file) {
      throw new FileNotFoundException("File by id '$id' not found in file repository.");
    }
    return $file;
  }

  /**
   * @inheritDoc
   */
  public function removeById(FileId $id): void {
    // TODO: Implement removeById() method.
  }

  /**
   * @inheritDoc
   */
  public function getFileUri(FileId $id): string {
    $sql = "SELECT uri FROM file WHERE id = ?";
    return $this->connection->fetchOne($sql, [(string)$id]);
  }
}
