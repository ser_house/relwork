<?php


namespace Framework\File\Image;


use Core\File\FilePathManager;
use Core\Job\ImageData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ImageType extends AbstractType {

  public function __construct(private FilePathManager $filePathManager) {

  }


  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults([
      'compound' => true,
      'is_temp' => true,
      'uri' => '',
      'file_name' => '',
    ]);

    $resolver->setAllowedTypes('is_temp', 'bool');

    $resolver->setNormalizer('is_temp', static function ($is_temp) {
      return !empty($is_temp) && 'false' !== $is_temp;
    });
  }

  /**
   * @inheritDoc
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
      ->add('image', FileType::class, [
        'label' => false,
        'constraints' => [
          new File([
            'maxSize' => '1024k',
            'mimeTypes' => [
              'image/jpeg',
              'image/png',
            ],
            'mimeTypesMessage' => 'Please upload a valid image',
          ]),
          new NotBlank(),
        ],
      ])
      ->add('file_name', HiddenType::class)
      ->add('uri', HiddenType::class)
      ->add('is_temp', HiddenType::class);
  }

  /**
   * @inheritDoc
   */
  public function buildView(FormView $view, FormInterface $form, array $options) {
    parent::buildView($view, $form, $options);

    $view->vars['text_errors'] = [];

    $errors = $form->getErrors(true);

    foreach ($errors as $error) {
      $view->vars['text_errors'][] = $error->getMessage();
    }


    if (!empty($view->vars['value'])) {
      /** @var ImageData $image */
      $image = $view->vars['value'];

      if ($image->is_temp) {
        $image->uri = $this->filePathManager->getTempPublicFileUri($image->file_name);
      }

      $view->vars['value'] = (array)$image;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(): string {
    return 'image';
  }
}
