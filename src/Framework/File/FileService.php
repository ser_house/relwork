<?php

namespace Framework\File;

use Core\File\FilePathManager;
use Core\File\IFileService;
use Core\File\IMovableFile;
use Core\File\PublicFile;
use Core\User\UserId;
use Symfony\Component\Filesystem\Filesystem;

final class FileService implements IFileService {
  public function __construct(
    private FilePathManager $filePathManager
  ) {

  }

  /**
   * @inheritDoc
   */
  public function moveToPublic(UserId $userId, IMovableFile $file): PublicFile {
    $file_name = $file->getFilename();

    $sub_dir = $this->filePathManager->getUserFilesCurrentSubDir($userId);
    $public_files_directory = $this->filePathManager->public_files_dir;

    $target_sub_dir = "$public_files_directory/$sub_dir";
    $this->filePathManager->testDirectory($target_sub_dir);

    $path = $file->moveTo($target_sub_dir);

    $relative_directory = $this->filePathManager->getRelativeFilePathFromAbsolutePath($path);

    $uri_dir = $this->filePathManager->getPublicFileUri($path);
    $uri = "$uri_dir/$file_name";

    return new PublicFile($file_name, $relative_directory, $uri);
  }

  /**
   * @inheritDoc
   */
  public function remove(string $path): void {
    $fileSystem = new Filesystem();
    $fileSystem->remove($path);
  }

  /**
   * @inheritDoc
   */
  public function removeByRelativePath(string $relative_path): void {
    $absolute_image_path = $this->filePathManager->getAbsoluteFilePathFromRelative($relative_path);
    $this->remove($absolute_image_path);

    // И все пустые директории в пути, оставшиеся после удаления файла.
    $dir = dirname($absolute_image_path);
    while(empty(array_diff(scandir($dir), array('.', '..')))) {
      $this->remove($dir);
      $dir = dirname($dir);
    }
  }
}
