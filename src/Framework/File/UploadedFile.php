<?php

namespace Framework\File;

use Core\File\IMovableFile;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

final class UploadedFile implements IMovableFile {

  public function __construct(private SymfonyUploadedFile $file) {
  }

  /**
   * @inheritDoc
   */
  public function getFilename(): string {
    return $this->file->getClientOriginalName();
  }

  /**
   * @inheritDoc
   */
  public function getPath(): string {
    return $this->file->getRealPath();
  }

  /**
   * @inheritDoc
   */
  public function moveTo(string $dir): string {
    return $this->file->move($dir, $this->getFilename())->getRealPath();
  }
}
