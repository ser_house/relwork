<?php
namespace Framework\Overriden\FormType;

use DateTimeZone;
use Doctrine\DBAL\Connection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeZoneToStringTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TimezoneType extends AbstractType {

	public function __construct(private Connection $connection) {

	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		if ('datetimezone' === $options['input']) {
			$builder->addModelTransformer(new DateTimeZoneToStringTransformer($options['multiple']));
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'choice_loader' => function (Options $options) {
				return new CallbackChoiceLoader(function () {
					return self::getTimezones();
				});
			},
			'choice_translation_domain' => false,
			'input' => 'string',
			'regions' => DateTimeZone::ALL,
		]);

		$resolver->setAllowedValues('input', ['string', 'datetimezone']);

		$resolver->setAllowedTypes('regions', 'int');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParent(): ?string {
		return ChoiceType::class;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix(): string {
		return 'timezone';
	}

	/**
	 * Returns a normalized array of timezone choices.
	 */
	private function getTimezones(): array {
		$timezones = [];

    $timezone_identifiers = DateTimeZone::listIdentifiers();
		foreach ($timezone_identifiers as $tz_name) {
			$timezones[$tz_name] = $tz_name;
		}

		return $timezones;
	}
}
