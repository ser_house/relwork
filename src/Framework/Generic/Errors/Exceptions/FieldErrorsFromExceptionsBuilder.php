<?php

namespace Framework\Generic\Errors\Exceptions;


use Core\Generic\Errors\Exceptions\FieldsExceptionsCollection;
use Core\Generic\Errors\Exceptions\IsGreaterThanException;
use Core\Generic\Errors\Exceptions\IsLessThanException;
use Core\Generic\Errors\Exceptions\LengthException;
use Core\Generic\Errors\Exceptions\MissingValueException;
use Core\Generic\Errors\FieldErrors;
use Core\Generic\Errors\IFieldErrorLabels;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class FieldErrorsFromExceptionsBuilder
 *
 * Строит сообщения об ошибках на основе человечьих названий полей.
 */
class FieldErrorsFromExceptionsBuilder {

  public function __construct(private TranslatorInterface $translator ) {

  }

  /**
   * @param IFieldErrorLabels $fieldErrorLabels
   * @param FieldsExceptionsCollection $fieldExceptions
   *
   * @return FieldErrors
   */
  public function build(IFieldErrorLabels $fieldErrorLabels, FieldsExceptionsCollection $fieldExceptions): FieldErrors {
    $exceptions = $fieldExceptions->getExceptions();

    $errors = new FieldErrors();
    foreach ($exceptions as $field => $field_exceptions) {
      $label = $fieldErrorLabels->getFromOneField($field, $fieldExceptions->translate_domain_name);

      foreach ($field_exceptions as $e) {
        switch (true) {

          case $e instanceof MissingValueException:
            $params = ['%field%' => $label];
            $error = $this->translator->trans('missing_value', $params, 'field_errors', 'ru');
            $errors->addError($field, $error);
            break;

          case $e instanceof IsLessThanException:
            $params = [
              '%field%' => $label,
              '%value%' => $e->value,
              '%restrict%' => $e->restrict,
            ];
            $error = $this->translator->trans('is_less_than', $params, 'field_errors', 'ru');
            $errors->addError($field, $error);
            break;

          case $e instanceof IsGreaterThanException:
            $params = [
              '%field%' => $label,
              '%value%' => $e->value,
              '%restrict%' => $e->restrict,
            ];
            $error = $this->translator->trans('is_greater_than', $params, 'field_errors', 'ru');
            $errors->addError($field, $error);
            break;

          case $e instanceof LengthException:
            $params = [
              '%field%' => $label,
              '%value%' => $e->value,
              '%min%' => $e->min,
              '%max%' => $e->max,
              '%len%' => $e->len,
            ];
            $error = $this->translator->trans('length', $params, 'field_errors', 'ru');
            $errors->addError($field, $error);
            break;

          default:
            $errors->addError($field, $e->getMessage());
            break;
        }
      }
    }

    return $errors;
  }
}
