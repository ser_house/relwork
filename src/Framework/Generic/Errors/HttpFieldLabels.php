<?php

namespace Framework\Generic\Errors;


use Core\Generic\Errors\IFieldErrorLabels;
use Symfony\Contracts\Translation\TranslatorInterface;

class HttpFieldLabels implements IFieldErrorLabels {
  public function __construct(private TranslatorInterface $translator ) {

  }

  /**
   * @inheritDoc
   */
  public function getFromOneField(string $field, string $translate_domain_name = null): string {
    return $this->getHumanFieldLabel($field, $translate_domain_name);
  }

  /**
   * @inheritDoc
   */
  public function getFromManyFields(array $fields, string $translate_domain_name = null): array {
    return $this->getHumanLabels($fields, $translate_domain_name);
  }

  /**
   * @param array $fields
   * @param string|null $translate_domain_name
   *
   * @return array
   */
  private function getHumanLabels(array $fields, string $translate_domain_name = null): array {
    $labels = [];
    foreach ($fields as $field) {
      $labels[] = $this->getHumanFieldLabel($field, $translate_domain_name);
    }

    return $labels;
  }

  /**
   * @param string $field
   * @param string|null $translate_domain_name
   *
   * @return string
   */
  private function getHumanFieldLabel(string $field, string $translate_domain_name = null): string {
    if (null === $translate_domain_name) {
      $label = $this->translator->trans($field, [], 'labels', 'ru');
    }
    else {
      $label = $this->translator->trans($field, [], $translate_domain_name, 'ru');
    }

    return $label ?: $field;
  }
}
