<?php

namespace Framework\Events;

use Core\Events\Event;
use Core\Events\IEventDispatcher;
use Symfony\Component\Messenger\MessageBusInterface;

class EventDispatcher implements IEventDispatcher {
  public function __construct(private MessageBusInterface $bus) {
  }

  /**
   * @inheritDoc
   */
  public function dispatch(Event $event): void {
    $this->bus->dispatch($event);
  }

  /**
   * @inheritDoc
   */
  public function dispatchEvents(iterable $events): void {
    foreach ($events as $event) {
      $this->dispatch($event);
    }
  }
}
