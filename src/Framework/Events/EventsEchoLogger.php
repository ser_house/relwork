<?php

namespace Framework\Events;

use Core\Client\Events\ClientRegisteredEvent;
use Core\Client\Events\RoleChangedEvent;
use Core\Events\Event;
use Core\Job\Events\JobAddedEvent;
use Core\User\Events\UserRegisteredEvent;
use ReflectionClass;

/**
 * Тестовый логгер событий, позволяет вывести в консоль все доменные события
 * в порядке их возникновения.
 */
class EventsEchoLogger {

  /**
   * Handle the event.
   *
   * @param Event $event
   *
   * @return void
   */
  public function __invoke(Event $event) {
    $reflect = new ReflectionClass($event);
    $event_name = $reflect->getShortName();

    $data = [];
//    $data = $this->getInfo($event);

    echo "\r{$event->dateTime->format('d.m.Y H:i:s')} {$event->eventInitiator->getRole()->name} {$event_name}\n";
    if ($data) {
      echo print_r($data, true), PHP_EOL;
    }
  }

  private function getInfo(Event $event): array {
    switch(true) {
      case $event instanceof UserRegisteredEvent:
        $user = $event->user;
        return [
          'id' => (string)$user->id,
          'name' => (string)$user->name,
          'email' => (string)$user->email,
          'tz' => $user->timeZone->getName(),
          'role' => (string)$user->role->title(),
        ];

      case $event instanceof ClientRegisteredEvent:
        return [
          'role' => (string)$event->role->title(),
        ];

      case $event instanceof RoleChangedEvent:
        return [
          'id' => (string)$event->userId,
          'prev_role' => (string)$event->prevRole->title(),
          'new_role' => (string)$event->newRole->title(),
        ];

      case $event instanceof JobAddedEvent:
        return [
          'job_id' => (string)$event->jobId,
        ];

      default:
        return [];
    }
  }
}
