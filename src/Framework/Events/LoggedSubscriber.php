<?php


namespace Framework\Events;

use Core\User\Context\ContextFactory;
use Core\User\IUserRepository;
use Core\User\Role;
use Core\User\User;
use Framework\Web\Client\Templates;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;


/**
 * Class LoggedSubscriber
 */
class LoggedSubscriber implements EventSubscriberInterface {

  public function __construct(
    private Environment $twig,
    private Templates $templates,
    private ContextFactory $contextFactory,
    private IUserRepository $userRepository,
    private RequestStack $requestStack,
  ) {

  }


  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::CONTROLLER => 'onKernelController',
    ];
  }

  /**
   * @param ControllerEvent $event
   *
   * @return void
   */
  public function onKernelController(ControllerEvent $event): void {
    $this->twig->addGlobal('mail_url', $_ENV['MAIL_URL']);

    $controller = $event->getController();

    if (is_array($controller) || !$event->isMainRequest()) {
      return;
    }

    $request = $event->getRequest();
    $route = $request->attributes->get('_route');
    if (in_array($route, [
      'admin',
      'admin_user_delete',
      'admin_login_as',
      'login',
      'registration',
      'registration_create_profile',
    ])) {
      return;
    }

    // Здесь уже должен быть только зарегистрированный
    // (исключая index, который из проверки надо исключить,
    // а из добавления данных пользователя на страницу - нет).
    $user = $this->getUser();
    if (null === $user && 'index' !== $route) {
      $event->setController(function () {
        return new RedirectResponse('/login');
      });
      return;
    }

    if ($user) {
      $this->addLoggedUserData($request, $user);

      // @todo: что если логику вынести в роль? и проверять там, только не роуты фреймворкные, а операции
      // Роуты только для продавца
      if (Role::SELLER !== $user->role) {
        $this->checkNotSellerAccess($route, $this->requestStack->getSession(), $event);
        return;
      }

      // Роуты только для покупателя
      if (Role::BUYER !== $user->role) {
        $this->checkNotBuyerAccess($route, $this->requestStack->getSession(), $event);
      }
    }
  }

  /**
   * @return User|null
   */
  private function getUser(): ?User {
    $session = $this->requestStack->getSession();
    if (null === $session) {
      return null;
    }

    $userId = $session->get('userId');
    if (empty($userId)) {
      return null;
    }

    $user = $this->userRepository->findByUserId($userId);
    if (null === $user) {
      $session->remove('userId');
      return null;
    }

    return $user;
  }

  /**
   * @param Request $request
   * @param User $user
   *
   * @return void
   */
  private function addLoggedUserData(Request $request, User $user): void {
    $this->twig->addGlobal('is_logged', true);

    $userContextFactory = $this->contextFactory->getUserContextFactory($user);
    $presenter = $userContextFactory->createPresenter($user);
    $profileMenuView = $presenter->presentMenu();

    $this->twig->addGlobal('profileMenu', $profileMenuView);
    $this->twig->addGlobal('profile_menu_template', $this->templates->profileMenuTemplateByRole($user->role));

    $request->attributes->set('user', $user);
  }

  /**
   * @param string $route
   * @param SessionInterface $session
   * @param ControllerEvent $event
   *
   * @return void
   */
  private function checkNotSellerAccess(string $route, SessionInterface $session, ControllerEvent $event): void {
    switch ($route) {
      case 'add_job':
        $event->setController(function () use ($session) {
          $session->getFlashBag()->add('error', 'Вы не можете создавать задачи.');

          return new RedirectResponse('/');
        });
        break;

      case 'update_job':
        $event->setController(function () use ($session) {
          $session->getFlashBag()->add('error', 'Вы не можете редактировать эту задачу.');

          return new RedirectResponse('/');
        });
        break;

      default:
        break;
    }
  }

  private function checkNotBuyerAccess(string $route, SessionInterface $session, ControllerEvent $event): void {

  }
}
