<?php

namespace Framework\Events;

use Core\Generic\Errors\Exceptions\InvalidInputDataException;
use Core\Generic\Infrastructure\JsonService;
use Core\System\UseCases\Registration\Exceptions\ExpiredException;
use Core\System\UseCases\Registration\Exceptions\NotFoundException;
use Framework\Generic\Errors\Exceptions\FieldErrorsFromExceptionsBuilder;
use Framework\Generic\Errors\HttpFieldLabels;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionsSubscriber implements EventSubscriberInterface {

  public function __construct(
    private HttpFieldLabels $fieldErrorLabels,
    private FieldErrorsFromExceptionsBuilder $errorsBuilder,
    private JsonService $jsonService,
  ) {
  }

  public function onKernelException(ExceptionEvent $event) {
    $e = $event->getThrowable();
    $isJson = $event->getResponse() instanceof JsonResponse;

    switch (true) {
      case $e instanceof NotFoundException:
        $response = new Response();
        $response->setContent($e->getMessage());
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
        $event->setResponse($response);
        break;

      case $e instanceof InvalidInputDataException:
        if ($isJson) {
          $errors = $this->errorsBuilder->build($this->fieldErrorLabels, $e->exceptionsCollection);
          $message = $this->jsonService->encode($errors);
          $response = new JsonResponse();
          $response->setContent($message);
          $response->setStatusCode(Response::HTTP_BAD_REQUEST);
          $event->setResponse($response);
        }
        break;

      default:
        break;
    }
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::EXCEPTION => ['onKernelException', 1],
    ];
  }

}
