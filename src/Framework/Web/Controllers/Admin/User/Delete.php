<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.01.2019
 * Time: 12:26
 */

namespace Framework\Web\Controllers\Admin\User;

use Core\File\IFileService;
use Core\ITransaction;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Throwable;


class Delete extends AbstractController {
  public function __construct(
    private Connection $connection,
    private ITransaction $transaction,
    private IFileService $fileService,
    private RequestStack $requestStack,
  ) {
  }

  public function __invoke(string $id) {

    $has_jobs = $this->connection->fetchOne("SELECT COUNT(*) FROM job WHERE client_id = ?", [$id]);
    $has_files = $this->connection->fetchOne("SELECT COUNT(*) FROM file WHERE user_id = ?", [$id]);

    $this->transaction->begin();
    try {
      $this->connection->delete('client', ['user_id' => $id]);
      $avatar = $this->connection->fetchAssociative("SELECT avatar_name, avatar_rel_dir FROM user_account WHERE id = ?", [$id]);
      if ($avatar) {
        $relative_path = "{$avatar['avatar_rel_dir']}/{$avatar['avatar_name']}";
        $this->fileService->removeByRelativePath($relative_path);
      }
      $this->connection->delete('user_account', ['id' => $id]);
      if ($has_jobs) {
        $this->connection->delete('job', ['client_id' => $id]);
      }
      if ($has_files) {
        $files = $this->connection->fetchAllAssociative("SELECT * FROM file WHERE user_id = ?", [$id]);
        foreach ($files as $file) {
          $relative_path = "{$file['relative_directory']}/{$file['name']}";
          $this->fileService->removeByRelativePath($relative_path);
        }
        $this->connection->delete('file', ['user_id' => $id]);
      }
      $this->transaction->commit();
      $this->requestStack->getSession()->getFlashBag()->add('success', 'User was deleted.');
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      $this->requestStack->getSession()->getFlashBag()->add('error', $e->getMessage());
    }
    return $this->redirectToRoute('admin');
  }
}
