<?php

namespace Framework\Web\Controllers\Admin\User;

use Core\User\IUserRepository;
use Core\User\UserId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class LoginAs extends AbstractController {
  public function __construct(
    private RequestStack $requestStack,
    private IUserRepository $userRepository,
  ) {
  }

  public function __invoke(Request $request, string $id): Response {
    $session = $this->requestStack->getSession();

    $user = $this->userRepository->findByUserId(new UserId($id));
    if ($user) {
      $session->set('userId', $user->id);
      $this->addFlash('info', "Вы вошли в систему как '$user->name'.");
    }
    else {
      $this->addFlash('error', 'Пользователь не найден.');
    }

    return $this->redirectToRoute('index');
  }
}
