<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.01.2019
 * Time: 12:26
 */

namespace Framework\Web\Controllers\Admin;

use Core\User\Role;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class Index extends AbstractController {
  public function __construct(
    private Connection $connection,
  ) {
  }
  public function __invoke() {
    $sql = "SELECT * FROM user_account ORDER BY created_at";
    $data = $this->connection->fetchAllAssociative($sql);
    $items = [];
    foreach($data as $datum) {
      $role = Role::from($datum['role']);
      $items[] = [
        'id' => $datum['id'],
        'name' => $datum['name'],
        'created_at' => $datum['created_at'],
        'role' => $role->title(),
        'avatar_uri' => "/files/{$datum['avatar_uri']}",
        'email' => $datum['email'],
      ];
    }
    return $this->render('admin/index.twig', ['items' => $items]);
  }

}
