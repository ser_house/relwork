<?php


namespace Framework\Web\Controllers\Job;

use Core\File\FilePathManager;
use Core\File\PublicFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadJobImage extends AbstractController {

	/**
	 * @inheritDoc
	 */
	public function __invoke(Request $request, FilePathManager $filePathManager) {
		/** @var SymfonyFile $image */
		$image = $request->files->get('image');

		if (!$image->isValid()) {
			$data = [
				'errors' => $image->getErrorMessage(),
			];
		}
		else {
			$mime = $image->getMimeType();
			$valid_mime_types = [
				'image/jpeg',
				'image/png',
			];
			// Это ajax отдельно для файла. До сохранения сущности этот файл является пустышкой.
			// Но при сохранении сущности файл должен быть обработан и сохранен как файл,
			// принадлежащий сущности. И сохранение должно знать, что этот файл уже есть на сервере
			// и его надо только передвинуть (и, возможно, обработать) куда надо.
			// Поэтому надо передать информацию об этом файле в процесс сохранения сущности,
			// а это значит, что путь такой:
			// клиент[файл] -> сервер[временный файл] -> клиент[временный файл] -> сервер[временный файл]
      $fileName = $image->getClientOriginalName();

      $temp_public_files_directory = $filePathManager->temp_public_files_dir;
      $filePathManager->testDirectory($temp_public_files_directory);
      $targetFile = $image->move($temp_public_files_directory, $fileName);

      $uri_dir = $filePathManager->temp_public_files_uri;

      $file_path = $targetFile->getRealPath();
      $name = $targetFile->getFilename();
      $uri = "$uri_dir/$name";
      $relative_directory = $filePathManager->getRelativeFilePathFromAbsolutePath($file_path);

      $uploadedFile = new PublicFile($name, $relative_directory, $uri);
			// Клиенту нужен публичный относительный (относительно public) адрес картинки, чтобы показать её пользователю.
			$data = [
				'uri' => '/' . $uploadedFile->uri,
				'name' => $uploadedFile->name,
			];
		}

		return new JsonResponse($data);
	}

}
