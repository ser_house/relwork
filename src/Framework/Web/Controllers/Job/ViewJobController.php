<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:32
 */

namespace Framework\Web\Controllers\Job;


use Core\Client\Seller\ISellerRepository;
use Core\File\IFileRepository;
use Core\Job\Category\ICategoryRepository;
use Core\Job\IJobRepository;
use Core\Job\JobId;
use Core\User\IUserRepository;
use Exception;
use Framework\Web\Job\Presenter\JobPagePresenter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

// @todo: два контроллера на просмотр - для продавца и покупателя (или больше, для модератора там и проч.)
class ViewJobController extends AbstractController {

  public function __construct(
    private IJobRepository $jobRepository,
    private IUserRepository $userRepository,
    private ISellerRepository $sellerRepository,
    private ICategoryRepository $categoryRepository,
    private IFileRepository $fileRepository,
  ) {
  }


  /**
   * @param $job_id
   *
   * @return Response
   * @throws Exception
   */
  public function __invoke($job_id) {
    $jobId = new JobId($job_id);
    $job = $this->jobRepository->getByJobId($jobId);

    $category = $this->categoryRepository->getByCategoryId($job->categoryId);
    $user = $this->userRepository->getByUserId($job->clientId);
    $seller = $this->sellerRepository->getByUserId($user->id);

    $presenter = new JobPagePresenter($job, $category, $user, $seller);
    $view = $presenter->buildView($this->fileRepository);

    return $this->render('job/view.twig', [
      'view' => $view,
    ]);
  }

}
