<?php


namespace Framework\Web\Controllers\Client\Seller;

use Core\Client\Seller\ISellerRepository;
use Core\Client\Seller\Presenter\MyJobsJobPresenter;
use Core\File\IFileRepository;
use Core\Job\Category\ICategoryRepository;
use Core\Job\IJobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;


class MyJobsController extends AbstractController {
  public function __construct(private RequestStack $requestStack) {
  }

  /**
   * @param Request $request
   * @param \Core\Client\Seller\ISellerRepository $sellerRepository
   * @param IJobRepository $jobRepository
   * @param ICategoryRepository $categoryRepository
   * @param IFileRepository $fileRepository
   *
   * @return RedirectResponse|Response
   */
	public function __invoke(
		Request $request,
		ISellerRepository $sellerRepository,
		IJobRepository $jobRepository,
		ICategoryRepository $categoryRepository,
		IFileRepository $fileRepository,
    MyJobsJobPresenter $presenter,
  ) {

		$session = $this->requestStack->getSession();
		$userId = $session->get('userId');

		$seller = $sellerRepository->findByUserId($userId);

		if (null === $seller) {
			$this->addFlash('error', 'Seller not found.');

			return $this->redirectToRoute('index');
		}

		$job_views = [];
		$jobs = $seller->getJobs($jobRepository);
		foreach ($jobs as $job) {
			$category = $categoryRepository->findByCategoryId($job->categoryId);
			// @todo: надо DTO иметь для такого случая и в нем сразу иметь картинки и проч.
			$jobImage = $fileRepository->findById($job->imageFileId);
			$job_views[] = $presenter->present($job, $category, $jobImage);
		}

		return $this->render('client/seller/job/myjobs.twig', ['job_views' => $job_views]);
	}
}
