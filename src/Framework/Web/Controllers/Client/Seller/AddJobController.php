<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.03.2019
 * Time: 3:31
 */

namespace Framework\Web\Controllers\Client\Seller;

use Core\Client\Seller\UseCases\AddJob\AddJobService;
use Core\File\IFileService;
use Core\Job\Request\JobAddRequest;
use Framework\Web\Job\FormTypes\Create;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AddJobController
 */
class AddJobController extends AbstractController {

  public function __construct(
    private AddJobService $addJobService,
    private IFileService $fileService,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function __invoke(Request $request) {
    /**
     * {@see \Framework\Web\Auth\LoggedListener::onKernelController}
     */
    $user = $request->attributes->get('user');

    $form = $this->createForm(Create::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->getData();
//      $data['image'] = $this->fileService->moveTempToPublic($data['image']['file_name'], $user->id);

      $addRequest = new JobAddRequest($data);
      $job = $this->addJobService->add($user, $addRequest);

      return $this->redirectToRoute('seller_my_job', ['job_id' => (string)$job->id]);
    }

    return $this->render('client/seller/job/add.twig', [
      'form' => $form->createView(),
    ]);
  }

}
