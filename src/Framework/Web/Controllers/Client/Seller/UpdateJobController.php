<?php


namespace Framework\Web\Controllers\Client\Seller;

use Core\Client\Seller\UseCases\ChangeJob\ChangeJobService;
use Core\Job\JobData;
use Core\Job\JobId;
use Framework\Web\Job\FormTypes\Edit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UpdateJobController extends AbstractController {

  public function __construct(
    private ChangeJobService $changeJobService,
  ) {

  }


  /**
   * @inheritDoc
   */
  public function __invoke(string $job_id, Request $request) {
    $jobData = $this->changeJobService->getJobDataByJobId(new JobId($job_id));

    $form = $this->createForm(Edit::class, $jobData);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      /**
       * {@see \Framework\Web\Auth\LoggedListener::onKernelController}
       */
      $user = $request->attributes->get('user');

      /** @var JobData $appJob */
      $appJob = $form->getData();

      // @todo: надо по уму. Например, если меняются важные поля (вроде смены статуса),
      // то используем метод на вроде Seller::changeJobState($jobId, $newState)
      $job = $this->changeJobService->change($user, $appJob);

      return $this->redirectToRoute('seller_my_jobs', ['job_id' => (string)$job->id]);
    }

    return $this->render('client/seller/job/update.twig', [
      'form' => $form->createView(),
    ]);
  }
}
