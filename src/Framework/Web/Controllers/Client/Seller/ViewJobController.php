<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.03.2019
 * Time: 21:32
 */

namespace Framework\Web\Controllers\Client\Seller;


use Core\Client\Seller\Presenter\JobPagePresenter;
use Core\File\IFileRepository;
use Core\Job\Category\ICategoryRepository;
use Core\Job\IJobRepository;
use Core\Job\JobId;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ViewJobController extends AbstractController {

  public function __construct(
    private IJobRepository $jobRepository,
    private ICategoryRepository $categoryRepository,
    private IFileRepository $fileRepository,
    private JobPagePresenter $presenter,
  ) {
  }


  /**
   * @inheritDoc
   */
  public function __invoke(string $job_id) {

    $jobId = new JobId($job_id);

    $job = $this->jobRepository->getByJobId($jobId);
    $category = $this->categoryRepository->findByCategoryId($job->categoryId);

    $view = $this->presenter->present($this->fileRepository, $job, $category);

    return $this->render('client/seller/job/view.twig', [
      'view' => $view,
    ]);
  }

}
