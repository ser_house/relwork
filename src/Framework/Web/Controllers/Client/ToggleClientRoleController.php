<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:02
 */

namespace Framework\Web\Controllers\Client;


use Core\Client\Service\ClientToggleRole;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;


class ToggleClientRoleController extends AbstractController {
  public function __construct(private RequestStack $requestStack) {
  }
	public function __invoke(Request $request, ClientToggleRole $toggleRole) {

		$session = $this->requestStack->getSession();
		$userId = $session->get('userId');

    if (empty($userId)) {
      throw new \Exception("Missing userId in session.");
    }
		$toggleRole->toggle($userId);

		$target_route = $request->get('src');
    if (empty($target_route)) {
      $target_route = 'index';
    }
		return $this->redirectToRoute($target_route);
	}
}
