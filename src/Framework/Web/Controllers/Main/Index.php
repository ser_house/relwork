<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.01.2019
 * Time: 12:26
 */

namespace Framework\Web\Controllers\Main;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class Index extends AbstractController {

  public function __invoke(Request $request) {
    $session = $request->getSession();
    $http_code = $session->get('http_code');
    $response = null;
    if ($http_code) {
      $response = new Response('', $http_code);
      $session->remove('http_code');
    }
    return $this->render('main/index.twig', [], $response);
  }

}
