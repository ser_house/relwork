<?php


namespace Framework\Web\Controllers\User;

use Core\User\UseCases\ViewProfile\Action as ViewProfileAction;
use Core\User\UserId;
use Framework\Web\Client\Templates;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends AbstractController {

  public function __construct(
    private ViewProfileAction $action,
    private Templates $templates,
  ) {
  }

  public function __invoke(Request $request) {
    /**
     * {@see \Framework\Web\Auth\LoggedListener::onKernelController}
     */
    $user = $request->attributes->get('user');
    $view = $this->action->getView($user);
    return $this->render($this->templates->profileTemplateByRole($user->role), ['view' => $view]);
  }
}
