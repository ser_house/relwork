<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 0:39
 */

namespace Framework\Web\Controllers\Auth;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;


class Logout extends AbstractController {
  public function __construct(private RequestStack $requestStack) {
  }

	public function __invoke(Request $request): Response {

		$session = $this->requestStack->getSession();
		if ($session) {
			$session->remove('userId');
		}

		return $this->redirectToRoute('index');
	}
}
