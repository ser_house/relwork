<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.01.2019
 * Time: 12:26
 */

namespace Framework\Web\Controllers\Auth;

use Core\System\UseCases\Registration\Registration;
use Core\User\Email\Email;
use Framework\Web\User\Registration\SendEmail\RegistrationActionFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class StartRegistration extends AbstractController {

	public function __invoke(Request $request, RegistrationActionFactory $registrationActionFactory) {

		$form = $this->createFormBuilder()
			->add('email', EmailType::class, [
				'label' => 'Email',
				'required' => true,
				'help' => 'На этот адрес будет отправлено письмо с инструкцией.',
			])
			->add('Send', SubmitType::class, [
        'row_attr' => ['class' => 'actions'],
      ])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			$registration = new Registration(new Email($data['email']));

			$action = $registrationActionFactory->getAction($registration);
			$action->execute();

			$form = null;
		}

		return $this->render('auth/registration/form.twig', [
			'form' => $form ? $form->createView() : null,
		]);
	}

}
