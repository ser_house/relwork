<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.01.2019
 * Time: 2:55
 */

namespace Framework\Web\Controllers\Auth;

use Core\Generic\Errors\Exceptions\InvalidInputDataException;
use Core\System\UseCases\Registration\Exceptions\ExpiredException;
use Core\System\UseCases\Registration\Exceptions\NotFoundException;
use Core\System\UseCases\Registration\RegistrationService;
use Core\User\Email\Email;
use Core\User\Exceptions\MissingMailCodeException;
use Core\User\UseCases\CreateProfile\Action as CreateProfileAction;
use Core\User\UseCases\CreateProfile\Request as CreateProfileRequest;
use Framework\File\UploadedFile;
use Framework\Generic\Errors\Exceptions\FieldErrorsFromExceptionsBuilder;
use Framework\Generic\Errors\HttpFieldLabels;
use Framework\Web\CsrfTokenFailResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;


/**
 * Class CreateProfile
 */
class CreateProfile extends AbstractController {

  public function __construct(
    private RegistrationService $registrationService,
    private CreateProfileAction $createProfileAction,
    private HttpFieldLabels $fieldErrorLabels,
    private FieldErrorsFromExceptionsBuilder $errorsBuilder,
  ) {
  }

  public function __invoke(Request $request) {
    $session = $request->getSession();

    try {
      $email = $this->registrationService->getEmailFromCode($request->get('arg'));
    }
    catch (NotFoundException $e) {
      $this->addFlash('error', $e->getMessage());
      $session->set('http_code', Response::HTTP_NOT_FOUND);
      return $this->redirectToRoute('index');
    }
    catch (MissingMailCodeException $e) {
      $this->addFlash('error', $e->getMessage());
      $session->set('http_code', Response::HTTP_BAD_REQUEST);
      return $this->redirectToRoute('index');
    }
    catch(ExpiredException $e) {
      $this->addFlash('error', $e->getMessage());
      $session->set('http_code', Response::HTTP_SERVICE_UNAVAILABLE);
      return $this->redirectToRoute('index');
    }
    catch (Throwable $e) {
      $this->addFlash('error', $e->getMessage());
      $session->set('http_code', Response::HTTP_INTERNAL_SERVER_ERROR);
      return $this->redirectToRoute('index');
    }

    $values = $this->createProfileAction->defaultValues();

    if ($request->isMethod('POST')) {
      if (!$this->isCsrfTokenValid('profile-form', $request->get("token"))) {
        return new CsrfTokenFailResponse();
      }

      $data = $request->request->all();
      $values = array_merge($values, $data);

      $avatar = $request->files->get('avatar');
      $values['avatar'] = $avatar ? new UploadedFile($avatar) : null;

      try {
        $createProfileRequest = new CreateProfileRequest(new Email($email), $values);
        $user = $this->createProfileAction->handle($createProfileRequest);
        $session->set('userId', $user->id);
        $session->set('password', $createProfileRequest->password);
        return $this->redirectToRoute('registration_profile_created');
      }
      catch (InvalidInputDataException $e) {
        $errors = $this->errorsBuilder->build($this->fieldErrorLabels, $e->exceptionsCollection);
        $values['errors'] = $errors->getErrors();
        $messages = $errors->listErrors();
        $message = implode('<br>', $messages);
        $this->addFlash('error', $message);
      }
      catch (Throwable $e) {
        $this->addFlash('error', $e->getMessage());
      }
    }

    return $this->render('auth/registration/create_profile.twig', $values);
  }
}
