<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.01.2019
 * Time: 2:55
 */

namespace Framework\Web\Controllers\Auth;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class ProfileCreated extends AbstractController {

  public function __invoke(Request $request) {
    $session = $request->getSession();
    $password = $session->get('password');
    if (!$password) {
      $this->addFlash('error', 'Ошибка создания профиля, обратитесь к администратору.');
      return $this->redirectToRoute('index');
    }

    $this->addFlash('success', 'Профиль создан.');
    $values = [
      'password' => $password,
    ];
    return $this->render('auth/registration/profile_created.twig', $values);
  }
}
