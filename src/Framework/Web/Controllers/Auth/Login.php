<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2019
 * Time: 9:27
 */

namespace Framework\Web\Controllers\Auth;


use Core\User\IUserRepository;
use Core\User\NickName;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class Login extends AbstractController {

  public function __construct(private RequestStack $requestStack) {
  }

  public function __invoke(Request $request, IUserRepository $userRepository): Response
	{
//		// get the login error if there is one
//		$error = $authenticationUtils->getLastAuthenticationError();
//		// last username entered by the user
//		$lastUsername = $authenticationUtils->getLastUsername();

		$session = $this->requestStack->getSession();
		$error = $this->checkError($request);

		$name = $request->get('name');
		if ($name) {
			$lastUsername = $name;

			$user = $userRepository->findByNickname(new NickName($name));
			if ($user && password_verify($request->get('password'), (string)$user->password)) {
				$session->set('userId', $user->id);
				$this->addFlash('info', 'Вы успешно вошли в систему!');
				return $this->redirectToRoute('index');
			}

			$this->addFlash('info', 'Пользователь с таким ником не найден.');
		}
		else {
			$lastUsername = null === $session ? '' : $session->get(Security::LAST_USERNAME, '');
		}

		return $this->render('auth/login.twig', [
			'last_username' => $lastUsername,
			'error' => $error
		]);
	}

  private function checkError(Request $request) {
    $session = $this->requestStack->getSession();
    $authenticationException = null;

    if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
      $authenticationException = $request->attributes->get(Security::AUTHENTICATION_ERROR);
    }
    elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
      $authenticationException = $session->get(Security::AUTHENTICATION_ERROR);

      $session->remove(Security::AUTHENTICATION_ERROR);
    }

    return $authenticationException;
  }
}
