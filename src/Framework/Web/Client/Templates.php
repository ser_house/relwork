<?php

namespace Framework\Web\Client;

use Core\User\Role;

class Templates {

  public function profileMenuTemplateByRole(Role $role): string {
    return match($role) {
      Role::SELLER => 'client/seller/menu.twig',
      Role::BUYER => 'client/buyer/menu.twig',
      default => throw new \Exception("Profile menu template for role $role->name not defined."),
    };
  }

  public function profileTemplateByRole(Role $role): string {
    return match($role) {
      Role::SELLER => 'client/seller/profile.twig',
      Role::BUYER => 'client/buyer/profile.twig',
      default => throw new \Exception("Profile template for role $role->name not defined."),
    };
  }
}
