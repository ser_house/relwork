<?php

namespace Framework\Web;

use Symfony\Component\HttpFoundation\Response;

class CsrfTokenFailResponse extends Response {

  public function __construct() {

    parent::__construct(
      "Operation not allowed",
      Response::HTTP_BAD_REQUEST,
      ['content-type' => 'text/plain']
    );
  }
}
