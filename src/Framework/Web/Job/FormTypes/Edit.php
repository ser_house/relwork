<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.03.2019
 * Time: 4:12
 */

namespace Framework\Web\Job\FormTypes;

use Core\File\FilePathManager;
use Core\Job\Category\ICategoryRepository;
use Core\Job\ImageData;
use Core\Job\JobData;
use Core\Job\State;
use Framework\File\Image\ImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\File as FrameworkFile;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class Edit
 */
class Edit extends Base {

	/**
	 * Edit constructor.
	 *
	 * @param ICategoryRepository $categoryRepository
	 * @param FilePathManager $filePathManager
	 */
	public function __construct(
    protected ICategoryRepository $categoryRepository,
    protected FilePathManager $filePathManager) {

		parent::__construct($categoryRepository, $filePathManager);
	}


	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$categoryChoices = parent::getCategoryChoices();

		// @todo: набор доступных для установки статусов задачи определяется бизнес-логикой.
		// Соответственно, нужен фильтр, который будет строить такой набор в зависимости
		// от различных факторов (текущего состояния задачи, кто хочет изменить её статус и проч.).
    $states = array_map(fn(State $state) => (string)$state->title(), State::cases());

		$builder->add('title', TextType::class, [
			'label' => 'Title',
			'required' => true,
		])
			->add('category_id', ChoiceType::class, [
				'label' => 'Category',
				'required' => true,
				'placeholder' => 'Choose an Category',
				'choices' => $categoryChoices,
			])
			->add('state', ChoiceType::class, [
				'label' => 'State',
				'required' => true,
				'placeholder' => 'Choose an State',
				'choices' => array_flip($states),
			])
			->add('description', TextareaType::class, [
				'label' => 'Description',
				'required' => true,
			])
			->add('image', ImageType::class, [
				'label' => 'Image',
				'required' => true,
				'help' => 'Acceptable image formats: jpeg, png.',
			])
			->add('files', FileType::class, [
				'label' => 'Files',
				'required' => false,
				'multiple' => true,
				'mapped' => false,
			])
			->add('cost', MoneyType::class, [
				'label' => 'Cost',
				'required' => true,
				'currency' => 'RUR',
			])
			->add('hours', NumberType::class, [
				'label' => 'Hours',
				'required' => true,
				'scale' => 2,
			])
			->add('Save', SubmitType::class, [
        'row_attr' => ['class' => 'actions'],
      ]);

		parent::buildForm($builder, $options);
	}

	/**
	 * @param FormEvent $event
	 */
	public function preSubmit(FormEvent $event) {
		$data = $event->getData();

		$image = ImageData::buildFromData($data['image']);
		if (!empty($image->file_name)) {
			if ($image->is_temp) {
				$file_path = $this->filePathManager->getTempPublicFilePath($image->file_name);
			}
			else {
				$file_path = $this->filePathManager->getPublicFilePathFromUri($image->uri);
			}
			$data['image']['image'] = new FrameworkFile($file_path);
		}

		$event->setData($data);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'data_class' => JobData::class,
		]);
	}
}
