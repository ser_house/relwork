<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 01.03.2019
 * Time: 4:12
 */

namespace Framework\Web\Job\FormTypes;


use Core\Job\ImageData;
use Framework\File\Image\ImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\File as FrameworkFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Class Create
 */
class Create extends Base {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$categoryChoices = parent::getCategoryChoices();

		$builder->add('title', TextType::class, [
			'label' => 'Title',
			'required' => true,
			'constraints' => [
				new NotBlank()
			],
		])
			->add('category', ChoiceType::class, [
				'label' => 'Category',
				'required' => true,
				'placeholder' => 'Choose an Category',
				'choices' => $categoryChoices,
				'constraints' => [
					new NotBlank()
				],
			])
			->add('description', TextareaType::class, [
				'label' => 'Description',
				'required' => true,
				'constraints' => [
					new NotBlank()
				],
			])
			->add('image', ImageType::class, [
				'label' => 'Image',
				'required' => true,
				'help' => 'Acceptable image formats: jpeg, png.',
			])
			->add('files', FileType::class, [
				'label' => 'Files',
				'required' => false,
				'multiple' => true,
			])
			->add('cost', MoneyType::class, [
				'label' => 'Cost',
				'required' => true,
				'currency' => 'RUR',
				'constraints' => [
					new NotBlank()
				],
			])
			->add('hours', NumberType::class, [
				'label' => 'Hours',
				'required' => true,
				'scale' => 2,
				'constraints' => [
					new NotBlank()
				],
			])
			->add('Save', SubmitType::class, [
        'row_attr' => ['class' => 'actions'],
      ]);

		parent::buildForm($builder, $options);
	}

	/**
	 * @param FormEvent $event
	 */
	public function preSubmit(FormEvent $event) {
		$data = $event->getData();

		$image = ImageData::buildFromData($data['image']);
		if ($image->is_temp && !empty($image->file_name)) {
			$file_path = $this->filePathManager->getTempPublicFilePath($image->file_name);

			$data['image']['image'] = new FrameworkFile($file_path);

			$event->setData($data);
		}
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'title' => '',
			'category' => null,
			'description' => '',
			'image' => null,
			'files' => [],
			'cost' => 0,
			'hours' => 0,
		]);
	}
}
