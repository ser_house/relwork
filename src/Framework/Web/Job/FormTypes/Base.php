<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.08.2019
 * Time: 13:02
 */


namespace Framework\Web\Job\FormTypes;


use Core\File\FilePathManager;
use Core\Job\Category\ICategoryRepository;
use Core\Job\Category\TreeBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;


/**
 * Class Base
 */
class Base extends AbstractType {

	public function __construct(
    protected ICategoryRepository $categoryRepository,
    protected FilePathManager $filePathManager,
  ) {
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'preSubmit']);
	}

	/**
	 *
	 * @return array
	 */
	protected function getCategoryChoices(): array {
		$categories = $this->categoryRepository->findAll();
    return (new TreeBuilder())->buildAsChoices($categories);
	}
}
