<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:05
 */

namespace Framework\Web\User\Registration\SendEmail\Action;

use Core\System\UseCases\Registration\IAction;

class TimeNotPassedAction extends BaseAction implements IAction {
	/**
	 * @inheritDoc
	 */
	public function execute(): void {
    $this->requestStack->getSession()->getFlashBag()->add('warning', 'Письмо с инструкциями уже выслано.');
	}
}
