<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:31
 */

namespace Framework\Web\User\Registration\SendEmail\Action;


use Core\System\UseCases\Registration\Registration;
use Symfony\Component\HttpFoundation\RequestStack;

class BaseAction extends \Core\System\UseCases\Registration\BaseAction {


	public function __construct(
    protected Registration $registration,
    protected RequestStack $requestStack,
  ) {
		parent::__construct($registration);
	}
}
