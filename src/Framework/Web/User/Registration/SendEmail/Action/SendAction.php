<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:04
 */

namespace Framework\Web\User\Registration\SendEmail\Action;

use Core\System\UseCases\Registration\IAction;
use Core\System\UseCases\Registration\IRegistrationRepository;
use Core\System\UseCases\Registration\IUrlGenerator;
use Core\System\UseCases\Registration\Registration;
use Core\User\Email\IEmailSender;
use Symfony\Component\HttpFoundation\RequestStack;


class SendAction extends BaseAction implements IAction {

	public function __construct(
    protected Registration $registration,
    protected RequestStack $requestStack,
    private IRegistrationRepository $registrationRepository,
    private IUrlGenerator $urlGenerator,
    private IEmailSender $emailSender,
  ) {
		parent::__construct($registration, $requestStack);
	}

	/**
	 * @inheritDoc
	 */
	public function execute(): void {
    $email = $this->registration->email;
    $url = $this->urlGenerator->generateCreateProfileUrl($email);

    $subject = 'Регистрация на сайте ' . getenv('APP_NAME');
    $content = [
      'site_name' => getenv('APP_NAME'),
      'reg_url' => $url,
    ];
    $this->emailSender->sendRegistrationEmail($email, $subject, $content);

		$this->registrationRepository->save($this->registration);

    $mail_url = $_ENV['MAIL_URL'];
		$this->requestStack->getSession()->getFlashBag()->add('info', "Инструкция выслана. <a href=$mail_url>Mail</a>");
	}
}
