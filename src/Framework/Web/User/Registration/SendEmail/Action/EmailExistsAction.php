<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 14:07
 */

namespace Framework\Web\User\Registration\SendEmail\Action;

use Core\System\UseCases\Registration\IAction;

class EmailExistsAction extends BaseAction implements IAction {

	/**
	 * @inheritDoc
	 */
	public function execute(): void {
    $this->requestStack->getSession()->getFlashBag()->add('error', 'Пользователь с такой почтой уже зарегистрирован.');
	}
}
