<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 4:18
 */

namespace Framework\Web\User\Registration\SendEmail;


use Core\System\UseCases\Registration\IAction;
use Core\System\UseCases\Registration\IRegistrationActionFactory;
use Core\System\UseCases\Registration\IRegistrationRepository;
use Core\System\UseCases\Registration\IUrlGenerator;
use Core\System\UseCases\Registration\Registration;
use Core\User\Email\IEmailSender;
use Core\User\IUserRepository;
use Framework\Web\User\Registration\SendEmail\Action\EmailExistsAction;
use Framework\Web\User\Registration\SendEmail\Action\SendAction;
use Framework\Web\User\Registration\SendEmail\Action\TimeNotPassedAction;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class RegistrationActionFactory
 */
class RegistrationActionFactory implements IRegistrationActionFactory {

	public function __construct(
    private IUserRepository $userRepository,
    private IRegistrationRepository $registrationRepository,
    private RequestStack $requestStack,
    private IUrlGenerator $urlGenerator,
    private IEmailSender $emailSender,
  ) {
	}


	/**
	 * @inheritDoc
	 */
	public function getAction(Registration $registration): IAction {
		// Если на такую почту письмо еще не отправлялось
		//   если с такой почтой пользователя еще нет
		//     отправить письмо с инструкцией
		//     сделать об этом запись
		//   если с такой почтой есть пользователь
		//     вывести ссылку на "восстановление" (на самом деле - изменение) пароля
		// если письмо уже отправлялось
		//   если время прошло
		//     отправить письмо с инструкцией
		//   если время не прошло
		//     вывести сообщение о том, что письмо уже отправлено, проверьте спам
		if ($registration->isSent($this->registrationRepository)) {
			if (!$registration->isPauseTimeExpired()) {
				return new TimeNotPassedAction($registration, $this->requestStack);
			}

			return new SendAction(
				$registration,
        $this->requestStack,
				$this->registrationRepository,
        $this->urlGenerator,
        $this->emailSender,
       );
		}

		if ($registration->emailIsExists($this->userRepository)) {
			return new EmailExistsAction($registration, $this->requestStack);
		}

		return new SendAction(
			$registration,
      $this->requestStack,
			$this->registrationRepository,
			$this->urlGenerator,
			$this->emailSender,
    );
	}
}
