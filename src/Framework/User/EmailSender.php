<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.01.2019
 * Time: 5:53
 */

namespace Framework\User;

use Core\User\Email\Email;
use Core\User\Email\IEmailSender;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailSender implements IEmailSender {

  public function __construct(
    private MailerInterface $mailer,
  ) {

  }

  /**
   * @inheritDoc
   */
  public function sendRegistrationEmail(Email $email, string $subject, array $content): void {
    $this->send($email, $subject, 'auth/registration/email.twig', $content);
  }

  /**
   * @inheritDoc
   */
  public function send(Email $email, string $subject, string $template, array $content): void {

    $message = (new TemplatedEmail())
      ->from('send@example.com')
      ->to(new Address($email))
      ->subject($subject)

      // path of the Twig template to render
      ->htmlTemplate($template)

      // pass variables (name => value) to the template
      ->context($content);

    $this->mailer->send($message);
  }
}
