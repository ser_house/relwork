<?php

namespace Framework\User\Registration;

use Core\Generic\Infrastructure\EncodeStringService;
use Core\System\UseCases\Registration\IUrlGenerator;
use Core\User\Email\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UrlGenerator implements IUrlGenerator {

  public function __construct(
    private UrlGeneratorInterface $urlGenerator,
    private EncodeStringService $encodeStringService
  ) {
  }

  public function generateCreateProfileUrl(Email $email): string {
    return $this->urlGenerator->generate(
      'registration_create_profile',
      ['arg' => $this->encodeStringService->encode($email)],
      UrlGeneratorInterface::ABSOLUTE_URL
    );
  }

}
