<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.01.2019
 * Time: 0:43
 */

namespace Framework\User\Registration;

use Core\System\UseCases\Registration\Exceptions\NotFoundException;
use Core\System\UseCases\Registration\IRegistrationRepository;
use Core\System\UseCases\Registration\Registration;
use Core\User\Email\Email;
use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\ParameterType;

/**
 * Class RegistrationRepository
 */
class RegistrationRepository implements IRegistrationRepository {

	public function __construct(private Connection $connection) {

	}

	/**
	 * @param Registration $registration
	 *
	 * @throws Exception
	 */
	public function save(Registration $registration): void {
		$sql = "INSERT INTO registration (email, sent_at) 
		VALUES (:email, 'now')
			ON CONFLICT (email) DO UPDATE 
				SET sent_at = excluded.sent_at";

		$stmt = $this->connection->prepare($sql);
		$email = $registration->email;
		$email_str = (string)$email;
		$stmt->bindParam(':email', $email_str);
		$stmt->executeStatement();
	}

	/**
	 * @param Email $email
	 *
	 * @return Registration|null
	 * @throws \Exception
	 */
	public function findByEmail(Email $email): ?Registration {
		$email_str = (string)$email;
		$queryBuilder = $this->connection->createQueryBuilder();
		$sent_at = $queryBuilder->select('sent_at')
			->from('registration')
			->where('email = ?')
			->setParameter(0, $email_str, ParameterType::STRING)
			->fetchOne();

		return $sent_at ? new Registration($email, new DateTimeImmutable($sent_at)): null;
	}

  /**
   * @inheritDoc
   */
  public function getByEmail(Email $email): Registration {
    $registration = $this->findByEmail($email);
    if (null === $registration) {
      throw new NotFoundException("Registration for '$email' not found.");
    }
    return $registration;
  }

  /**
   * @param Email $email
   *
   * @throws Exception
   * @throws Exception
   */
	public function removeByEmail(Email $email): void {
		$email_str = (string)$email;
		$queryBuilder = $this->connection->createQueryBuilder();
		$queryBuilder->delete('registration')
			->where('email = ?')
			->setParameter(0, $email_str, ParameterType::STRING)
			->executeQuery();
	}
}
