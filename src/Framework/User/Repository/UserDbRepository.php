<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 2:39
 */

namespace Framework\User\Repository;

use Core\User\Hydrator;
use Core\User\Email\Email;
use Core\User\Exceptions\UserNotFoundException;
use Core\User\IUserRepository;
use Core\User\NickName;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

/**
 * Class UserDbRepository
 */
class UserDbRepository implements IUserRepository {

  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {
  }


  /**
   * @inheritDoc
   */
  public function existsUserWithEmail(string $email): bool {
    $queryBuilder = $this->connection->createQueryBuilder();
    $user_id = $queryBuilder->select('id')
      ->from('user_account')
      ->where('email = ?')
      ->setParameter(0, $email, ParameterType::STRING)
      ->fetchOne();

    return !empty($user_id);
  }

  /**
   * @inheritDoc
   */
  public function findByUserId(UserId $userId): ?User {
    $item = $this->connection->fetchAssociative("SELECT * FROM user_account WHERE id = ?", [(string)$userId]);
    if (empty($item)) {
      return null;
    }

    /** @var User $user */
    $user = $this->hydrator->hydrate($item, User::class);
    return $user;
  }

  /**
   * @inheritDoc
   */
  public function getByUserId(UserId $userId): User {
    $user = $this->findByUserId($userId);
    if (null === $user) {
      throw new UserNotFoundException("User by userId '$userId' not found in repository.");
    }
    return $user;
  }


  /**
   * @inheritDoc
   */
  public function findByNickname(NickName $name): ?User {
    $queryBuilder = $this->connection->createQueryBuilder();
    $name_str = (string)$name;
    $item = $queryBuilder->select('*')
      ->from('user_account')
      ->where('name = ?')
      ->setParameter(0, $name_str, ParameterType::STRING)
      ->fetchAssociative();

    if (empty($item)) {
      return null;
    }

    /** @var User $user */
    $user = $this->hydrator->hydrate($item, User::class);
    return $user;
  }

  /**
   * @inheritDoc
   */
  public function findByEmail(Email $email): ?User {
    $queryBuilder = $this->connection->createQueryBuilder();
    $email_str = (string)$email;
    $item = $queryBuilder->select('*')
      ->from('user_account')
      ->where('email = ?')
      ->setParameter(0, $email_str, ParameterType::STRING)
      ->fetchAssociative();

    if (empty($item)) {
      return null;
    }

    /** @var User $user */
    $user = $this->hydrator->hydrate($item, User::class);
    return $user;
  }

  /**
   * @inheritDoc
   */
  public function updateRole(UserId $userId, Role $newRole): void {
    $this->connection->update('user_account', ['role' => $newRole->value], ['id' => (string)$userId]);
  }
}
