<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 2:39
 */

namespace Framework\Client\Repository;

use Core\Client\Buyer\Buyer;
use Core\Client\Buyer\IBuyerRepository;
use Core\Client\Exception\BuyerNotFoundException;
use Core\Client\Buyer\Hydrator;
use Core\Generic\Infrastructure\StaticCache;
use Core\User\UserId;
use Doctrine\DBAL\Connection;

/**
 * Class BuyerRepository
 */
class BuyerRepository implements IBuyerRepository {
  protected StaticCache $cache;

  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {
    $this->cache = new StaticCache();
  }

  /**
   * @inheritDoc
   */
  public function findByUserId(UserId $userId): ?Buyer {
    $sql = "SELECT 
      u.*,
      c.settings,
      c.balance,
      c.reserved
    FROM client AS c
    INNER JOIN user_account AS u ON u.id = c.user_id
    WHERE c.user_id = ?";
    $item = $this->connection->fetchAssociative($sql, [(string)$userId]);
    if (empty($item)) {
      return null;
    }

    /** @var Buyer $buyer */
    $buyer = $this->hydrator->hydrate($item, Buyer::class);
    return $buyer;
  }

  /**
   * @inheritDoc
   */
  public function getByUserId(UserId $userId): Buyer {
    $key = "buyer_$userId";
    if (!$this->cache->has($key)) {
      $buyer = $this->findByUserId($userId);
      if (null === $buyer) {
        throw new BuyerNotFoundException("Buyer by userId '$userId' not found in repository.");
      }
      $this->cache->set($key, $buyer);
    }

    return $this->cache->get($key);
  }

  /**
   * @inheritDoc
   */
  public function add(Buyer $buyer): void {
    $user = $buyer->user;

    $data = [
      'id' => (string)$buyer->getId(),
      'created_at' => $user->createdAt->format('Y-m-d H:i'),
      'name' => (string)$user->name,
      'email' => (string)$user->email,
      'password' => (string)$user->password,
      'timezone' => $user->timeZone->getName(),
      'avatar_name' => $user->avatar->name,
      'avatar_rel_dir' => $user->avatar->rel_dir,
      'avatar_uri' => $user->avatar->uri,
      'role' => $user->role->value,
    ];
    $this->connection->insert('user_account', $data);

    $data = [
      'user_id' => (string)$buyer->getId(),
      'settings' => json_encode($buyer->settings, JSON_THROW_ON_ERROR),
      'balance' => (string)$buyer->balance->getAmount(),
      'reserved' => (string)$buyer->getReserved()->getAmount(),
    ];
    $this->connection->insert('client', $data);
  }
}
