<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.02.2019
 * Time: 21:48
 */

namespace Framework\Client\Repository;


use Core\Client\Client;
use Core\Client\IClientRepository;
use Core\Client\Seller\Level;
use Core\Client\Seller\Rating;
use Core\Client\Settings;
use Core\Money\Money;
use Core\User\IUserRepository;
use Core\User\Role;
use Core\User\UserId;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;

class ClientRepository implements IClientRepository {

	public function __construct(private Connection $connection, private IUserRepository $userRepository) {

	}
	/**
	 * @inheritDoc
	 */
	public function findByUserId(UserId $userId): ?Client {
    $sql = "SELECT 
      c.*, 
      u.role
    FROM client AS c
    INNER JOIN user_account AS u ON u.id = c.user_id
    WHERE c.user_id = ?
    ";
    $item = $this->connection->fetchAssociative($sql, [(string)$userId]);
		if (empty($item)) {
			return null;
		}

		return $this->dbItemToClient($item);
	}

	/**
	 * @param array $dbItem
	 *
	 * @return Client
	 * @throws Exception
	 */
	private function dbItemToClient(array $dbItem): Client {
		$userId = new UserId($dbItem['user_id']);
		$role = Role::from($dbItem['role']);
		$dbSettings = json_decode($dbItem['settings'], true, 512, JSON_THROW_ON_ERROR);
		$settings = new Settings($dbSettings['universal']);
		$balance = new Money($dbItem['balance']);
		$reserved = new Money($dbItem['reserved']);
		$rating = new Rating($dbItem['rating']);
		$level = Level::from($dbItem['level']);

		return new Client($userId, $settings, $role, $balance, $reserved, $rating, $level);
	}

	/**
	 * @inheritDoc
	 */
	public function updateRole(UserId $userId, Role $newRole): void {
		$this->userRepository->updateRole($userId, $newRole);
	}
}
