<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.01.2019
 * Time: 2:39
 */

namespace Framework\Client\Repository;

use Core\Client\Exception\SellerNotFoundException;
use Core\Client\Seller\ISellerRepository;
use Core\Client\Seller\Seller;
use Core\Client\Seller\Hydrator;
use Core\Generic\Infrastructure\StaticCache;
use Core\User\UserId;
use Doctrine\DBAL\Connection;

/**
 * Class SellerRepository
 */
class SellerRepository implements ISellerRepository {
  protected StaticCache $cache;

  public function __construct(
    private Connection $connection,
    private Hydrator $hydrator,
  ) {
    $this->cache = new StaticCache();
  }

  /**
   * @inheritDoc
   */
  public function findByUserId(UserId $userId): ?Seller {
    $sql = "SELECT 
      u.*,
      c.settings,
      c.balance,
      c.rating,
      c.level 
    FROM client AS c
    INNER JOIN user_account AS u ON u.id = c.user_id
    WHERE c.user_id = ?";
    $item = $this->connection->fetchAssociative($sql, [(string)$userId]);
    if (empty($item)) {
      return null;
    }

    /** @var Seller $seller */
    $seller = $this->hydrator->hydrate($item, Seller::class);
    return $seller;
  }

  /**
   * @inheritDoc
   */
  public function getByUserId(UserId $userId): Seller {
    $key = "seller_$userId";
    if (!$this->cache->has($key)) {
      $seller = $this->findByUserId($userId);
      if (null === $seller) {
        throw new SellerNotFoundException("Seller by userId '$userId' not found in repository.");
      }
      $this->cache->set($key, $seller);
    }

    return $this->cache->get($key);
  }

  /**
   * @inheritDoc
   */
  public function add(Seller $seller): void {
    $user = $seller->user;

    $data = [
      'id' => (string)$seller->getId(),
      'created_at' => $user->createdAt->format('Y-m-d H:i'),
      'name' => (string)$user->name,
      'email' => (string)$user->email,
      'password' => (string)$user->password,
      'timezone' => $user->timeZone->getName(),
      'avatar_name' => $user->avatar->name,
      'avatar_rel_dir' => $user->avatar->rel_dir,
      'avatar_uri' => $user->avatar->uri,
      'role' => $user->role->value,
    ];
    $this->connection->insert('user_account', $data);

    $data = [
      'user_id' => (string)$seller->getId(),
      'settings' => json_encode($seller->settings, JSON_THROW_ON_ERROR),
      'balance' => (string)$seller->getBalance()->getAmount(),
      'rating' => (string)$seller->getRating()->value(),
      'level' => $seller->getLevel()->value,
    ];
    $this->connection->insert('client', $data);
  }
}
