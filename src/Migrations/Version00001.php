<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.01.2019
 * Time: 17:43
 */

namespace DoctrineMigrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;


class Version00001 extends AbstractMigration {

	public function up(Schema $schema): void {
		// gen_random_uuid()
		$sql = "CREATE EXTENSION IF NOT EXISTS pgcrypto";
		$this->addSql($sql);

		try {
			$this->create($schema);
			$this->fill($schema);
		}
		catch (\Throwable $e) {
			$this->down($schema);
			throw $e;
		}
	}

	private function create(Schema $schema) {

		// user
		$sql = "CREATE TABLE IF NOT EXISTS user_account (
				id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
				name VARCHAR (64) NOT NULL,
				created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
				email VARCHAR (255) UNIQUE NOT NULL,
				password VARCHAR (255) NOT NULL,
				timezone VARCHAR (64) NOT NULL,
				avatar_name VARCHAR (64) NOT NULL,
				avatar_rel_dir VARCHAR(1024) NOT NULL,
				avatar_uri VARCHAR (255) NOT NULL,
				role SMALLINT NOT NULL
			)";
		$this->addSql($sql);

		// client
		$sql = "CREATE TABLE IF NOT EXISTS client (
				user_id UUID NOT NULL PRIMARY KEY 
				  REFERENCES user_account(id) ON DELETE CASCADE,
				settings json NOT NULL,
				balance DECIMAL(12,2) NOT NULL DEFAULT 0.00,
				reserved DECIMAL(12,2) NOT NULL DEFAULT 0.00,
				rating FLOAT(2) NOT NULL DEFAULT 0.00,
				level SMALLINT NOT NULL DEFAULT 0
					)";
		$this->addSql($sql);

		// file
		// user_id может быть NULL, чтобы избежать циклических зависимостей
		// и чтобы можно было очищать записи вместе с удалением файлов
		// где-нибудь по расписанию.
		$sql = "CREATE TABLE IF NOT EXISTS file (
				id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
				created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
				updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
				user_id UUID NULL REFERENCES user_account(id) ON DELETE SET NULL,
				name VARCHAR (64) NOT NULL,
				relative_directory VARCHAR(1024) NOT NULL,
				uri VARCHAR (255) NOT NULL
			)";
		$this->addSql($sql);

		$sql = "CREATE TABLE IF NOT EXISTS registration (
				email VARCHAR (64) UNIQUE NOT NULL,
				sent_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
				PRIMARY KEY (email, sent_at)
			)";
		$this->addSql($sql);

		// category of job
		$sql = "CREATE TABLE IF NOT EXISTS category (
				id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
				title VARCHAR (64) NOT NULL,
				description TEXT,
				parent_id UUID REFERENCES category(id)
		)";
		$this->addSql($sql);

		// job
		$sql = "CREATE TABLE IF NOT EXISTS job (
				id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
				created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
				client_id UUID NULL REFERENCES client(user_id) ON DELETE SET NULL,
				category_id UUID NULL REFERENCES category(id) ON DELETE SET NULL,
				title VARCHAR (255) NOT NULL,
				description TEXT,
				image_file_id UUID NULL REFERENCES file(id) ON DELETE CASCADE,
				state SMALLINT NOT NULL DEFAULT 0,
				cost DECIMAL(12,2) NOT NULL DEFAULT 0.00,
				hours INTEGER NOT NULL NOT NULL
		)";
		$this->addSql($sql);

		// job_file
		$sql = "CREATE TABLE IF NOT EXISTS job_file (
				job_id UUID NULL REFERENCES job(id) ON DELETE SET NULL,
				file_id UUID NULL REFERENCES file(id) ON DELETE CASCADE,
				PRIMARY KEY (job_id, file_id)
		)";
		$this->addSql($sql);
	}

	private function fill(Schema $schema) {
		$categories = [
			'Текст' => [
				'рерайт',
				'копирайт',
				'оригинальные статьи',
				'переводы',
			],
			'Графика' => [],
			'Дизайн' => [],
			'Разработка' => [
				'верстка',
				'бэкенд',
				'фронтэнд',
				'сайт',
				'мобильные приложения',
			],
			'Поисковое продвижение' => [],
			'Обучение' => [],
		];

		foreach($categories as $title => $children) {
			$id = Uuid::uuid4();
			$sql = "INSERT INTO category (id, title, parent_id) VALUES ('$id', '$title', NULL)";
			$this->addSql($sql);
			if (!empty($children)) {
				foreach($children as $child_title) {
					$child_id = Uuid::uuid4();
					$sql = "INSERT INTO category (id, title, parent_id) VALUES ('$child_id', '$child_title', '$id')";
					$this->addSql($sql);
				}
			}
		}
	}

	public function down(Schema $schema): void {
		$tables = [
			'registration',
			'job_file',
			'job',
			'file',
			'category',
			'client',
			'user_account',
		];

		$this->addSql('ALTER TABLE user_account DROP CONSTRAINT avatar_fk');

		foreach($tables as $table) {
			$this->addSql("DROP TABLE IF EXISTS $table");
		}
	}
}
