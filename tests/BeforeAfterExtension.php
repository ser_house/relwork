<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2019
 * Time: 21:16
 */

namespace App\Tests;

use PHPUnit\Runner\AfterLastTestHook;
use PHPUnit\Runner\AfterTestHook;
use PHPUnit\Runner\BeforeFirstTestHook;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\StringInput;

final class BeforeAfterExtension extends KernelTestCase implements BeforeFirstTestHook, AfterLastTestHook, AfterTestHook {

  /**
   * Создаём тестовую базу и делаем миграции до всех тестов.
   *
   * @throws \Exception
   */
  public function executeBeforeFirstTest(): void {
    $this->runCommand("doctrine:database:create --if-not-exists --quiet");
    $this->runCommand("--no-interaction doctrine:migrations:migrate --quiet");
  }

  /**
   * После всех тестов удаляем тестовую базу.
   *
   * @throws \Exception
   */
  public function executeAfterLastTest(): void {
    $this->runCommand("doctrine:database:drop --if-exists --force --quiet");
  }

  /**
   * После каждого теста чистим затронутые таблицы (чтобы следующий тест работал на чистую).
   *
   * @param string $test
   * @param float $time
   *
   * @throws \Exception
   */
  public function executeAfterTest(string $test, float $time): void {
    // $test выглядит так: App\Tests\functional\Api\Management\SocialTest::testAdd
    $this->runCommand("dbal:run-sql 'DELETE FROM client'");
    $this->runCommand("dbal:run-sql 'DELETE FROM job'");
    $this->runCommand("dbal:run-sql 'DELETE FROM user_account'");
    $this->runCommand("dbal:run-sql 'DELETE FROM registration'");
  }

  /**
   * @param string $command
   *
   * @return int
   * @throws \Exception
   */
  private function runCommand(string $command) {
    $kernel = self::bootKernel();
    $application = new Application($kernel);
    $application->setAutoExit(false);

    $result = $application->run(new StringInput($command));
    // Сбрасываем bootKernel, чтобы тесты могли работать с ним самостоятельно
    // (например, WebTestCase::createClient() отказывается делать своё дело,
    // если ядро уже загружено).
    $this->tearDown();
    return $result;
  }
}
