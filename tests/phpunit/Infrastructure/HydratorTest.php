<?php

namespace App\Tests\phpunit\Infrastructure;

use Core\Client\Buyer\Buyer;
use Core\Client\Buyer\Hydrator as BuyerHydrator;
use Core\Client\Seller\Hydrator as SellerHydrator;
use Core\Client\Seller\Level;
use Core\Client\Seller\Seller;
use Core\Client\Settings;
use Core\File\File;
use Core\File\FileId;
use Core\Generic\Infrastructure\Hydrator;
use Core\Job\Category\Category;
use Core\Job\Category\CategoryId;
use Core\Job\Job;
use Core\Job\JobId;
use Core\Job\State;
use Core\User\Hydrator as UserHydrator;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class HydratorTest extends TestCase {

  public function testUser(): void {
    $userId = new UserId();
    $created_at = (new DateTimeImmutable())->format('Y-m-d H:i');
    $name = 'name';
    $email = 'email@example.com';
    $password = 'password';
    $tz = 'Europe/Volgograd';

    $data = [
      'id' => (string)$userId,
      'created_at' => $created_at,
      'name' => $name,
      'email' => $email,
      'password' => $password,
      'timezone' => $tz,
      'avatar_name' => 'avatar_name',
      'avatar_rel_dir' => 'avatar_rel_dir',
      'avatar_uri' => 'avatar_uri',
      'role' => Role::SELLER->value,
    ];

    $hydrator = new UserHydrator();
    /** @var User $user */
    $user = $hydrator->hydrate($data);

    self::assertEquals($userId, $user->id);
    self::assertEquals($created_at, $user->createdAt->format('Y-m-d H:i'));
    self::assertEquals($name, $user->name);
    self::assertEquals($password, $user->password);
    self::assertEquals($tz, $user->timeZone->getName());
    self::assertEquals(Role::SELLER, $user->role);
  }

  public function testSeller(): void {
    $userId = new UserId();
    $created_at = (new DateTimeImmutable())->format('Y-m-d H:i');
    $name = 'name';
    $email = 'email@example.com';
    $password = 'password';
    $tz = 'Europe/Volgograd';
    $settings = new Settings();
    $balance = 0.0;
    $rating = 0.0;
    $level = Level::NEWBIE;
    $data = [
      'id' => (string)$userId,
      'created_at' => $created_at,
      'name' => $name,
      'email' => $email,
      'password' => $password,
      'timezone' => $tz,
      'avatar_name' => 'avatar_name',
      'avatar_rel_dir' => 'avatar_rel_dir',
      'avatar_uri' => 'avatar_uri',
      'role' => Role::SELLER->value,
      'settings' => json_encode($settings, JSON_THROW_ON_ERROR),
      'balance' => $balance,
      'rating' => $rating,
      'level' => $level->value,
    ];

    $hydrator = new SellerHydrator(new UserHydrator());
    /** @var Seller $seller */
    $seller = $hydrator->hydrate($data);

    self::assertEquals($userId, $seller->getId());
    self::assertEquals($settings, $seller->settings);
    self::assertEquals($balance, $seller->getBalance()->getAmount());
    self::assertEquals($rating, $seller->getRating()->value());
    self::assertEquals(Level::NEWBIE, $seller->getLevel());
  }

  public function testBuyer(): void {
    $userId = new UserId();
    $created_at = (new DateTimeImmutable())->format('Y-m-d H:i');
    $name = 'name';
    $email = 'email@example.com';
    $password = 'password';
    $tz = 'Europe/Volgograd';
    $settings = new Settings();
    $balance = 0.0;
    $reserved = 0.0;

    $data = [
      'id' => (string)$userId,
      'created_at' => $created_at,
      'name' => $name,
      'email' => $email,
      'password' => $password,
      'timezone' => $tz,
      'avatar_name' => 'avatar_name',
      'avatar_rel_dir' => 'avatar_rel_dir',
      'avatar_uri' => 'avatar_uri',
      'role' => Role::BUYER->value,
      'settings' => json_encode($settings, JSON_THROW_ON_ERROR),
      'balance' => $balance,
      'reserved' => $reserved,
    ];

    $hydrator = new BuyerHydrator(new UserHydrator());
    /** @var Buyer $buyer */
    $buyer = $hydrator->hydrate($data, Buyer::class);

    self::assertEquals($userId, $buyer->getId());
    self::assertEquals($settings, $buyer->settings);
    self::assertEquals($balance, $buyer->balance->getAmount());
    self::assertEquals($reserved, $buyer->getReserved()->getAmount());
  }

  public function testJob(): void {
    $id = new JobId();
    $created_at = (new DateTimeImmutable())->format('Y-m-d H:i');
    $clientId = new UserId();
    $categoryId = new CategoryId();
    $title = 'title';
    $description = 'description';
    $imageFileId = new FileId();
    $state = State::NEW->value;
    $cost = 100.0;
    $hours = 5;
    $data = [
      'id' => (string)$id,
      'created_at' => $created_at,
      'client_id' => (string)$clientId,
      'category_id' => (string)$categoryId,
      'title' => $title,
      'description' => $description,
      'image_file_id' => (string)$imageFileId,
      'state' => $state,
      'cost' => $cost,
      'hours' => $hours,
    ];

    $hydrator = new Hydrator();
    /** @var Job $job */
    $job = $hydrator->hydrate($data, Job::class);

    self::assertEquals($id, $job->id);
    self::assertEquals($created_at, $job->createdAt->format('Y-m-d H:i'));
    self::assertEquals($clientId, $job->clientId);
    self::assertEquals($categoryId, $job->categoryId);
    self::assertEquals($title, $job->title);
    self::assertEquals($description, $job->description);
    self::assertEquals($imageFileId, $job->imageFileId);
    self::assertEquals($state, $job->state->value);
    self::assertEquals($cost, $job->cost->getAmount());
    self::assertEquals($hours, $job->hours->value);
  }

  public function testCategory(): void {
    $id = new CategoryId();
    $title = 'title';
    $description = 'description';
    $parentId = null;

    $data = [
      'id' => (string)$id,
      'title' => $title,
      'description' => $description,
      'parent_id' => $parentId,
    ];

    $hydrator = new Hydrator();
    /** @var Category $category */
    $category = $hydrator->hydrate($data, Category::class);

    self::assertEquals($id, $category->id);
    self::assertEquals($title, $category->title);
    self::assertEquals($description, $category->description);
    self::assertEquals($parentId, $category->parentId);

    $parentId = new CategoryId();
    $data['parent_id'] = $parentId;
    $category = $hydrator->hydrate($data, Category::class);
    self::assertEquals($parentId, $category->parentId);
  }

  public function testFile(): void {
    $id = new FileId();
    $created_at = (new DateTimeImmutable())->format('Y-m-d H:i');
    $userId = new UserId();
    $name = 'name';
    $relative_directory = 'relative_directory';
    $uri = 'uri';

    $data = [
      'id' => (string)$id,
      'created_at' => $created_at,
      'user_id' => (string)$userId,
      'name' => $name,
      'relative_directory' => $relative_directory,
      'uri' => $uri,
    ];

    $hydrator = new Hydrator();
    $file = $hydrator->hydrate($data, File::class);

    self::assertEquals($id, $file->id);
    self::assertEquals($created_at, $file->createdAt->format('Y-m-d H:i'));
    self::assertEquals($userId, $file->userId);
    self::assertEquals($name, $file->name);
    self::assertEquals($relative_directory, $file->relative_directory);
    self::assertEquals($uri, $file->uri);
  }
}
