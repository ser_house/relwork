<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2019
 * Time: 6:39
 */

namespace App\Tests\phpunit\Infrastructure;

use Core\Generic\Infrastructure\EncodeStringService;
use PHPUnit\Framework\TestCase;

/**
 * Class EncodeStringServiceTest
 */
class EncodeStringServiceTest extends TestCase {

	/**
	 *
	 */
	public function testService() {
		$email = 'example@example.com';
		$service = new EncodeStringService();

		$encoded = $service->encode($email);
		$decoded = $service->decode($encoded);

		$this->assertEquals($email, $decoded);
	}
}
