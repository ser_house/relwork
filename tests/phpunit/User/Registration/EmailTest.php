<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.01.2019
 * Time: 0:35
 */

namespace App\Tests\phpunit\User\Registration;

use Core\System\UseCases\Registration\Registration;
use Core\User\Email\Email;
use DateInterval;
use DateTimeImmutable;
use Exception;
use PHPUnit\Framework\TestCase;

// bin/phpunit tests/phpunit/Registration/EmailTest.php

/**
 * Class EmailTest
 */
class EmailTest extends TestCase {

  private Email $email;
  private DateTimeImmutable $datetime;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();
    $this->email = new Email('example@example.com');
    $this->datetime = new DateTimeImmutable();
  }

  /**
   * @throws Exception
   */
  public function testIsCanBeSent() {
    $pause_after_sent = Registration::PAUSE_AFTER_SENT;

    $few = $pause_after_sent - 1;
    $interval = new DateInterval("PT{$few}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertFalse($registration->isPauseTimeExpired());

    $few = $pause_after_sent;
    $interval = new DateInterval("PT{$few}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertTrue($registration->isPauseTimeExpired());

    $enough = $pause_after_sent + 1;
    $interval = new DateInterval("PT{$enough}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertTrue($registration->isPauseTimeExpired());
  }

  public function testLinkLifetime() {
    $link_lifetime = Registration::LINK_LIFETIME;

    $correct = $link_lifetime - 1;
    $interval = new DateInterval("PT{$correct}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertFalse($registration->isExpired());

    $correct = $link_lifetime;
    $interval = new DateInterval("PT{$correct}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertFalse($registration->isExpired());

    $expired = $link_lifetime + 1;
    $interval = new DateInterval("PT{$expired}M");
    $intervalTime = $this->datetime->sub($interval);

    $registration = new Registration($this->email, $intervalTime);
    $this->assertTrue($registration->isExpired());
  }
}
