<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2019
 * Time: 21:02
 */

namespace App\Tests\phpunit\User\Registration;

use Core\System\UseCases\Registration\IUrlGenerator;
use Core\System\UseCases\Registration\Registration;
use Core\User\Email\Email;
use Core\User\Email\IEmailSender;
use Core\User\IUserRepository;
use DateInterval;
use DateTimeImmutable;
use Framework\User\Registration\RegistrationRepository;
use Framework\Web\User\Registration\SendEmail\Action\EmailExistsAction;
use Framework\Web\User\Registration\SendEmail\Action\SendAction;
use Framework\Web\User\Registration\SendEmail\Action\TimeNotPassedAction;
use Framework\Web\User\Registration\SendEmail\RegistrationActionFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class ActionTest extends KernelTestCase {

  private Email $email;
  private RegistrationActionFactory $actionFactory;
  private RegistrationRepository $registrationRepository;
  private IUserRepository $userRepository;
  private RequestStack $requestStack;
  private IUrlGenerator $urlGenerator;
  private IEmailSender $emailSender;

  protected function setUp(): void {
    $this->email = new Email('example@example.com');
    $this->registrationRepository = $this->getMockBuilder(RegistrationRepository::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->userRepository = $this->getMockBuilder(IUserRepository::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->urlGenerator = $this->getMockBuilder(IUrlGenerator::class)->disableOriginalConstructor()->getMock();
    $this->emailSender = $this->getMockBuilder(IEmailSender::class)->disableOriginalConstructor()->getMock();

    $this->requestStack = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
    $this->requestStack->method('getSession')->willReturn(new Session(new MockArraySessionStorage()));
  }

  public function testTimeNotPassed() {
    $this->userRepository->method('existsUserWithEmail')->willReturn(false);

    $this->actionFactory = new RegistrationActionFactory(
      $this->userRepository,
      $this->registrationRepository,
      $this->requestStack,
      $this->urlGenerator,
      $this->emailSender
    );

    $sentTime = $this->buildNotPassedSentTime();

    $registration = new Registration($this->email, $sentTime);

    $this->registrationRepository->method('findByEmail')->willReturn($registration);

    $action = $this->actionFactory->getAction($registration);

    $this->assertInstanceOf(TimeNotPassedAction::class, $action);
  }

  public function testEmailExists() {
    $this->registrationRepository->method('findByEmail')->willReturn(null);
    $this->userRepository->method('existsUserWithEmail')->willReturn(true);

    $this->actionFactory = new RegistrationActionFactory(
      $this->userRepository,
      $this->registrationRepository,
      $this->requestStack,
      $this->urlGenerator,
      $this->emailSender
    );

    $sentTime = $this->buildPassedSentTime();

    $registration = new Registration($this->email, $sentTime);

    $action = $this->actionFactory->getAction($registration);

    $this->assertInstanceOf(EmailExistsAction::class, $action);
  }

  public function testSend() {
    $this->registrationRepository->method('findByEmail')->willReturn(null);
    $this->userRepository->method('existsUserWithEmail')->willReturn(false);

    $this->actionFactory = new RegistrationActionFactory(
      $this->userRepository,
      $this->registrationRepository,
      $this->requestStack,
      $this->urlGenerator,
      $this->emailSender
    );

    $sentTime = $this->buildPassedSentTime();

    $registration = new Registration($this->email, $sentTime);

    $action = $this->actionFactory->getAction($registration);

    $this->assertInstanceOf(SendAction::class, $action);
  }

  private function buildNotPassedSentTime(): DateTimeImmutable {
    return (new DateTimeImmutable())->sub(new DateInterval("PT2M"));
  }

  private function buildPassedSentTime(): DateTimeImmutable {
    return (new DateTimeImmutable())->sub(new DateInterval("PT6M"));
  }
}
