<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.02.2019
 * Time: 21:49
 */

namespace App\Tests\phpunit\User\Client;

use Core\Client\Seller\Level;
use PHPUnit\Framework\TestCase;

/**
 * Class LevelTest
 */
class LevelTest extends TestCase {

	/**
	 *
	 */
	public function testNextLevel() {

		// First level and next.
		$newbieLevel = Level::NEWBIE;
		$nexLevel = $newbieLevel->nextLevel();
		$this->assertEquals(Level::VETERAN, $nexLevel);

		// Last level and next.
		$expertLevel = Level::EXPERT;
		$nexLevel = $expertLevel->nextLevel();
		$this->assertNull($nexLevel);
	}

	/**
	 *
	 */
	public function testPrevLevel() {
		// Last level and prev.
		$expertLevel = Level::EXPERT;
		$masterLevel = Level::MASTER;

		$prevLevel = $expertLevel->prevLevel();
		$this->assertEquals($masterLevel, $prevLevel);

		// First level and prev.
		$newbieLevel = Level::NEWBIE;
		$prevLevel = $newbieLevel->prevLevel();
		$this->assertNull($prevLevel);
	}
}
