<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2019
 * Time: 4:08
 */

namespace App\Tests\phpunit\User\Client;

use App\Tests\fixtures\Initiator;
use App\Tests\functional\WebCase;
use Core\Client\Exception\ImpossibleToDownLevel;
use Core\Client\Exception\ImpossibleToUpLevel;
use Core\Client\Seller\Level;
use Core\Client\Seller\Rating;
use Core\Client\Seller\Seller;
use Core\Client\Settings;
use Core\File\IFileService;
use Core\Money\Money;
use Core\User\Role;


/**
 * Class SellerTest
 */
class SellerTest extends WebCase {
  use Initiator;

  private Seller $seller;
  private IFileService $fileService;

  /**
   * @throws \Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->initServices(self::getContainer());

    $user = $this->initUser(Role::SELLER);

    $settings = new Settings();
    $balance = new Money(1000);
    $rating = new Rating(1.0);
    $this->seller = new Seller($user, $settings, $balance, $rating, Level::NEWBIE);
  }

  /**
   *
   */
  public function testAddMoney() {
    $toAdd = new Money(300);
    $this->seller->addMoney($toAdd);

    $expected = new Money(1000 + 300);

    $this->assertEquals($expected, $this->seller->getBalance());
  }

  /**
   *
   */
  public function testGetAvailableMoney() {
    $available = $this->seller->getAvailableMoney();

    $this->assertEquals(new Money(1000), $available);
  }

  /**
   * @throws ImpossibleToDownLevel
   * @throws ImpossibleToUpLevel
   */
  public function testNewLevel() {
    // Следующий после самого младшего уровень.
    $newLevel = Level::VETERAN;
    $this->seller->levelUp();
    $this->assertEquals($newLevel, $this->seller->getLevel());

    // Поднимем до наивысшего уровня.
    $expertLevel = Level::EXPERT;
    while ($expertLevel != $this->seller->levelUp()) {
      ;
    }
    $this->assertEquals($expertLevel, $this->seller->getLevel());

    // Уровень перед наивысшим.
    $newLevel = Level::MASTER;
    $this->seller->levelDown();
    $this->assertEquals($newLevel, $this->seller->getLevel());

    // Дальше проверим выходы за границы допустимых уровней.

    // Опять поднимем до наивысшего
    $this->seller->levelUp();

    // и проверим попытку поднять уровень еще выше.
    $this->expectException(ImpossibleToUpLevel::class);
    $this->seller->levelUp();

    // Опустимся до самого младшего
    $newbieLevel = Level::NEWBIE;
    while ($newbieLevel != $this->seller->levelDown()) {
      ;
    }
    $this->assertEquals($newbieLevel, $this->seller->getLevel());

    // и проверим попытку опустить уровень еще ниже.
    $this->expectException(ImpossibleToDownLevel::class);
    $this->seller->levelDown();
  }
}
