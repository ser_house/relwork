<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 17.02.2019
 * Time: 12:12
 */

namespace App\Tests\phpunit\User\Client;

use App\Tests\fixtures\Initiator;
use App\Tests\functional\WebCase;
use Core\Client\Buyer\Buyer;
use Core\Client\Exception\NotEnoughAmount;
use Core\Client\Settings;
use Core\Money\Money;
use Core\User\Role;
use Exception;

class BuyerTest extends WebCase {
  use Initiator;

  private Buyer $buyer;

  /**
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->initServices(self::getContainer());

    $user = $this->initUser(Role::BUYER);

    $settings = new Settings();
    $balance = new Money(1000);
    $reserved = new Money(500);
    $this->buyer = new Buyer($user, $settings, $balance, $reserved);
  }

  /**
   * @throws NotEnoughAmount
   */
  public function testReserveMoney() {
    $this->expectException(NotEnoughAmount::class);

    $toReserve = new Money(1000);
    $this->buyer->reserveMoney($toReserve);
  }
}
