<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 15.02.2019
 * Time: 18:15
 */

namespace App\Tests\phpunit\Job\Category;

use Core\Job\Category\Category;
use Core\Title;
use Exception;
use Framework\Job\Category\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


/**
 * Class CategoryRepositoryTest
 */
class CategoryRepositoryTest extends KernelTestCase {
  private CategoryRepository $categoryRepository;

  /**
   *
   */
  protected function setUp(): void {
    parent::setUp();

    $container = static::getContainer();

    $this->categoryRepository = $container->get('category.repository.db');
  }

  /**
   *
   */
  public function testFindAll() {
    $categories = $this->categoryRepository->findAll();
    $this->assertTrue(!empty($categories));
  }

  /**
   * @throws Exception
   */
  public function testAdd() {
    $title = new Title('New root category');

    $category = new Category($title);

    $this->categoryRepository->add($category);

    $dbCategory = $this->categoryRepository->findByCategoryId($category->id);

    $this->assertEquals($category, $dbCategory);
  }
}
