<?php

namespace App\Tests\fixtures;

use Core\Client\Buyer\Buyer;
use Core\Client\Buyer\IBuyerRepository;
use Core\Client\Seller\ISellerRepository;
use Core\Client\Seller\Level;
use Core\Client\Seller\Seller;
use Core\Client\Settings;
use Core\File\IFileService;
use Core\User\Hydrator;
use Core\User\IUserRepository;
use Core\User\Password\HashedPassword;
use Core\User\Role;
use Core\User\User;
use Core\User\UserId;
use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

trait Initiator {
  private IUserRepository $userRepository;
  private ISellerRepository $sellerRepository;
  private IBuyerRepository $buyerRepository;
  private IFileService $fileService;
  private Connection $connection;
  private Hydrator $userHydrator;


  private function initServices(ContainerInterface $container): void {
    $this->connection = $container->get('test_alias.db_connection');
    $this->userRepository = $container->get('test_alias.user.repository.db');
    $this->sellerRepository = $container->get('test_alias.seller.repository.db');
    $this->buyerRepository = $container->get('test_alias.buyer.repository.db');
    $this->fileService = $container->get('test_alias.file_service');
    $this->userHydrator = new Hydrator();
  }

  private function getFixtureAvatarUploadedFile(): SymfonyUploadedFile {
    $avatar_file_name = 'avatar';
    $file_fixtures_dir = dirname(__FILE__, 2) . "/fixtures";
    $orig_path = "$file_fixtures_dir/$avatar_file_name.jpg";
    $new_path = "$file_fixtures_dir/{$avatar_file_name}1.jpg";
    copy($orig_path, $new_path);
    return new SymfonyUploadedFile($new_path, 'avatar.jpg', null, null, true);
  }

  private function initUser(Role $role): User {
    $password = new HashedPassword('24sfsdfe');
    $createdAt = new DateTimeImmutable();
    $id = new UserId();
    $data = [
      'id' => (string)$id,
      'name' => 'ClientNickName',
      'created_at' => $createdAt->format('Y-m-d H:i'),
      'email' => 'example@example.com',
      'password' => (string)$password,
      'timezone' => 'Europe/Volgograd',
      'avatar_name' => 'avatar_name',
      'avatar_rel_dir' => 'avatar_rel_dir',
      'avatar_uri' => 'avatar_uri',
      'role' => $role->value,
    ];
    $this->connection->insert('user_account', $data);
    return $this->userRepository->getByUserId($id);
  }

  private function initSeller(): Seller {
    $user = $this->initUser(Role::SELLER);
    $data = [
      'user_id' => (string)$user->id,
      'settings' => json_encode(new Settings(), JSON_THROW_ON_ERROR),
      'balance' => 0.0,
      'rating' => 0.0,
      'level' => Level::NEWBIE->value,
    ];
    $this->connection->insert('client', $data);

    return $this->sellerRepository->getByUserId($user->id);
  }

  private function initBuyer(): Buyer {
    $user = $this->initUser(Role::BUYER);
    $data = [
      'user_id' => (string)$user->id,
      'settings' => json_encode(new Settings(), JSON_THROW_ON_ERROR),
      'balance' => 0.0,
      'reserved' => 0.0,
    ];
    $this->connection->insert('client', $data);
    return $this->buyerRepository->getByUserId($user->id);
  }
}
