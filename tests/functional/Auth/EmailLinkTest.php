<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2019
 * Time: 7:02
 */

namespace App\Tests\functional\Auth;

use App\Tests\functional\WebCase;
use Core\Generic\Infrastructure\EncodeStringService;
use Core\System\UseCases\Registration\IRegistrationRepository;
use Core\System\UseCases\Registration\Registration;
use Core\User\Email\Email;
use Exception;

/**
 * Class EmailLinkTest
 */
class EmailLinkTest extends WebCase {

  private Email $email;
  private EncodeStringService $encodeStringService;
  private IRegistrationRepository $registrationRepository;

  /**
   *
   */
  protected function setUp(): void {
    parent::setUp();

    $this->email = new Email('example@example.com');
    $this->encodeStringService = new EncodeStringService();

    $this->registrationRepository = $this->container->get('test_alias.registration.repository.db');
  }

  /**
   * @throws Exception
   */
  public function testSuccess() {
    // Полезно с живой (но тестовой) базой поиграться.
    $registration = new Registration($this->email);
    $this->registrationRepository->save($registration);

    // Сформировать ссылку
    $url = $this->buildCorrectUrl();

    // Сделать по ней переход
    $this->client->request('GET', $url);
    $response = $this->client->getResponse();

    // Чтобы получить 200, надо, чтобы почта была сохранена в базе.
    $this->assertEquals(200, $response->getStatusCode());

    $this->registrationRepository->removeByEmail($this->email);
  }

  /**
   *
   */
  public function testUnknownEmail() {
    // Сформировать ссылку
    $url = $this->buildCorrectUrl();

    // Сделать по ней переход
    $this->client->request('GET', $url);
    $response = $this->client->getResponse();
//		$this->writeResultToFile('testUnknownEmail');
    // И, поскольку записи об отправке письма на эту почту нет,
    // мы должны получить Not found.
    $this->assertEquals(404, $response->getStatusCode());
  }

  /**
   *
   */
  public function testIncorrectUrlArgument() {
    $url = $this->buildIncorrectUrl();

    $this->client->request('GET', $url);
    $response = $this->client->getResponse();

    $this->assertEquals(500, $response->getStatusCode());
  }

  /**
   *
   * @return string
   */
  private function buildCorrectUrl(): string {
    return $this->router->generate(
      'registration_create_profile',
      ['arg' => $this->encodeStringService->encode($this->email)]
    );
  }

  /**
   *
   * @return string
   */
  private function buildIncorrectUrl(): string {
    return $this->router->generate(
      'registration_create_profile',
      ['arg' => 'fake_arg']
    );
  }
}
