<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.02.2019
 * Time: 17:52
 */

namespace App\Tests\functional\Auth;

use App\Tests\fixtures\Initiator;
use App\Tests\functional\WebCase;
use Core\Generic\Infrastructure\EncodeStringService;
use Core\Money\Money;
use Core\User\Email\Email;
use Core\User\Role;
use Framework\User\EmailSender;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;


class ClientTest extends WebCase {
  use Initiator;

  private EncodeStringService $encodeStringService;


  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->initServices(self::getContainer());
    $this->encodeStringService = new EncodeStringService();
  }


  public function testSellerRegistration() {
    $this->mockServices();

    $this->client->request('GET', $this->router->generate('registration'));
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $email = 'seller@example.com';

    $this->client->submitForm('Send', [
      'form[email]' => $email,
    ]);
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $arg = $this->encodeStringService->encode($email);
    $profile_url = $this->router->generate('registration_create_profile', ['arg' => $arg]);

    $this->client->request('GET', $profile_url);
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $crawler = $this->client->getCrawler();
    $csrf_token = $crawler->filter("input[name='token']")->first()->attr('value');

    $name = 'nickname';
    $timezone = 'Europe/Volgograd';

    $this->client->request(
      'POST',
      $profile_url,
      [
        'token' => $csrf_token,
        'nickname' => $name,
        'email' => $email,
        'timezone' => $timezone,
        'role' => Role::SELLER->value,
        'x' => 0,
        'y' => 0,
        'size' => 500,
      ],
      [
        'avatar' => $this->getFixtureAvatarUploadedFile(),
      ]
    );
    $this->writeResultToFile();
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);

    $user = $this->userRepository->findByEmail(new Email($email));
    self::assertNotNull($user);
    self::assertEquals(Role::SELLER, $user->role);

    $seller = $this->sellerRepository->findByUserId($user->id);
    self::assertNotNull($seller);
  }

  public function testBuyerRegistration() {
    $this->mockServices();

    $this->client->request('GET', $this->router->generate('registration'));
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $email = 'buyer@example.com';

    $this->client->submitForm('Send', [
      'form[email]' => $email,
    ]);
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $arg = $this->encodeStringService->encode($email);
    $profile_url = $this->router->generate('registration_create_profile', ['arg' => $arg]);

    $this->client->request('GET', $profile_url);
    self::assertResponseStatusCodeSame(Response::HTTP_OK);

    $crawler = $this->client->getCrawler();
    $csrf_token = $crawler->filter("input[name='token']")->first()->attr('value');

    $name = 'buyer_nick';
    $timezone = 'Europe/Volgograd';

    $this->client->request(
      'POST',
      $profile_url,
      [
        'token' => $csrf_token,
        'nickname' => $name,
        'email' => $email,
        'timezone' => $timezone,
        'role' => Role::BUYER->value,
        'x' => 0,
        'y' => 0,
        'size' => 500,
      ],
      [
        'avatar' => $this->getFixtureAvatarUploadedFile(),
      ]
    );
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);

    $user = $this->userRepository->findByEmail(new Email($email));
    self::assertNotNull($user);
    self::assertEquals(Role::BUYER, $user->role);

    $buyer = $this->buyerRepository->findByUserId($user->id);
    self::assertNotNull($buyer);

    $this->assertEquals(new Money(), $buyer->getAvailableMoney());
    $this->assertEquals(new Money(), $buyer->getReserved());
  }

  private function mockServices(): void {
    $this->client->disableReboot();
    $emailSender = $this->getMockBuilder(EmailSender::class)->disableOriginalConstructor()->getMock();
    $emailSender->expects($this->once())->method('sendRegistrationEmail');

    $this->container->set('test_alias.email_sender', $emailSender);

    $requestStack = $this->getMockBuilder(RequestStack::class)->disableOriginalConstructor()->getMock();
    $requestStack->method('getSession')->willReturn(new Session(new MockArraySessionStorage()));
  }
}
