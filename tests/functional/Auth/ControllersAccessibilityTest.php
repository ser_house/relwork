<?php

namespace App\Tests\functional\Auth;

use App\Tests\functional\WebCase;
use Symfony\Component\HttpFoundation\Response;

class ControllersAccessibilityTest extends WebCase {

  public function testStartRegistration(): void {
    $this->client->request('GET', $this->router->generate('registration'));
    self::assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testLogin(): void {
    $this->client->request('GET', $this->router->generate('login'));
    self::assertResponseStatusCodeSame(Response::HTTP_OK);
  }

  public function testLogout(): void {
    $this->client->request('GET', $this->router->generate('logout'));
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);
  }
}
