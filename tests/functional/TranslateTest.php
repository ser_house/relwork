<?php

namespace App\Tests\functional;

use Core\Generic\Errors\Exceptions\FieldsExceptionsCollection;
use Core\Generic\Errors\Exceptions\IsLessThanException;
use Framework\Generic\Errors\Exceptions\FieldErrorsFromExceptionsBuilder;
use Framework\Generic\Errors\HttpFieldLabels;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TranslateTest extends KernelTestCase {
  private HttpFieldLabels $fieldErrorLabels;
  private FieldErrorsFromExceptionsBuilder $errorsBuilder;

  /**
   *
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fieldErrorLabels = static::getContainer()->get('test_alias.http_field_labels');
    $this->errorsBuilder = static::getContainer()->get('test_alias.field_errors_builder');
  }

  /**
   * @dataProvider labels
   */
  public function testLabels($field, $label) {
    $result = $this->fieldErrorLabels->getFromOneField($field, 'profile_labels');
    self::assertEquals($label, $result);
  }

  private function labels(): array {
    return [
      ['nickname', 'Ник'],
      ['password', 'Пароль'],
      ['timezone', 'Часовой пояс'],
      ['role', 'Роль'],
      ['avatar', 'Аватарка'],
    ];
  }

  public function testErrors() {
    $fieldExceptions = new FieldsExceptionsCollection('profile');

    $fieldExceptions->addException(new IsLessThanException(-0.5, 0.0), 'x');
    $fieldExceptions->addException(new IsLessThanException(-0.5, 0.0), 'y');
    $fieldExceptions->addException(new IsLessThanException(99, 100.0), 'size');

    $errors = $this->errorsBuilder->build($this->fieldErrorLabels, $fieldExceptions);
    self::assertTrue($errors->hasErrors());
    $errors_arr = $errors->getErrors();
    self::assertCount(3, $errors_arr);

    self::assertArrayHasKey('x', $errors_arr);
    self::assertArrayHasKey('y', $errors_arr);
    self::assertArrayHasKey('size', $errors_arr);

    self::assertCount(1, $errors_arr['x']);
    self::assertCount(1, $errors_arr['y']);
    self::assertCount(1, $errors_arr['size']);
  }
}
