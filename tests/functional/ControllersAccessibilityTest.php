<?php

namespace App\Tests\functional;

use Symfony\Component\HttpFoundation\Response;

class ControllersAccessibilityTest extends WebCase {

  public function testIndex(): void {
    $this->client->request('GET', $this->router->generate('index'));
    self::assertResponseStatusCodeSame(Response::HTTP_OK);
  }
}
