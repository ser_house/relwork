<?php

namespace App\Tests\functional\Client;

use App\Tests\functional\WebCase;
use Symfony\Component\HttpFoundation\Response;

class ControllersAccessibilityTest extends WebCase {

  public function testAddJob(): void {
    $this->client->request('GET', $this->router->generate('seller_add_job'));
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);
  }

  public function testSellerMyJobs(): void {
    $this->client->request('GET', $this->router->generate('seller_my_jobs'));
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);
  }

}
