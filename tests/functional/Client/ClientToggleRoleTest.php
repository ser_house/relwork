<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.02.2019
 * Time: 8:46
 */

namespace App\Tests\functional\Client;

use App\Tests\fixtures\Initiator;
use App\Tests\functional\WebCase;
use Core\User\Role;
use Symfony\Component\HttpFoundation\Response;

class ClientToggleRoleTest extends WebCase {
  use Initiator;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->client->disableReboot();
    $this->initServices(self::getContainer());
  }

  public function testToggleSellerToBuyer() {
    $seller = $this->initSeller();

    $this->setSessionData(['userId' => $seller->getId()]);
    $this->client->request('GET', $this->router->generate('toggle_role'));
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);

    $updatedUser = $this->userRepository->getByUserId($seller->getId());
    $this->assertEquals(Role::BUYER, $updatedUser->role);
  }

  public function testToggleBuyerToSeller() {
    $buyer = $this->initBuyer();

    $this->setSessionData(['userId' => $buyer->getId()]);

    $this->client->request('GET', $this->router->generate('toggle_role'));
    self::assertResponseStatusCodeSame(Response::HTTP_FOUND);

    $updatedUser = $this->userRepository->getByUserId($buyer->getId());
    $this->assertEquals(Role::SELLER, $updatedUser->role);
  }
}
