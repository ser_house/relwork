<?php

namespace App\Tests\functional;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class WebCase extends WebTestCase {
  protected ContainerInterface $container;
  protected RouterInterface $router;
  protected KernelBrowser $client;

  protected function setUp(): void {
    $this->client = static::createClient();
    $this->container = static::getContainer();
    $this->router = $this->container->get('router');
  }

  /**
   * Если вдруг что не так с проверкой страницы, то удобно посмотреть будет.
   *
   * @param string $filename
   */
  protected function writeResultToFile(string $filename = 'response') {
    $content = $this->client->getResponse()->getContent();
    file_put_contents("/var/www/src/var/log/$filename.html", $content);
  }

  /**
   * @param array<string, mixed> $values пары название-значение
   *
   * @return void
   */
  protected function setSessionData(array $values): void {
    static::getContainer()->get('event_dispatcher')->addListener(
      KernelEvents::REQUEST,
      function (RequestEvent $event) use ($values) {
        /** @var Session $session */
        $session = $this->container->get('session.factory')->createSession();
        foreach ($values as $k => $v) {
          $session->set($k, $v);
        }
        $event->getRequest()->setSession($session);
      }
    );
  }
}
