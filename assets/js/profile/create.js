require('../app');
require('../vue');

import AvatarEdit from '../../vue/profile/AvatarEdit';

new Vue({
	el: '#avatar-edit-field',
	components: {
		AvatarEdit
	},
});
