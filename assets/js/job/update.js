require('../app');
require('../vue');

import JobImage from '../../vue/Job/JobImage.vue';

new Vue({
	el: '#image-field-app',
	components: {
		JobImage
	},
});
