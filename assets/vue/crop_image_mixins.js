const MODE_MOVE = 'move_move';
const MODE_RESIZE = 'move_resize';

let base = {
  props: {
    minSideSize: {
      type: Number,
      default: 100,
    },
    resizeRectSize: {
      type: Number,
      default: 10,
    },
    selectionInitMarginPercent: {
      type: Number,
      default: 20,
    },
  },
  data() {
    return {
      isMouseDown: false,
      mode: MODE_MOVE,
      left: 0,
      top: 0,
      sideSize: 0,
      canvas: null,
      ctx: null,
      selectionStrokeStyle: '#fff',
      resizeStrokeStyle: '#fff',
      showClear: false,
      clearTop: 0,
      clearLeft: 0,
    };
  },
  methods: {
    initCanvas(image) {
      this.canvas.width = image.width;
      this.canvas.height = image.height;

      let top = image.offsetTop;
      let left = image.offsetLeft;
      this.canvas.setAttribute('style', `top: ${top}px; left: ${left}px;`);

      this.clearLeft = left + image.width - this.$refs.clear.offsetWidth - 5;
      this.clearTop = top + 5;

      // @todo: при небольшом ресайзе окна нет смысла сбрасывать позицию в исходную.
      let max_size = Math.max(image.width, image.height);
      let selection_init_margin = Math.round(max_size / 100 * this.selectionInitMarginPercent);

      this.top = selection_init_margin;
      this.left = selection_init_margin;
      this.sideSize = Math.min(image.width, image.height) - selection_init_margin * 2;

      this.draw();
    },
    onMouseDown(e) {
      this.isMouseDown = true;

      let rect = this.canvas.getBoundingClientRect();
      let mouseX = e.clientX - rect.left;
      let mouseY = e.clientY - rect.top;

      if (this.isMouseInResizeElement(mouseX, mouseY)) {
        this.mode = MODE_RESIZE;
      }
      else {
        this.mode = MODE_MOVE;
      }
    },
    onMouseDownMove(e) {
      if (MODE_MOVE === this.mode) {
        this.left += e.movementX;
        this.top += e.movementY;
      }
      else {
        this.sideSize += Math.max(e.movementX, e.movementY);
      }

      this.checkSelection();

      this.draw();
    },
    onMouseMove(e) {
      let rect = this.canvas.getBoundingClientRect();
      let mouseX = e.clientX - rect.left;
      let mouseY = e.clientY - rect.top;

      if (this.isMouseInResizeElement(mouseX, mouseY)) {
        this.resizeStrokeStyle = '#f00';
        this.canvas.style.cursor = 'nwse-resize';
        this.showClear = !this.isMouseDown;
      }
      else if (this.isMouseInSelection(mouseX, mouseY)) {
        if (MODE_RESIZE !== this.mode) {
          this.resizeStrokeStyle = '#fff';
          this.canvas.style.cursor = 'move';
        }
        this.showClear = !this.isMouseDown;
      }
      else {
        this.resizeStrokeStyle = '#fff';
        this.canvas.style.cursor = 'default';
        this.showClear = true;
      }

      if (this.isMouseDown) {
        this.onMouseDownMove(e);
      }
      else {
        this.draw();
      }
    },
    onMouseUp(e) {
      this.isMouseDown = false;
      this.mode = MODE_MOVE;
    },
    onMouseEnter() {
      this.showClear = true;
    },
    onMouseLeave(e) {
      if (e.relatedTarget !== this.$refs.clear) {
        this.showClear = false;
      }
    },
    checkSelection() {
      if (this.sideSize < this.minSideSize) {
        this.sideSize = this.minSideSize;
      }

      if (this.sideSize > this.canvas.width) {
        this.sideSize = this.canvas.width;
      }

      if (this.sideSize > this.canvas.height) {
        this.sideSize = this.canvas.height;
      }

      if (this.left < 0) {
        this.left = 0;
      }

      if (this.top < 0) {
        this.top = 0;
      }

      if (this.left > this.canvas.width - this.sideSize) {
        this.left = this.canvas.width - this.sideSize;
      }

      if (this.top > this.canvas.height - this.sideSize) {
        this.top = this.canvas.height - this.sideSize;
      }
    },
  },
  mounted() {
    this.canvas = document.getElementById('canvas');
    this.ctx = this.canvas.getContext('2d');

    document.addEventListener('mouseup', this.onMouseUp);

    this.init();
  },
  destroyed() {
    document.removeEventListener('mouseup', this.onMouseUp);
  },
};

let cropRect = {
  mixins: [
    base
  ],
  methods: {
    isMouseInResizeElement(mouseX, mouseY) {
      let rect = {
        left: this.left,
        top: this.top,
        right: this.left + this.sideSize,
        bottom: this.top + this.sideSize,
      };

      let resizeRect = {
        x: rect.right - this.resizeRectSize / 2,
        y: rect.bottom - this.resizeRectSize / 2,
      };

      return (mouseX > resizeRect.x && mouseX < resizeRect.x + this.resizeRectSize
        && mouseY > resizeRect.y && mouseY < resizeRect.y + this.resizeRectSize);
    },
    isMouseInSelection(mouseX, mouseY) {
      let rect = {
        left: this.left,
        top: this.top,
        right: this.left + this.sideSize,
        bottom: this.top + this.sideSize,
      };

      return (mouseX > rect.left && mouseX < rect.right
        && mouseY > rect.top && mouseY < rect.bottom);
    },
    draw() {
      this.ctx.fillStyle = 'rgba(0, 0, 0, 0.7)';

      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

      this.ctx.clearRect(this.left, this.top, this.sideSize, this.sideSize);
      this.ctx.strokeStyle = this.selectionStrokeStyle;

      this.ctx.beginPath();

      let rect = {
        left: this.left,
        top: this.top,
        right: this.left + this.sideSize,
        bottom: this.top + this.sideSize,
      };
      this.ctx.moveTo(rect.left, rect.top);
      this.ctx.lineTo(rect.left, rect.bottom);

      this.ctx.moveTo(rect.right, rect.top);
      this.ctx.lineTo(rect.right, rect.bottom);

      this.ctx.moveTo(rect.left, rect.top);
      this.ctx.lineTo(rect.right, rect.top);

      this.ctx.moveTo(rect.left, rect.bottom);
      this.ctx.lineTo(rect.right, rect.bottom);

      this.ctx.stroke();

      let resizeRect = {
        x: rect.right - this.resizeRectSize / 2,
        y: rect.bottom - this.resizeRectSize / 2,
      };
      this.drawResizeElement(resizeRect.x, resizeRect.y);
    },
    drawResizeElement(x, y) {
      this.ctx.clearRect(x, y, this.resizeRectSize, this.resizeRectSize);
      this.ctx.strokeStyle = this.resizeStrokeStyle;
      this.ctx.strokeRect(x, y, this.resizeRectSize, this.resizeRectSize);
    },
  },
};

const CIRCLE = Math.PI * 2;

let cropCircle = {
  mixins: [
    base
  ],
  methods: {
    isMouseInResizeElement(mouseX, mouseY) {
      let resizeStartDistance = this.sideSize - this.sideSize / 8 - this.resizeRectSize;
      let resizeRect = {
        x: this.left + resizeStartDistance,
        y: this.top + resizeStartDistance,
      };

      return (mouseX > resizeRect.x && mouseX < resizeRect.x + this.resizeRectSize
        && mouseY > resizeRect.y && mouseY < resizeRect.y + this.resizeRectSize);
    },
    isMouseInSelection(mouseX, mouseY) {
      let rect = {
        left: this.left,
        top: this.top,
        right: this.left + this.sideSize,
        bottom: this.top + this.sideSize,
      };

      return (mouseX > rect.left && mouseX < rect.right
        && mouseY > rect.top && mouseY < rect.bottom);
    },
    draw() {
      this.ctx.fillStyle = 'rgba(0, 0, 0, 0.7)';

      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

      this.ctx.strokeStyle = this.selectionStrokeStyle;

      let radius = this.sideSize / 2;

      this.ctx.save();
      this.ctx.beginPath();
      this.ctx.arc(this.left + radius, this.top + radius, radius, 0, CIRCLE, false);
      this.ctx.lineWidth = 2;
      this.ctx.stroke();
      this.ctx.fillStyle = 'rgba(255, 255, 255, 1)';
      this.ctx.globalCompositeOperation = 'destination-out';
      this.ctx.fill();
      this.ctx.restore();

      let resizeStartDistance = this.sideSize - this.sideSize / 8 - this.resizeRectSize / 2;
      this.drawResizeElement(this.left + resizeStartDistance, this.top + resizeStartDistance);
    },
    drawResizeElement(x, y) {
      let radius = this.resizeRectSize / 2;
      this.ctx.save();
      this.ctx.beginPath();
      this.ctx.arc(x, y, radius, 0, CIRCLE, false);
      this.ctx.lineWidth = 2;
      this.ctx.strokeStyle = this.resizeStrokeStyle;
      this.ctx.stroke();
      this.ctx.fillStyle = 'rgba(255, 255, 255, 1)';
      this.ctx.globalCompositeOperation = 'destination-out';
      this.ctx.fill();
      this.ctx.restore();
    },
  },
};

export {
  cropRect,
  cropCircle,
}
